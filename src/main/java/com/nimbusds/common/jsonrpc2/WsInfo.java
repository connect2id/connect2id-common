package com.nimbusds.common.jsonrpc2;


/**
 * Web service name and version.
 */
public final class WsInfo {

	
	/**
	 * The web service name.
	 */
	private final String name;
	
	
	/**
	 * The web service version.
	 */
	private final String version;
	
	
	/**
	 * Creates a new web service name and version instance.
	 *
	 * @param name    The web service name.
	 * @param version The web service version.
	 */
	public WsInfo(final String name, final String version) {
	
		this.name = name;
		this.version = version;
	}


	/**
	 * Gets the web service name.
	 *
	 * @return The web service name.
	 */
	public String getName() {
	
		return name;
	}
	
	
	/**
	 * Gets the web service version.
	 *
	 * @return The web service version.
	 */
	public String getVersion() {
	
		return version;
	}
	
	
	/**
	  * Returns the string representation of this web service anme and
	  * version.
	  *
	  * <p>Format:
	  *
	  * <pre>
	  * [ws-name] [ws-version]
	  * </pre>
	  *
	  * @return The string representation.
	  */
	@Override
	public String toString() {
	
		return name + " " + version;
	}
}
