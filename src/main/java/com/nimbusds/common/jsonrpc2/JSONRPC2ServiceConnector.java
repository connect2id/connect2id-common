package com.nimbusds.common.jsonrpc2;


import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

import java.net.URL;
import java.util.concurrent.atomic.AtomicLong;


/**
 * JSON-RPC 2.0 service connector.
 *
 * <p>Supports:
 *
 * <ul>
 *     <li>Local connections via a {@code com.thetransactioncompany.jsonrpc2.server.RequestHandler}
 *         interface.
 *     <li>Network (HTTP) connections.
 * </ul>
 */
public class JSONRPC2ServiceConnector {


	/**
	 * Local JSON-RPC 2.0 service connection, represented as a JSON-RPC 2.0
	 * request handler; {@code null} if remote (HTTP) connections are used.
	 */
	private final RequestHandler jsonrpc2Service;
	
	
	/**
	 * Remote (HTTP) JSON-RPC 2.0 service connection, represented by a 
	 * JSON-RPC 2.0 session to the service URL; {@code null} if local 
	 * connections are used.
	 */
	private final JSONRPC2Session session;
	
	
	/**
	 * Optional API key for service access, {@code null} if not required.
	 */
	private final String apiKey;
	
	
	/**
	 * Incrementable JSON-RPC 2.0 request ID.
	 */
	private final AtomicLong reqID = new AtomicLong();


	/**
	 * Creates a new local JSON-RPC 2.0 service connector.
	 *
	 * @param jsonrpc2Service The JSON-RPC 2.0 service accessible through a 
	 *                        local request handler interface. Must not be 
	 *                        {@code null}.
	 * @param apiKey          Optional API key for service access, 
	 *                        {@code null} if not required.
	 */
	public JSONRPC2ServiceConnector(final RequestHandler jsonrpc2Service, 
		                        final String apiKey) {
	
		if (jsonrpc2Service == null)
			throw new IllegalArgumentException("The JSON-RPC 2.0 service request handler must not be null");
		
		this.jsonrpc2Service = jsonrpc2Service;

		session = null;
		
		this.apiKey = apiKey;
	}
	
	
	/**
	 * Creates a new remote (HTTP) JSON-RPC 2.0 service connector.
	 *
	 * @param url                  The JSON-RPC 2.0 service HTTP(S) URL. 
	 *                             Must not be {@code null}.
	 * @param trustSelfSignedCerts Determines the trust of self-signed 
	 *                             X.509 certificates presented by the
	 *                             service (for HTTPS connections).
	 * @param apiKey               Optional API key for service access, 
	 *                             {@code null} if not required.
	 * @param connectTimeout       Connect timeout for HTTP requests, in 
	 *                             milliseconds. Zero disables the option.
	 * @param readTimeout          Read timeout for HTTP requests, in
	 *                             milliseconds. Zero disables the option.
	 */
	public JSONRPC2ServiceConnector(final URL url, 
	                                final boolean trustSelfSignedCerts,
	                                final String apiKey,
	                                final int connectTimeout,
	                                final int readTimeout) {
	
		if (url == null)
			throw new IllegalArgumentException("The JSON-RPC 2.0 service URL must not be null");
		
		session = new JSONRPC2Session(url);
		
		session.getOptions().trustAllCerts(trustSelfSignedCerts);
		session.getOptions().setConnectTimeout(connectTimeout);
		session.getOptions().setReadTimeout(readTimeout);

		jsonrpc2Service = null;
		
		this.apiKey = apiKey;
	}
	
	
	/**
	 * Gets the optional API key for service access.
	 *
	 * @return The API key for service access, {@code null} if not
	 *         provided.
	 */
	public String getAPIKey() {
	
		return apiKey;
	}


	/**
	 * Gets the next JSON-RPC 2.0 request ID from the atomic counter.
	 *
	 * @return The next request ID.
	 */
	public long getNextRequestID() {

		return reqID.incrementAndGet();
	}
	
	
	/**
	 * Returns {@code true} if the connector is to a local service
	 * instance.
	 *
	 * @return {@code true} if this is a local service connector.
	 */
	public boolean isLocal() {

		return jsonrpc2Service != null;
	}
	
	
	/**
	 * Returns {@code false} if the connector is to a remote (HTTP) service
	 * instance.
	 *
	 * @return {@code true} if this is a remote (HTTP) service connector.
	 */
	public boolean isRemote() {

		return session != null;
	}
	
	
	/**
	 * Sends the specified JSON-RPC 2.0 request to the service (local or 
	 * remote).
	 *
	 * @param request The JSON-RPC 2.0 request to send. Must not be 
	 *                {@code null}.
	 *
	 * @return The received JSON-RPC 2.0 response.
	 *
	 * @throws JSONRPC2SessionException If the request call failed due to a 
	 *                                  service connection or other 
	 *                                  exception.
	 */
	public JSONRPC2Response send(final JSONRPC2Request request)
		throws JSONRPC2SessionException {
		
		if (isLocal()) {
			
			// Local service
			
			// Simulate localhost request over HTTPS
			boolean secure = true;
			var mctx = new MessageContext("localhost", "127.0.0.1", secure);
			
			return jsonrpc2Service.process(request, mctx);
		}
		else {
		
			// Remote service
			return session.send(request);
		}
	}
}
