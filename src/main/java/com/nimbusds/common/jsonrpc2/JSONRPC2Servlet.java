package com.nimbusds.common.jsonrpc2;


import com.nimbusds.common.servlet.ServletLogUtility;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.function.Function;


/**
 * Base abstract JSON-RPC 2.0 request servlet for Connect2id services. Requests
 * are received at the {@link #doPost HTTP POST endpoint} and passed to
 * {@link #service} for processing. The {@link #doGet HTTP GET endpoint}
 * prints out a {@link Banner banner text message} that identifies the
 * web service name and version and lists the supported JSON-RPC 2.0 requests.
 *
 * <p>Inheriting servlets must initialise all protected members in their
 * {@code init(...)} method.
 */
public abstract class JSONRPC2Servlet extends HttpServlet {


	/**
	 * The target JSON-RPC 2.0 service.
	 */
	protected RequestHandlerWithLifecycle service;
	
	
	/**
	 * The web service name.
	 */
	protected String wsName;
	
	
	/**
	 * The web service version.
	 */
	protected String wsVersion;
	
	
	/**
	 * The Connect2id service banner, printed at the HTTP GET endpoint.
	 */
	protected Banner banner;
	
	
	/**
	 * The HTTP (POST) response content type of JSON-RPC 2.0 responses.
	 */
	protected String responseContentType;
	
	
	/**
	 * Optional mapper of JSON-RPC 2.0 error codes to HTTP status codes.
	 */
	protected Function<Integer, Integer> httpStatusCodeMapper;
	
	
	/**
	 * The logger.
	 */
	private final Logger log = LogManager.getLogger(JSONRPC2Servlet.class);
	
	
	/**
         * Receives JSON-RPC 2.0 requests for the Connect2id service by means
	 * of HTTP POST.
         *
         * @param httpRequest  The HTTP request.
         * @param httpResponse The output HTTP response.
         *
         * @throws ServletException On a request handling exception.
         */
	@Override
        public void doPost(HttpServletRequest httpRequest,
                           HttpServletResponse httpResponse)
                throws ServletException {
        
                // Log the HTTP request
                ServletLogUtility.log(httpRequest);
        
                // Get the remote client details
                var requestContext = new MessageContext(httpRequest);
        
                // Identify service in custom HTTP header
                httpResponse.setHeader("X-Web-Service", wsName);
                
                // Indicate response content type
                httpResponse.setContentType(responseContentType);
                
                // Prepare to write out the response
                PrintWriter out;
                try {
                        out = httpResponse.getWriter();
                        
                } catch (IOException e) {
                        log.error("HTTP POST I/O exception: {}", e.getMessage(), e);
                        throw new ServletException("HTTP POST I/O exception: " + e.getMessage(), e);
                }
	
		// Get the HTTP POST body
		StringBuilder postContent = new StringBuilder(256);
		
		try {
			BufferedReader reader = httpRequest.getReader();
			
			String line;
			
			while ((line = reader.readLine()) != null) {
				postContent.append(line);
				postContent.append(System.getProperty("line.separator"));
			}
			
			reader.close();
			
		} catch (IOException e) {
			log.error("HTTP POST I/O exception: {}", e.getMessage(), e);
			throw new ServletException("HTTP POST I/O exception: " + e.getMessage(), e);
		}
                
                
                // Parse the JSON-RPC 2.0 request
                JSONRPC2Request request;
                
                try {
                        request = JSONRPC2Request.parse(postContent.toString());
                        
                } catch (JSONRPC2ParseException e) {
                
                        log.info("Invalid JSON-RPC 2.0 request: {}", e.getMessage());
                        
                        // Report parse error back to client
                        out.println(new JSONRPC2Response(JSONRPC2Error.PARSE_ERROR, null));
                        return;
                }
                
                // Process the request
                JSONRPC2Response response = service.process(request, requestContext);
		
		if (httpStatusCodeMapper != null && ! response.indicatesSuccess()) {
			JSONRPC2Error jsonrpc2Error = response.getError();
			httpResponse.setStatus(httpStatusCodeMapper.apply(jsonrpc2Error.getCode()));
		}
                
                // Output response JSON
                out.println(response);
		out.close();
		
		// Log the JSONRPC 2.0 request + response
		JSONRPC2LogUtility.log(request, response);
        }
	
	
	/**
         * Outputs a simple text/plain banner message that identifies the 
	 * Connect2id web service name/version and lists the supported JSON-RPC
	 * 2.0 requests.
         *
         * @param httpRequest  The HTTP request.
         * @param httpResponse The output HTTP response.
         *
         * @throws ServletException On a request handling exception.
         */
	@Override
        public void doGet(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
                throws ServletException {
                
                // Log the HTTP request
                ServletLogUtility.log(httpRequest);
		
		// Identify web service name in the HTTP header
		httpResponse.setHeader("X-Web-Service", wsName);
		
		// Indicate a plain text message
		httpResponse.setContentType("text/plain; charset=utf-8");
                
                try {
                        banner.apply(httpResponse);
			
                } catch (IOException e) {
                        log.error("HTTP GET I/O exception: {}", e.getMessage(), e);
                        throw new ServletException(e.getMessage(), e);
                }
        }
	
	
	/**
         * Called at servlet shutdown. Stops the Connect2id web service,
	 * exceptions are logged at WARN level.
         */
	@Override
        public void destroy() {
		
		log.info("Stopping {} ...", wsName);
		
		try {
			service.stop();
			
		} catch (Exception e) {
		
			log.warn("{} stop exception: {}", wsName, e.getMessage(), e);
		}
        }
}
