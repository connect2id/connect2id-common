package com.nimbusds.common.jsonrpc2;


import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


/**
 * Handles JSON-RPC 2.0 requests for general information about a web service.
 * The {@link #init} method must be called before servicing requests.
 *
 * <p>List of the handled requests:
 *
 * <ul>
 *     <li><b>ws.getName</b> Reports the web service name (as a string).
 *     <li><b>ws.getVersion</b> Reports the web service version (as a string).
 *     <li><b>ws.getTime</b> Reports the local server time (as a string).
 * </ul>
 */
public class WsInfoRequestHandler
	implements RequestHandler {
	
	
	/**
	 * ISO 8601 date/time format.
	 */
	private final static SimpleDateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	
	
	/**
	 * The web service name and version to report.
	 */
	private WsInfo wsInfo;
	
	
	/** 
	 * The handled request names. 
	 */
	private static final String[] HANDLED_REQUESTS = {"ws.getName", "ws.getVersion", "ws.getTime"};
	
	
	/**
	 * Initialises this WS info request handler.
	 *
	 * @param wsInfo The web service name and version to report. Must not be 
	 *               {@code null}.
	 */
	public void init(final WsInfo wsInfo) {
	
		if (wsInfo == null)
			throw new IllegalArgumentException("The web service info must not be null");
	
		this.wsInfo = wsInfo;
		
		iso8601DateTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
	}
	
	
	/**
	 * Lists the JSON-RPC 2.0 request method names that this handler 
	 * processes.
	 *
	 * @return The method names of the served JSON-RPC 2.0 requests.
	 */
	public String[] handledRequests() {
	
		return HANDLED_REQUESTS;
	}
	
	
	/**
	 * Returns the local web service time in 
	 * <a href="http://en.wikipedia.org/wiki/ISO_8601">ISO-8601</a> format
	 * {@code yyyy-MM-dd'T'HH:mm:ssZ} (UTC timezone), for example 
	 * "2012-07-20T13:53:51Z".
	 *
	 * @return The local web server time (UTC timezone).
	 */
	private String getWsTime() {
	
		var now = new Date();
		
		// Append UTC timezone as Z
		return iso8601DateTimeFormat.format(now) + "Z";
	}

	
	/**
	 * Processes JSON-RPC 2.0 requests for general information about this 
	 * web service.
	 *
	 * @param request    The JSON-RPC 2.0 request.
	 * @param requestCtx Additional information about the request.
	 *
	 * @return The JSON-RPC 2.0 response.
	 */
	public JSONRPC2Response process(final JSONRPC2Request request, final MessageContext requestCtx) {
	
		// Get the request method
		String method = request.getMethod();
		
		// Get the request ID
		Object id = request.getID();
		
		// Process the requests		
		Object result;
		
		try {
                        result = switch (method) {
                                case "ws.getName" -> wsInfo.getName();
                                case "ws.getVersion" -> wsInfo.getVersion();
                                case "ws.getTime" -> getWsTime();
                                default -> throw JSONRPC2Error.METHOD_NOT_FOUND;
                        };
			
		} catch (JSONRPC2Error e) {
			
			// Return response with error
			return new JSONRPC2Response(e, id);
		}
		
		// Return response with result
		return new JSONRPC2Response(result, id);
	}
}
