package com.nimbusds.common.jsonrpc2;


import org.apache.logging.log4j.Logger;

import java.net.URL;


/**
 * Logs the detected name and version of a remote JSON-RPC 2.0 web service, as 
 * reported by a {@link WsInfoRequestHandler}. To prevent blocking the detection
 * routine is run on a separate thread.
 *
 * <p>Example:
 *
 * <pre>
 * new Thread(new LoggingWsDetector(...)).start();
 * </pre>
 */
public class LoggingWsDetector extends WsDetector implements Runnable {
	
	
	/**
	 * The expected web service name.
	 */
	private final String expectedWsName;
	
	
	/**
	 * The logger.
	 */
	private final Logger log;
	
	
	/**
	 * The detected WS name and version, {@code null} if not detected.
	 */
	private WsInfo wsInfo;
	
	
	/**
	 * Creates a new JSON-RPC 2.0 web service detector.
	 *
	 * @param url            The web service HTTP(S) URL. Must not be 
	 *                       {@code null}.
	 * @param expectedWsName The expected web service name, as reported by 
	 *                       {@code ws.getName}. Must not be {@code null}.
	 * @param log            The logger. Must not be {@code null}.
	 */
	public LoggingWsDetector(final URL url, final String expectedWsName, final Logger log) {
	
		super(url);
		
		if (expectedWsName == null)
			throw new IllegalArgumentException("The web service name must not be null");
		
		this.expectedWsName = expectedWsName;
		
		
		if (log == null)
			throw new IllegalArgumentException("The logger must not be null");
			
		this.log = log;
	}
	
	
	/**
	 * Returns the detected web service name and version.
	 *
	 * @return The web service name and version, {@code null} if not 
	 *         detected.
	 */
	public WsInfo getWsInfo() {
	
		return wsInfo;
	}
	
	
	/**
	 * Detects and logs the JSON-RPC 2.0 web service name and version. The 
	 * target web service must handle {@code ws.getName} and 
	 * {@code ws.getVersion} calls, as implemented by 
	 * {@link WsInfoRequestHandler}.
	 *
	 * <p>Any encountered exceptions are logged at WARN level.
	 */
	@Override
	public void run() {
	
		WsInfo wsInfo;
		
		try {
			wsInfo = detect();
			
		} catch (Exception e) {
		
			log.warn("Couldn't detect remote {} web service: {}", expectedWsName, e.getMessage());
			return;
		}
		
		if (! expectedWsName.equalsIgnoreCase(wsInfo.getName())) {
		
			log.warn("Remote web service is not {}, detected {} {}", expectedWsName, wsInfo.getName(), wsInfo.getVersion());
			return;
		}
			
		
		log.info("Detected remote web service {} {}", wsInfo.getName(), wsInfo.getVersion());
	}
}
