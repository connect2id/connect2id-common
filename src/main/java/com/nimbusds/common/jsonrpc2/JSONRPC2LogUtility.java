package com.nimbusds.common.jsonrpc2;


import com.thetransactioncompany.jsonrpc2.JSONRPC2ParamsType;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;


/**
 * Helper methods for Log4j message generation.
 */
public class JSONRPC2LogUtility {


	/**
	 * The logger.
	 */
	private static final Logger log = LogManager.getLogger("JSON-RPC");
	
	
	/**
	 * Logs (at INFO level) the name of the JSON-RPC 2.0 request and the 
	 * resulting response (success or error code + message). The message 
	 * may be optionally prefixed.
	 *
	 * <p>Format:
	 * 
	 * <pre>
	 * [prefix] request-method-name: Success | Error code: Message
	 * </pre>
	 *
	 * <p>Example:
	 *
	 * <pre>
	 * [CID dff6fb77-7f96-4b82-a0f9-0ac25cfb1852] ldap.getEntry: Success
	 *
	 * [CID dff6fb77-7f96-4b82-a0f9-0ac25cfb1852] ldap.getEntry: Error -1000: Invalid/expired LDAP connection identifier (CID)
	 * </pre>
	 *
	 * @param request  The JSON-RPC 2.0 request. Must not be {@code null}.
	 * @param response The resulting JSON-RPC 2.0 response. Must not be
	 *                 {@code null}.
	 * @param prefix   The log message prefix. If {@code null} the log 
	 *                 message will not be prefixed.
	 */
	public static void log(final JSONRPC2Request request, 
		               final JSONRPC2Response response, 
		               final String prefix) {
		
		if (! log.isInfoEnabled())
			return;
		
		StringBuilder msg = new StringBuilder();
		
		if (prefix != null) {
			msg.append("[").append(prefix).append("] ");
		}
		
		msg.append(request.getMethod());
		
		msg.append(": ");


		if (response.indicatesSuccess()) {
			msg.append("Success");
		} else {
			msg.append("Error ")
				.append(response.getError().getCode())
				.append(": ")
				.append(response.getError().getMessage());
		}

		log.info(msg.toString());
	}
	
	
	/**
	 * Logs (at INFO level) the name of the JSON-RPC 2.0 request and the 
	 * resulting response (success or error code + message).
	 *
	 * <p>Format:
	 * 
	 * <pre>
	 * request-method-name: Success | Error code: Message
	 * </pre>
	 *
	 * <p>Example:
	 *
	 * <pre>
	 * ldap.getEntry: Success
	 *
	 * ldap.getEntry: Error -1000 Invalid/expired LDAP connection identifier (CID)
	 * </pre>
	 *
	 * @param request  The JSON-RPC 2.0 request. Must not be {@code null}.
	 * @param response The resulting JSON-RPC 2.0 response. Must not be
	 *                 {@code null}.
	 */
	public static void log(final JSONRPC2Request request, 
		               final JSONRPC2Response response) {
	
		log(request, response, null);
	}
	
	
	/**
	 * Hides the value of the specified parameter in a JSON-RPC 2.0 request.
	 * The value is replaced by a string "[hidden]". If the specified 
	 * parameter is not found request is returned unmodified.
	 *
	 * <p>This method is intended for logging JSON-RPC 2.0 requests.
	 *
	 * @param request   The JSON-RPC 2.0 request. May be {@code null}.
	 * @param paramName The name of the parameter to hide. May be 
	 *                  {@code null}.
	 *
	 * @return The processed JSON-RPC 2.0 request.
	 */
	public static JSONRPC2Request hideParameter(final JSONRPC2Request request, 
		                                    final String paramName) {
	
		if (request == null)
			return null;
	
		if (request.getParamsType() != JSONRPC2ParamsType.OBJECT)
			return request;
		
		Map <String,Object> params = request.getNamedParams();
		
		if (params.containsKey(paramName))
			params.put(paramName, "[hidden]");
		
		return request;
	}
	
	
	/**
	 * Hides the "password" parameter value in a JSON-RPC 2.0 request. The
	 * value is replaced by a string "[hidden]". If no "password" parameter
	 * is contained the request is returned unmodified.
	 *
	 * <p>This method is intended for logging JSON-RPC 2.0 requests.
	 *
	 * @param request The JSON-RPC 2.0 request. May be {@code null}.
	 *
	 * @return The JSON-RPC 2.0 request with any password value hidden.
	 */
	public static JSONRPC2Request hidePassword(final JSONRPC2Request request) {
	
		return hideParameter(request, "password");
	}


	/**
	 * Prevents public instantiation.
	 */
	private JSONRPC2LogUtility() { }
}
