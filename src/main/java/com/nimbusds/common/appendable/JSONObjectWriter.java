package com.nimbusds.common.appendable;


import jakarta.ws.rs.WebApplicationException;
import net.minidev.json.JSONAware;
import net.minidev.json.JSONValue;

import java.io.IOException;
import java.io.Writer;
import java.util.function.Consumer;


/**
 * JSON object writer for JAX-RS result streaming.
 *
 * <p>Use {@link com.nimbusds.common.json.JSONObjectWriter}.
 */
@Deprecated
public class JSONObjectWriter<T extends JSONAware> implements Consumer<T>, Appendable<T> {


	/**
	 * Writer for the appended elements.
	 */
	private final Writer writer;


	/**
	 * The JSON object key extractor.
	 */
	private final KeyExtractor<T> keyExtractor;


	/**
	 * The position of the appender.
	 */
	private boolean first;


	/**
	 * Creates a new JSON object writer.
	 *
	 * @param writer       Writer for the JSON object. Must not be
	 *                     {@code null}.
	 * @param keyExtractor Extracts or determines a JSON object key from
	 *                     the appended elements. Must not be {@code null}.
	 */
	public JSONObjectWriter(final Writer writer, final KeyExtractor<T> keyExtractor) {


		if (writer == null) {
			throw new IllegalArgumentException("The writer must not be null");
		}

		this.writer = writer;

		if (keyExtractor == null) {
			throw new IllegalArgumentException("The key extractor must not be null");
		}

		this.keyExtractor = keyExtractor;

		first = true;
	}


	/**
	 * Writes out the opening '{' of the JSON object.
	 */
	public void writeStart() {

		try {
			writer.write('{');

		} catch (IOException e) {

			throw new WebApplicationException("Couldn't write JSON object: " + e.getMessage(), e);
		}
	}


	@Override
	public void accept(final T element) {

		if (element == null) {
			return; // Nothing to append
		}

		String key = keyExtractor.extractKey(element);

		if (key == null) {
			return; // Couldn't extract key
		}

		StringBuilder sb = new StringBuilder();

		if (first) {
			first = false;
		} else {
			sb.append(',');
		}

		sb.append('"');

		sb.append(JSONValue.escape(key));
		sb.append('"');
		sb.append(':');
		sb.append(element.toJSONString());

		try {
			writer.write(sb.toString());
			writer.flush();

		} catch (IOException e) {

			throw new WebApplicationException("Couldn't write JSON object: " + e.getMessage(), e);
		}
	}
	
	
	@Override
	public void append(final T element) {
		accept(element);
	}


	/**
	 * Writes out the closing '}' of the JSON object and closes the writer.
	 */
	public void writeEnd() {

		try {
			writer.write('}');
			writer.close();

		} catch (IOException e) {

			throw new WebApplicationException("Couldn't write JSON object: " + e.getMessage(), e);
		}
	}
}

