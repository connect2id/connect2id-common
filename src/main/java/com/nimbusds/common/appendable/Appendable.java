package com.nimbusds.common.appendable;


import java.io.Serializable;


/**
 * Generic appendable interface.
 *
 * <p>User {@link java.util.function.Consumer} instead.
 */
@Deprecated
public interface Appendable <T> extends Serializable {


	/**
	 * Appends the specified element.
	 *
	 * @param element The element to append. May be {@code null}.
	 */
	void append(final T element);
}