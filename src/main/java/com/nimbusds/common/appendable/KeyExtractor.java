package com.nimbusds.common.appendable;


/**
 * Key extractor, intended for {@link JSONObjectWriter} use.
 *
 * <p>Use {@link com.nimbusds.common.json.KeyExtractor}.
 */
@Deprecated
public interface KeyExtractor<T> {


	/**
	 * Extracts or determines a JSON object key from the specified object.
	 *
	 * @param object The object. Must not be {@code null}.
	 *
	 * @return The JSON object key, {@code null} if extraction failed.
	 */
	String extractKey(final T object);
}
