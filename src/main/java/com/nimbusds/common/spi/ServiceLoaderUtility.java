package com.nimbusds.common.spi;


import java.util.*;


/**
 * Service (SPI) loader utility.
 */
public class ServiceLoaderUtility {
	
	
	/**
	 * Loads a single Service Provider Interface (SPI) implementation.
	 *
	 * @param tClass      The SPI. Not {@code null}.
	 * @param defaultImpl The default implementation, {@code null} if not
	 *                    specified.
	 *
	 * @return The loaded SPI implementation or the default one.
	 *
	 * @throws RuntimeException If more than one SPI implementation was
	 *                          found.
	 */
	public static <T> T loadSingle(final Class<T> tClass, final T defaultImpl) {
		
		List<T> impls = new LinkedList<>();
		
		for (T t : ServiceLoader.load(tClass)) {
			impls.add(t);
		}
		
		if (impls.isEmpty()) {
			return defaultImpl;
		}
		
		if (impls.size() > 1) {
			
			List<String> classNames = new LinkedList<>();
			
			for (T impl: impls) {
				classNames.add(impl.getClass().getName());
			}
			
			throw new RuntimeException("More than one " + tClass.getName() + " SPI implementation found: " + classNames);
		}
		
		return impls.get(0);
	}
	
	
	/**
	 * Loads multiple Service Provider Interface (SPI) implementations.
	 *
	 * @param tClass      The SPI. Not {@code null}.
	 * @param defaultImpl The default implementation, {@code null} if not
	 *                    specified.
	 *
	 * @return The loaded SPI implementations, if none were loaded and a
	 *         default was specified, a singleton set with it.
	 */
	public static <T> Set<T> loadMultiple(final Class<T> tClass, final T defaultImpl) {
		
		Set<T> impls = new HashSet<>();
		
		for (T t : ServiceLoader.load(tClass)) {
			impls.add(t);
		}
		
		if (impls.isEmpty() && defaultImpl != null) {
			impls.add(defaultImpl);
		}
		
		return impls;
	}
}
