package com.nimbusds.common.log;


import com.nimbusds.oauth2.sdk.id.Identifier;
import org.apache.commons.lang3.StringUtils;


/**
 * Log sanitiser utility.
 */
public class LogSanitizer {
	
	
	private static String escapeMetaCharacters(String inputString){
		
		final String[] metaCharacters = {"\\","{","}","[","]","(",")","|","&","%"};
		
		for (String metaCharacter : metaCharacters) {
			if (inputString.contains(metaCharacter)) {
				inputString = inputString.replace(metaCharacter, "\\" + metaCharacter);
			}
		}
		
		inputString = inputString.replace("\n", "\\n");
		inputString = inputString.replace("\r", "\\r");
		inputString = inputString.replace("\t", "\\t");
		
		return inputString;
	}
	
	
	/**
	 * Sanitizes an identifier.
	 *
	 * @param identifier The identifier, may be {@code null}.
	 *
	 * @return The sanitized identifier string, trimmed to 30 chars,
	 *         {@code null} if none.
	 */
	public static String sanitize(final Identifier identifier) {
		
		return sanitize(identifier, 30);
	}
	
	
	/**
	 * Sanitizes an identifier.
	 *
	 * @param identifier The identifier, may be {@code null}.
	 * @param maxLength  The max length of the output.
	 *
	 * @return The sanitized identifier string, trimmed to 30 chars,
	 *         {@code null} if none.
	 */
	public static String sanitize(final Identifier identifier, final int maxLength) {
		
		return identifier != null ? sanitize(identifier.getValue(), maxLength) : null;
	}
	
	
	/**
	 * Sanitizes a string.
	 *
	 * @param string    The string, may be {@code null}.
	 * @param maxLength The max length of the output.
	 *
	 * @return The sanitized string, trimmed to 30 chars, {@code null} if
	 *         none.
	 */
	public static String sanitize(final String string, final int maxLength) {
		
		if (string == null) {
			return null;
		}
		
		String trimmed = StringUtils.abbreviate(string, "...", maxLength);
		
		return escapeMetaCharacters(trimmed);
	}
}
