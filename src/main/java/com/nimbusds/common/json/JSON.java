package com.nimbusds.common.json;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * JSON serialisation. Serialises {@code null} values.
 */
public final class JSON {
	
	
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();
	
	
	/**
	 * Serialises the specified object to a JSON string. Unicode characters
	 * are not escaped.
	 *
	 * @param obj The object.
	 *
	 * @return The JSON string.
	 */
	public static String toJSON(final Object obj) {
		
		return GSON.toJson(obj);
	}
}
