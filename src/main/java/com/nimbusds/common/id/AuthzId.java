package com.nimbusds.common.id;


import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.LDAPException;

import java.text.ParseException;


/**
 * Represents a SASL authorisation identity, as specified in RFC 4513, section
 * 5.2.1.8. This can be a distinguished name (DN) or a username.
 *
 * <p>Note that for the purpose of keying and comparing authorisation 
 * identities, the DNs are normalised and the usernames are converted to lower 
 * case.
 *
 * <p>DN form: 
 *
 * <pre>
 * "dn: uid=alice,ou=people,dc=wonderland,dc=net"
 * </pre>
 *
 * <p>Username form:
 *
 * <pre>
 * "u: alice"
 * </pre>
 */
public final class AuthzId extends BaseIdentifier {


	/**
	 * The identity data types.
	 */
	public enum IdentityType {
	
		/**
		 * Distinguished name (DN).
		 */
		DN,
		
		
		/**
		 * Username.
		 */
		USERNAME
	}
	
	 
	/**
	 * Anonymous user identity.
	 */
	public static final AuthzId ANONYMOUS = new AuthzId(DN.NULL_DN);
	
	
	/**
	 * The authorisation identity as DN, {@code null} if specified by a 
	 * username.
	 */
	private final DN dn;
	
	
	/**
	 * The authorisation identity as username, {@code null} if specified by
	 * a DN.
	 */
	private final Username username;
	
	
	
	/**
	 * The identity type.
	 */
	private final IdentityType identityType;
	
	
	/**
	 * Parses a string representation of a SASL authorisation identity.
	 *
	 * @param value The string to parse, if {@code null} or empty 
	 *              {@link #ANONYMOUS} is returned.
	 *
	 * @return The parsed authorisation identity.
	 *
	 * @throws ParseException On a bad authzid syntax.
	 */
	public static AuthzId parse(final String value)
		throws ParseException {
	
		if (value == null || value.trim().isEmpty())
			return ANONYMOUS;
		
		if (value.toLowerCase().startsWith("dn:")) {
			
			DN dn;
			
			try {
				// there may be whitespace between "dn:" and the DN value
				dn = new DN(value.substring(3).trim());
				
			} catch (LDAPException e) {
			
				throw new ParseException("Bad authzId syntax: " + e.getMessage(), 3);
			}
			
			return new AuthzId(dn);

		} else if (value.toLowerCase().startsWith("u:")) {
		
			// there may be whitespace between "u:" and the username value
			return new AuthzId(new Username(value.substring(2)));

		} else {
			throw new ParseException("Bad authzId syntax: String must start with dn: or u:", 0);
		}
	}
	
	
	/**
	 * Returns the authzId string representation of the specified 
	 * distinguished name (DN).
	 *
	 * @param dn The distinguished name (DN). Must not be {@code null}.
	 *
	 * @return The authzId string.
	 */
	private static String toAuthzIdString(final DN dn) {
	
		if (dn == null)
			throw new IllegalArgumentException("The authzId DN must not be null");
	
		if (dn.equals(DN.NULL_DN))
			return "dn:";
		else
			return "dn:" + dn.toNormalizedString();
	}
	
	
	/**
	 * Returns the authzId string representation of the specified username.
	 *
	 * @param username The username. Must not be {@code null}.
	 *
	 * @return The authzId string.
	 */
	private static String toAuthzIdString(final Username username) {
	
		if (username == null)
			throw new IllegalArgumentException("The authzId username must not be null");
		
		if (username.toString().isEmpty())
			return "dn:"; // anonymous
		
		return "u:" + username;
	}
	
	
	/**
	 * Creates a new authorisation identity from the specified DN.
	 *
	 * <p>Note that for the purpose of keying and comparing authorisation
	 * IDs, the DN will be normalised (simple normalisation, without 
	 * consulting the schema).
	 *
	 * @param dn The DN, must not be {@code null}. If {@code DN.NULL_DN} 
	 *           indicates an anonymous user.
	 */
	public AuthzId(final DN dn) {
	
		super(toAuthzIdString(dn));
		identityType = IdentityType.DN;
		this.dn = dn;
		username = null;
	}
	
	
	/**
	 * Creates a new authorisation identity from the specified username.
	 *
	 * <p>Note that for the purpose of keying and comparing authorisation
	 * IDs, the username will be converted to lower case.
	 *
	 * @param username The username, must not be {@code null}. If empty
	 *                 indicates an anonymous user.
	 */
	public AuthzId(final Username username) {
	
		super(toAuthzIdString(username));
		identityType = IdentityType.USERNAME;
		dn = null;
		this.username = username;
	}
	
	
	/**
	 * Gets the identity type.
	 *
	 * @return The identity type.
	 */
	public IdentityType getIdentityType() {
	
		return identityType;
	}
	
	
	/**
	 * Gets the identity DN.
	 *
	 * @return The DN, {@code null} if specified as a username instead.
	 */
	public DN getDN() {
	
		return dn;
	}
	
	
	/**
	 * Gets the identity username.
	 *
	 * @return The username, {@code null} if specified as a DN instead.
	 */
	public Username getUsername() {
	
		return username;
	}
	
	
	@Override
	public boolean equals(final Object object) {

	        return object instanceof AuthzId && this.toString().equals(object.toString());
	}
}
