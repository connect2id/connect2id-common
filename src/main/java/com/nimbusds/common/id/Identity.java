package com.nimbusds.common.id;


/**
 * Marker interface for user and other identities used in Connect2id software.
 */
public interface Identity { }
