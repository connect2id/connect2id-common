package com.nimbusds.common.id;


/**
 * Represents a secure immutable session identifier (SID).
 */
public final class SID extends BaseIdentifier {


	/**
         * Creates a new unique session identifier (SID) based on a secure 
	 * randomly generated 256-bit number, Base64URL-encoded.
         */
        public SID() {
        
		super();
        }
	
	
	/**
         * Creates a new session identifier (SID) from the specified string.
         *
         * @param value The session identifier (SID) value.
         */
        public SID(final String value) {
        
		super(value);
	}
	
	
	/**
         * Overrides {@code Object.equals()}.
         *
         * @param object The object to compare to.
         *
         * @return {@code true} if the objects have the same value, otherwise
         *         {@code false}.
         */
	@Override
        public boolean equals(final Object object) {
        
                return object instanceof SID && this.toString().equals(object.toString());
        }
}
