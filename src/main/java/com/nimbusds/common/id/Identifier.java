package com.nimbusds.common.id;


/**
 * Marker interface for identifiers (IDs) used in Connect2id software.
 */
public interface Identifier { }
