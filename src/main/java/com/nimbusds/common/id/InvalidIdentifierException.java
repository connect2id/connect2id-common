package com.nimbusds.common.id;


/**
 * Invalid identifier exception.
 */
public class InvalidIdentifierException extends Exception {
	

	/**
	 * Creates a new invalid identifier exception.
	 *
	 * @param message The message.
	 */
	InvalidIdentifierException(final String message) {
		super(message);
	}
}
