package com.nimbusds.common.id;


import com.nimbusds.jose.util.Base64URL;
import net.jcip.annotations.ThreadSafe;


/**
 * Generator of secure random identifiers protected with a Hash-based (SHA-256)
 * Message Authentication Code (MAC).
 *
 * <p>Implementations must be thread-safe.
 */
@ThreadSafe
public interface IdentifierWithHMACGenerator<T> {
	

	/**
	 * Generates a new secure random identifier with HMAC protection. The
	 * identifier must be of sufficient length and randomness to prevent
	 * collision with other existing identifiers.
	 *
	 * @return The identifier, with HMAC protection.
	 */
	T generate();


	/**
	 * Applies HMAC protection to the specified identifier value.
	 *
	 * @param value The identifier value, BASE64-URL encoded. Must not be
	 *              empty or {@code null}.
	 *
	 * @return The identifier with HMAC protection.
	 */
	T applyHMAC(final Base64URL value);


	/**
	 * Extracts the raw value from the specified HMAC protected identifier
	 * (to be used as a key in database queries, etc). The HMAC is not
	 * checked!
	 *
	 * @param identifier The identifier with HMAC protection. Must not be
	 *                   {@code null}.
	 *
	 * @return The raw identifier value, BASE64-URL encoded.
	 */
	Base64URL extractValue(final T identifier);


	/**
	 * Checks the HMAC of the specified identifier and returns its raw
	 * value (to be used as a key in database queries, etc).
	 *
	 * @param identifier The identifier with HMAC protection. Must not be
	 *                   {@code null}.
	 *
	 * @return The raw identifier value, BASE64-URL encoded.
	 *
	 * @throws InvalidIdentifierException If the identifier format is
	 *                                    illegal or didn't pass the HMAC
	 *                                    check.
	 */
	Base64URL validateAndExtractValue(final T identifier)
		throws InvalidIdentifierException;
}
