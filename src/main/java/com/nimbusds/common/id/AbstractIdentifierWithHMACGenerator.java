package com.nimbusds.common.id;


import com.nimbusds.jose.util.Base64URL;
import net.jcip.annotations.ThreadSafe;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;


/**
 * Abstract generator of secure random identifiers with SHA-256 based Message
 * Authentication Code (MAC) protection.
 */
@ThreadSafe
public abstract class AbstractIdentifierWithHMACGenerator<T> implements IdentifierWithHMACGenerator<T> {


	/**
	 * The HMAC key.
	 */
	private final SecretKey hmacKey;


	/**
	 * Creates a new generator of secure random identifiers with SHA-256
	 * based Message Authentication Code (MAC) protection.
	 *
	 * @param hmacKey The HMAC key, must be 256 bits long.
	 */
	public AbstractIdentifierWithHMACGenerator(final SecretKey hmacKey) {

		if (hmacKey.getEncoded().length != 256 / 8) {
			throw new IllegalArgumentException("The HMAC key must be 256 bits long");
		}

		// Ensure we have the right JCA algorithm
		this.hmacKey = new SecretKeySpec(hmacKey.getEncoded(), "HmacSha256");
	}


	/**
	 * Returns the HMAC key.
	 *
	 * @return The HMAC key.
	 */
	public SecretKey getHMACKey() {

		return hmacKey;
	}


	@Override
	public Base64URL validateAndExtractValue(final T identifier)
		throws InvalidIdentifierException {

		IdentifierWithHMAC id = IdentifierWithHMAC.parseAndValidate(identifier.toString(), getHMACKey());

		return new Base64URL(id.toString().split("\\.")[0]);
	}


	@Override
	public Base64URL extractValue(T identifier) {

		return new Base64URL(identifier.toString().split("\\.")[0]);
	}
}
