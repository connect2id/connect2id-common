package com.nimbusds.common.id;


import com.nimbusds.jose.util.Base64URL;
import net.minidev.json.JSONAware;
import net.minidev.json.JSONObject;

import java.security.SecureRandom;


/**
 * The base class for identifiers (IDs) used in Connect2id software.
 */
public abstract class BaseIdentifier implements Identifier, Comparable<Identifier>, JSONAware {
	
	
	/**
	 * The secure random generator.
	 */
	protected static final SecureRandom SECURE_RANDOM = new SecureRandom();
	
	
	/**
	 * The default byte length of generated identifiers.
	 */
	public static final int DEFAULT_BYTE_LENGTH = 32;

	
	/**
	 * The identifier value. 
	 */
	private final String value;
	
	
	/**
	 * Creates a new unique identifier (ID) based on a secure randomly 
	 * generated 256-bit number, Base64URL-encoded.
	 */
	public BaseIdentifier() {
	
		this(DEFAULT_BYTE_LENGTH);
	}
	
	
	/**
	 * Creates a new unique identifier (ID) based on a secure randomly
	 * generated number with the specified byte length, Base64URL-encoded.
	 */
	public BaseIdentifier(int byteLength) {
	
		byte[] n = new byte[byteLength];
		SECURE_RANDOM.nextBytes(n);
		value = Base64URL.encode(n).toString();
	}
	
	
	/**
	 * Creates a new identifier (ID) from the specified string. The value 
	 * is not validated (for legality or uniqueness) in any way.
	 *
	 * @param value The identifier (ID) value. Must not be {@code null}.
	 */
	public BaseIdentifier(final String value) {
	
		if (value == null)
			throw new IllegalArgumentException("The value must not be null");
	
		this.value = value;
	}


	@Override
	public int compareTo(final Identifier other) {

		return value.compareTo(other.toString());
	}
	
	
	/**
	 * Overrides {@code Object.hashCode()}.
	 *
	 * @return The object hash code.
	 */
	@Override
	public int hashCode() {
	
		return value.hashCode();
	}
	
	
	/**
	 * Overrides {@code Object.equals()}.
	 *
	 * @param object The object to compare to.
	 *
	 * @return {@code true} if the objects have the same value, otherwise
	 *         {@code false}.
	 */
	@Override
	public abstract boolean equals(final Object object);
	
	
	/**
	 * Returns the string representation of this identifier (ID).
	 *
	 * @return The string representation.
	 */
	@Override
	public String toString() {
	
		return value;
	}
	
	
	/**
	 * Returns the JSON string representation of this identifier (ID).
	 * 
	 * @return The JSON string representation.
	 */
	@Override
	public String toJSONString() {
		
		return '"' + JSONObject.escape(value) + '"';
	}
}
