package com.nimbusds.common.config;


import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;


/**
 * Details of a custom key store for client X.509 certificates to be presented 
 * to a remote server.
 *
 * <p>Supports Log4j logging, see {@link #log}.
 *
 * <p>Property keys: [prefix]*
 */
public class CustomKeyStoreConfiguration
	implements LoggableConfiguration {


	/**
	 * If {@code true} a custom key store file must be used for the client 
	 * X.509 certificates to be presented to the remote server (if such 
	 * authentication is required).
	 *
	 * <p>If {@code false} the default key store will be used (if one has 
	 * been provided and correctly configured).
	 *
	 * <p>Property key: [prefix]enable
	 */
	public final boolean enable;


	/**
	 * The file system location of the custom key store file.
	 *
	 * <p>Property key: [prefix]file
	 */
	public final String file;


	/**
	 * The type of the custom key store file, typically "JKS" or "PKCS12". 
	 * An empty or {@code null} string indicates to use the system default 
	 * type.
	 *
	 * <p>Property key: [prefix]type
	 */
	public final String type;


	/**
	 * The password to unlock the custom key store file. An empty or 
	 * {@code null} string indicates that no password is required.
	 *
	 * <p>Property key: [prefix]password
	 */
	public final String password;


	/**
	 * The logger.
	 */
	private final Logger log = LogManager.getLogger(LOG_CATEGORY);


	/**
         * Creates a new custom key store configuration from the specified 
	 * properties.
	 *
	 * <p>Mandatory properties:
	 *
	 * <ul>
	 *     <li>none
	 * </ul>
	 *
	 * <p>Conditionally mandatory properties:
	 *
	 * <ul>
	 *     <li>[prefix]file - if the key store is enabled
	 * </ul>
	 *
	 * <p>Optional properties, with defaults:
	 *
	 * <ul>
	 *     <li>[prefix]enable = false
	 *     <li>[prefix]type = null
	 *     <li>[prefix]password = null
	 * </ul>
         *
         * @param prefix The properties prefix. Must not be {@code null}.
	 * @param props  The properties. Must not be {@code null}.
         *
         * @throws PropertyParseException On a missing or invalid property.
         */
        public CustomKeyStoreConfiguration(final String prefix, final Properties props)
                throws PropertyParseException {

                var pr = new PropertyRetriever(props);

                enable = pr.getOptBoolean(prefix + "enable", false);

                if (enable) {
                        file = pr.getString(prefix + "file");
                        type = pr.getOptString(prefix + "type", null);
                        password = pr.getOptString(prefix + "password", null);
                } else {
                        file = null;
                        type = null;
                        password = null;
                }
        }


	/**
	 * Logs the configuration details at INFO level using Log4j.
	 */
	@Override
	public void log() {

		log.info("[CM1210] Custom key store enabled: {}", enable);

		if (enable) {
			log.info("[CM1211] Custom key store file: {}", file);
			log.info("[CM1212] Custom key store type: {}", type);
		}
	}
}
