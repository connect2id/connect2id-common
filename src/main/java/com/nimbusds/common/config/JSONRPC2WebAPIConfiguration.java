package com.nimbusds.common.config;


import com.nimbusds.common.contenttype.ContentType;
import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.util.List;
import java.util.Properties;


/**
 * JSON-RPC 2.0 web API configuration. Connect2id web services with additional
 * API configuration properties can extend this class.
 *
 * <p>The configuration is stored as public fields which become immutable 
 * (final) after their initialisation.
 *
 * <p>Property keys: [prefix]*
 */
public class JSONRPC2WebAPIConfiguration implements LoggableConfiguration {


	/**
         * The value of the HTTP "Content-Type" header for the JSON-RPC 2.0 
	 * responses.
         *
         * <p>Typically set to {@code application/json;charset=utf-8} or to 
	 * {@code text/plain;charset=utf-8} to support browser CORS requests.
	 * 
	 * <p>Note that the charset must always be defined and set to
	 * {@code utf-8}.
         *
         * <p>Property key: [prefix]responseContentType
         */
	public final String responseContentType;


	/**
         * The default response content type.
         */
        public static final String DEFAULT_RESPONSE_CONTENT_TYPE = 
		"application/json; charset=utf-8";
	
	
	/**
	 * <p>If {@code true} the web service will expose exception details to
	 * clients in the JSON-RPC 2.0 error "data" field. Use this setting for
	 * debugging purposes or if the web clients are trusted.
	 * 
	 * <p>If {@code false} the web service will not provide any exception
	 * details in JSON-RPC 2.0 errors. Use this setting if the web clients 
	 * are not trusted or if they don't need to know details about 
	 * encountered exceptions.
	 *
	 * <p>Property key: [prefix]exposeExceptions
	 */
	public final boolean exposeExceptions;


	/**
	 * The default expose exceptions policy.
	 */
	public static final boolean DEFAULT_EXPOSE_EXCEPTIONS = false;
	
	
	/**
	 * If {@code true} enables reporting of request processing time by 
	 * appending a non-standard "xProcTime" attribute to the JSON-RPC 2.0 
	 * responses.
	 *
	 * <p>Intended for debugging and testing purposes. Disabled by default 
	 * to prevent parse exceptions by clients which don't allow unexpected 
	 * JSON attributes in the JSON-RPC 2.0 response messages.
	 *
	 * <p>Property key: [prefix]reportRequestProcTime
	 */
	public final boolean reportRequestProcTime;


	/**
	 * The default request processing time reporting.
	 */
	public static final boolean DEFAULT_REPORT_REQUEST_PROC_TIME = false;


	/**
	 * Creates a JSON-RPC 2.0 web API configuration from the specified 
	 * properties.
	 *
	 * <p>Mandatory properties:
	 *
	 * <ul>
	 *     <li>none
	 * </ul>
	 *
	 * <p>Optional properties, with defaults:
	 *
	 * <ul>
	 *     <li>[prefix]responseContentType = application/json; charset=utf-8
	 *     <li>[prefix]exposeExceptions = false
	 *     <li>[prefix]reportRequestProcTime =  false
	 *     <li>
	 * </ul>
	 *
	 * @param prefix The properties prefix. Must not be {@code null}.
	 * @param props  The properties. Must not be {@code null}.
	 *
	 * @throws PropertyParseException On a missing or invalid property.
	 */
	public JSONRPC2WebAPIConfiguration(final String prefix, final Properties props)
		throws PropertyParseException {

		var pr = new PropertyRetriever(props);
		
		// Response content type
		ContentType mime;

		try {
			mime = ContentType.parse(pr.getOptString(prefix + "responseContentType",
				                               DEFAULT_RESPONSE_CONTENT_TYPE));

                } catch (ParseException e) {
                        throw new PropertyParseException("Invalid Content-Type header value", 
				                         prefix + "responseContentType");
                }

		List<ContentType.Parameter> mimeParameters = mime.getParameters();

		if (! mimeParameters.contains(new ContentType.Parameter("charset", "utf-8"))) {
			throw new PropertyParseException("The charset parameter of " +
				prefix + "responseContentType must be UTF-8");
		}

		responseContentType = mime.toString();

		// Expose exceptions
		exposeExceptions = pr.getOptBoolean(prefix + "exposeExceptions", 
			                            DEFAULT_EXPOSE_EXCEPTIONS);

		// xProcTime reporting
                reportRequestProcTime = pr.getOptBoolean(prefix + "reportRequestProcTime", 
			                                 DEFAULT_REPORT_REQUEST_PROC_TIME);
	}


	/**
	 * Logs the configuration details at INFO level.
	 */
	@Override
	public void log() {

		Logger log = LogManager.getLogger(LOG_CATEGORY);

		log.info("[CM2100] Web API HTTP response Content-Type: {}", responseContentType);
		log.info("[CM2101] Web API exposes exception details: {}", exposeExceptions);
		log.info("[CM2102] Web API reports request processing time: {}", reportRequestProcTime);
	}
}
