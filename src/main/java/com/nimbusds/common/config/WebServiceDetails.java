package com.nimbusds.common.config;


import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.Properties;


/**
 * Web service connect details.
 *
 * <p>The configuration is stored as public fields which become immutable 
 * (final) after their initialisation.
 *
 * <p>Property keys: [prefix]*
 */
public class WebServiceDetails implements LoggableConfiguration {


	/**
	 * The HTTP(S) URL of the web service.
	 *
	 * <p>Property key: [prefix]url
	 */
	public final URL url;


	/**
	 * Specifies whether to accept self-signed X.509 certificates presented
	 * by the web service (for HTTPS connections).
	 *
	 * <p>Property key: [prefix]trustSelfSignedCerts
	 */
	public final boolean trustSelfSignedCerts;


	/**
	 * The default self-signed certificate policy.
	 */
	public static final boolean DEFAULT_SELF_SIGNED_CERT_TRUST = false;
	
	
	/**
	 * Specifies an HTTP connect timeout for web service requests, in 
	 * milliseconds. Zero implies the option is disabled (timeout of 
	 * infinity).
	 *
	 * <p>Property key: [prefix]connectTimeout
	 */
	public final int connectTimeout;
	
	
	/**
	 * The default HTTP connect timeout for web service requests (disabled).
	 */
	public static final int DEFAULT_CONNECT_TIMEOUT = 0;
	
	
	/**
	 * Specifies an HTTP read timeout for web service requests, in
	 * milliseconds. Zero implies the option is disabled (timeout of
	 * infinity).
	 *
	 * <p>Property key: [prefix]readTimeout
	 */
	public final int readTimeout;
	
	
	/**
	 * The default HTTP read timeout for web service requests (disabled).
	 */
	public static final int DEFAULT_READ_TIMEOUT = 0;
	
	
	/**
	 * Optional API key to include with requests to the web service, 
	 * {@code null} if none.
	 *
	 * <p>Property key: [prefix]apiKey
	 */
	public final String apiKey;


	/**
	 * Creates a new web service details instance from the specified 
	 * properties.
	 *
	 * <p>Mandatory properties:
	 *
	 * <ul>
	 *     <li>[prefix]url
	 * </ul>
	 *
	 * <p>Optional properties, with defaults:
	 *
	 * <ul>
	 *     <li>[prefix]trustSelfSignedCerts = false
	 *     <li>[prefix]connectTimeout = 0
	 *     <li>[prefix]readTimeout = 0
	 *     <li>[prefix]apiKey = null
	 * </ul>
	 *
	 * @param prefix The properties prefix. Must not be {@code null}.
	 * @param props  The properties. Must not be {@code null}.
	 *
	 * @throws PropertyParseException On a missing or invalid property.
	 */
	public WebServiceDetails(final String prefix, final Properties props)
		throws PropertyParseException {

		var pr = new PropertyRetriever(props);
		
		url = pr.getURL(prefix + "url");

		if (! url.getProtocol().equalsIgnoreCase("http") && 
		    ! url.getProtocol().equalsIgnoreCase("https"))
        		throw new PropertyParseException("URL protocol must be either HTTP or HTTPS", 
				                         prefix + "url", 
							 url.toString());

		trustSelfSignedCerts = pr.getOptBoolean(prefix + "trustSelfSignedCerts", 
			                                DEFAULT_SELF_SIGNED_CERT_TRUST);
	
		connectTimeout = pr.getOptInt(prefix + "connectTimeout",
		                              DEFAULT_CONNECT_TIMEOUT);
		
		if (connectTimeout < 0)
			throw new PropertyParseException("The connect timeout value must be zero or positive",
				                         prefix + "connectTimeout");
		
		readTimeout = pr.getOptInt(prefix + "readTimeout",
		                           DEFAULT_READ_TIMEOUT);
	
		if (readTimeout < 0)
			throw new PropertyParseException("The read timeout value must be zero or positive",
			                                 prefix + "readTimeout");
	
		apiKey = pr.getOptString(prefix + "apiKey", null);
	}


	/**
	 * Logs the configuration details at INFO level.
	 */
	@Override
	public void log() {

		Logger log = LogManager.getLogger(LOG_CATEGORY);
		
		log.info("[CM2000] Web service URL: {}", url);
		
		if (url.getProtocol().equalsIgnoreCase("http"))
			log.warn("[CM2001] Web service connection is not protected (plain HTTP), consider using SSL (HTTPS)");

		if (url.getProtocol().equalsIgnoreCase("https"))
			log.info("[CM2002] Self-signed web service certificates are trusted: {}", trustSelfSignedCerts);
	
		if (connectTimeout > 0)
			log.info("[CM2003] Web service HTTP connect timeout: {} ms", connectTimeout);
		else
			log.info("[CM2003] Web service HTTP connect timeout: disabled");
		
		if (readTimeout > 0)
			log.info("[CM2004] Web service HTTP read timeout: {} ms", readTimeout);
		else
			log.info("[CM2004] Web service HTTP read timeout: disabled");
		
		if (apiKey != null)
			log.info("[CM2005] Web service API key: provided");
		else
			log.info("[CM2005] Web service API key: not provided");
	}
}
