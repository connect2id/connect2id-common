package com.nimbusds.common.config;


import java.util.Properties;

import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * LDAP server connection pool details.
 *
 * <p>The configuration is stored as public fields which become immutable 
 * (final) after their initialisation.
 *
 * <p>Property keys: [prefix]*
 */
public class LDAPServerConnectionPoolDetails extends LDAPServerDetails {


	/**
	 * The target connection pool size. Must be greater than zero.
	 *
	 * <p>Property key: [prefix]connectionPoolSize
	 */  
	public final int connectionPoolSize;


	/**
	 * The default target connection pool size (5 connections).
	 */
	public static final int DEFAULT_CONNECTION_POOL_SIZE = 5;


	/**
	 * The initial connection pool size. Must be greater than zero and
	 * less or equal to {@link #connectionPoolSize}.
	 *
	 * <p>Property key: [prefix]connectionPoolInitialSize
	 */
	public final int connectionPoolInitialSize;


	/**
	 * The default initial connection pool size (zero connections).
	 */
	public static final int DEFAULT_CONNECTION_POOL_INITIAL_SIZE = 0;


	/**
	 * The maximum length of time in milliseconds to wait for a connection 
	 * to become available when trying to obtain a connection from the 
	 * pool. A value of zero should be used to indicate that the pool 
	 * should not block at all if no connections are available and that it 
	 * should either create a new connection or throw an exception.
	 *
	 * <p>Property key: [prefix]connectionPoolMaxWaitTime
	 */
	public final int connectionPoolMaxWaitTime;


	/**
	 * The default connection pool maximum wait time, in milliseconds.
	 */
	public static final int DEFAULT_CONNECTION_POOL_MAX_WAIT_TIME = 500;


	/**
	 * The maximum time in milliseconds that a connection in this pool may
	 * be established before it should be closed and replaced with another
	 * connection. A value of zero indicates that no maximum age should be
	 * enforced.
	 *
	 * <p>Property key: [prefix]connectionMaxAge
	 */
	public final long connectionMaxAge;


	/**
	 * The default maximum connection time.
	 */
	public static final int DEFAULT_CONNECTION_MAX_AGE = 0;


	/**
	 * Creates a new LDAP server connection pool details instance from the 
	 * specified properties.
	 *
	 * <p>Mandatory properties:
	 *
	 * <ul>
	 *     <li>[prefix]url
	 * </ul>
	 *
	 * <p>Conditionally mandatory properties:
	 *
	 * <ul>
	 *     <li>[prefix]selectionAlgorithm - if more than one LDAP server 
	 *         URL is specified.
	 * </ul>
	 *
	 * <p>Optional properties, with defaults:
	 *
	 * <ul>
	 *     <li>[prefix]security = STARTTLS
	 *     <li>[prefix]connectTimeout = 0
	 *     <li>[prefix]trustSelfSignedCerts = false
	 *     <li>[prefix]connectionPoolSize = 5
	 *     <li>[prefix]connectionPoolInitialSize = 0
	 *     <li>[prefix]connectionPoolMaxWaitTime = 500
	 *     <li>[prefix]connectionMaxAge = 0
	 * </ul>
	 *
	 * @param prefix The properties prefix. Must not be {@code null}.
	 * @param props  The properties. Must not be {@code null}.
	 *
	 * @throws PropertyParseException On a missing or invalid property.
	 */
	public LDAPServerConnectionPoolDetails(final String prefix, final Properties props)
		throws PropertyParseException {

		this(prefix, props, true);
	}


	/**
	 * Creates a new LDAP server connection pool details instance from the 
	 * specified properties.
	 *
	 * <p>Mandatory properties:
	 *
	 * <ul>
	 *     <li>none
	 * </ul>
	 *
	 * <p>Conditionally mandatory properties:
	 *
	 * <ul>
	 *     <li>[prefix]url
	 *     <li>[prefix]selectionAlgorithm - if more than one LDAP server 
	 *         URL is specified.
	 * </ul>
	 *
	 * <p>Optional properties, with defaults:
	 *
	 * <ul>
	 *     <li>[prefix]security = STARTTLS
	 *     <li>[prefix]connectTimeout = 0
	 *     <li>[prefix]trustSelfSignedCerts = false
	 *     <li>[prefix]connectionPoolSize = 5
	 *     <li>[prefix]connectionPoolInitialSize = 0
	 *     <li>[prefix]connectionPoolMaxWaitTime = 500
	 *     <li>[prefix]connectionMaxAge = 0
	 * </ul>
	 *
	 * @param prefix The properties prefix. Must not be {@code null}.
	 * @param props  The properties. Must not be {@code null}.
	 *
	 * @throws PropertyParseException On a missing or invalid property.
	 */
	public LDAPServerConnectionPoolDetails (final String prefix, final Properties props, final boolean requireURL) 
		throws PropertyParseException {

		super(prefix, props, requireURL);

		var pr = new PropertyRetriever(props);

		connectionPoolSize = pr.getOptInt(prefix + "connectionPoolSize",
			DEFAULT_CONNECTION_POOL_SIZE);

		connectionPoolInitialSize = pr.getOptInt(prefix + "connectionPoolInitialSize",
			DEFAULT_CONNECTION_POOL_INITIAL_SIZE);

		if (connectionPoolInitialSize > connectionPoolSize) {
			throw new PropertyParseException("The LDAP server connection pool initial size must not exceed the max size",
				prefix + "connectionPoolInitialSize", connectionPoolInitialSize + "");
		}
	
		connectionPoolMaxWaitTime = pr.getOptInt(prefix + "connectionPoolMaxWaitTime",
			DEFAULT_CONNECTION_POOL_MAX_WAIT_TIME);

		connectionMaxAge = pr.getOptLong(prefix + "connectionMaxAge",
			DEFAULT_CONNECTION_MAX_AGE);
	}


	/**
	 * Logs the configuration details at INFO level.
	 */
	@Override
	public void log() {

		super.log();

		if (url == null)
			return;

		Logger log = LogManager.getLogger(LOG_CATEGORY);
		
		log.info("[CM1100] LDAP server connection pool size: {}", connectionPoolSize);
		log.info("[CM1103] LDAP server connection pool initial size: {}", connectionPoolInitialSize);
		log.info("[CM1101] LDAP server connection pool max wait time: {} ms", connectionPoolMaxWaitTime);
		log.info("[CM1102] LDAP server connection pool max age: {} ms", connectionMaxAge);
	}
}