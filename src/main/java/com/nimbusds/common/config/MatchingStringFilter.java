package com.nimbusds.common.config;

import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;

import java.util.*;


/**
 * Matching string filter.
 */
public class MatchingStringFilter {


        /**
         * Match all wildcard.
         */
        public static final String WILDCARD = "*";


        private final boolean all;


        private final Set<String> exactMatches;


        /**
         * Creates a new matching string filter. When the configuration is not
         * set the default behaviour is match none.
         *
         * @param propName The name of the property that configures the
         *                 filter.
         * @param props    The properties. Must not be {@code null}.
         *
         * @throws PropertyParseException If parsing of the property failed.
         */
        public MatchingStringFilter(final String propName, final Properties props)
                throws PropertyParseException {

                this(propName, props, new MatchingStringFilter(false));
        }


        /**
         * Creates a new matching string filter.
         *
         * @param propName      The name of the property that configures the
         *                      filter.
         * @param props         The properties. Must not be {@code null}.
         * @param defaultFilter The default filter to use when the
         *                      configuration property is not set.
         *
         * @throws PropertyParseException If parsing of the property failed.
         */
        public MatchingStringFilter(final String propName, final Properties props, final MatchingStringFilter defaultFilter)
                throws PropertyParseException {

                var pr = new PropertyRetriever(props);

                if (pr.getOptString(propName, "").trim().isEmpty()) {
                        all = defaultFilter.all;
                        exactMatches = defaultFilter.exactMatches;
                        return;
                }

                if (WILDCARD.equals(pr.getOptString(propName, "").trim())) {
                        all = true;
                        exactMatches = Collections.emptySet();
                        return;
                }

                List<String> names = pr.getOptStringList(propName, Collections.emptyList());
                all = false;
                exactMatches = new HashSet<>(names);
        }


        /**
         * Creates a new matching string filter.
         *
         * @param all {@code true} to match all, {@code false} to match none.
         */
        public MatchingStringFilter(final boolean all) {
                this.all = all;
                exactMatches = Collections.emptySet();
        }


        /**
         * Filters the matching strings from the specified collection.
         *
         * @param stringList The string collection, {@code null} if not
         *                   specified.
         *
         * @return The matching strings, empty list if none.
         */
        public List<String> filter(final Collection<String> stringList) {

                if (stringList == null || stringList.isEmpty()) {
                        return Collections.emptyList();
                }

                if (all) {
                        return new LinkedList<>(stringList);
                }

                List<String> result = new LinkedList<>();

                for (String string: stringList) {
                        if (string == null) {
                                continue;
                        }
                        if (exactMatches.contains(string)) {
                                result.add(string);
                        }
                }
                return result;
        }


        @Override
        public String toString() {
                return all ? "* (all)" : exactMatches.toString();
        }
}
