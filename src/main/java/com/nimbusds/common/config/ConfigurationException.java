package com.nimbusds.common.config;


/**
* Thrown to indicate a configuration exception. This class is intended to report
* invalid or missing configuration parameters at service startup.
*/
public class ConfigurationException extends RuntimeException {


	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = -2999697764205357423L;
	

	/**
	 * Creates a new configuration exception with the specified message.
	 *
	 * @param message The exception message.
	 */
	public ConfigurationException(final String message) {

		super(message);
	}
	
	
	/**
	 * Creates a new configuration exception with the specified message and
	 * cause.
	 *
	 * @param message The exception message.
	 * @param cause   The exception cause.
	 */
	public ConfigurationException(final String message, final Throwable cause) {

		super(message, cause);
	}
}
