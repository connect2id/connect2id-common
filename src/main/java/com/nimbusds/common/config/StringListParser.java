package com.nimbusds.common.config;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * String list parse utility.
 */
public class StringListParser {
	
	
	/**
	 * Parses a list of strings from the specified strings containing space
	 * or comma separated tokens.
	 *
	 * @param s The string, {@code null} if not specified.
	 *
	 * @return The string list, empty list if the string was {@code null}.
	 */
	public static List<String> parse(final String s) {
		
		if (s == null || s.trim().isEmpty())
			return Collections.emptyList();
		
		return Arrays.asList(s.split("\\s*(,|\\s)\\s*"));
	}
	
	
	/**
	 * Prevents public instantiation.
	 */
	private StringListParser(){}
}
