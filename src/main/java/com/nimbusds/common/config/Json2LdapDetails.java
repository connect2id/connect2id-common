package com.nimbusds.common.config;


import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;


/**
 * Json2Ldap web service connect details.
 *
 * <p>The configuration is stored as public fields which become immutable 
 * (final) after their initialisation.
 *
 * <p>Property keys: [prefix]*
 */
public class Json2LdapDetails extends WebServiceDetails {


	/**
	 * Determines whether to use the default LDAP server specified by the 
	 * Json2Ldap web service.
	 *
	 * <p>If {@code false} the LDAP server details must be specified 
	 * explicitly in the {@link LDAPServerDetails LDAP server 
	 * configuration}.
	 *
	 * <p>Property key: [prefix]useDefaultLDAPServer
	 */
	public final boolean useDefaultLDAPServer;


	/**
	 * The default LDAP server use.
	 */
	public static final boolean DEFAULT_LDAP_SERVER = true;


	/**
	 * Creates a new Json2Ldap details instance from the specified 
	 * properties.
	 *
	 * <p>Mandatory properties:
	 *
	 * <ul>
	 *     <li>[prefix]url
	 * </ul>
	 *
	 * <p>Optional properties, with defaults:
	 *
	 * <ul>
	 *     <li>[prefix]useDefaultLDAPServer = true
	 *     <li>[prefix]trustSelfSignedCerts = false
	 *     <li>[prefix]connectTimeout = 0
	 *     <li>[prefix]readTimeout = 0
	 *     <li>[prefix]apiKey = null
	 * </ul>
	 *
	 * @param prefix The properties prefix. Must not be {@code null}.
	 * @param props  The properties. Must not be {@code null}.
	 *
	 * @throws PropertyParseException On a missing or invalid property.
	 */
	public Json2LdapDetails(final String prefix, final Properties props)
		throws PropertyParseException {

		super(prefix, props);

		var pr = new PropertyRetriever(props);
		
		useDefaultLDAPServer = pr.getOptBoolean(prefix + "useDefaultLDAPServer", 
			                                DEFAULT_LDAP_SERVER);
	}


	/**
	 * Logs the configuration details at INFO level.
	 */
	@Override
	public void log() {

		Logger log = LogManager.getLogger(LOG_CATEGORY);
		
		log.info("[CM2300] Json2Ldap URL: {}", url);
		
		if (url.getProtocol().equalsIgnoreCase("http"))
			log.warn("[CM2301] Json2Ldap connection is not protected (plain HTTP), consider using SSL (HTTPS)");
		
		log.info("[CM2302] Json2Ldap with default LDAP server: {}", useDefaultLDAPServer);

		if (url.getProtocol().equalsIgnoreCase("https"))
			log.info("[CM2303] Self-signed Json2Ldap certificates are trusted: {}", trustSelfSignedCerts);
	
		if (connectTimeout > 0)
			log.info("[CM2304] Json2Ldap HTTP connect timeout: {} ms", connectTimeout);
		else
			log.info("[CM2304] Json2Ldap HTTP connect timeout: disabled");
		
		if (readTimeout > 0)
			log.info("[CM2305] Json2Ldap HTTP read timeout: {} ms", readTimeout);
		else
			log.info("[CM2305] Json2Ldap HTTP read timeout: disabled");
		
		if (apiKey != null)
			log.info("[CM2306] Json2Ldap API key: provided");
		else
			log.info("[CM2306] Json2Ldap API key: not provided");
	}
}
