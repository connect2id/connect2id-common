package com.nimbusds.common.config;


import com.nimbusds.common.monitor.WildCardMetricFilter;
import com.thetransactioncompany.util.PropertyFilter;
import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;
import net.jcip.annotations.Immutable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


/**
 * DropWizard metrics configuration. System property override is enabled.
 *
 * <p>The configuration is stored as public fields which become immutable
 * (final) after their initialisation.
 *
 * <p>Property keys: monitor.*
 *
 * <p>Example properties:
 *
 * <pre>
 * monitor.entryCountCacheTimeout=1800
 * monitor.enableJMX=true
 * monitor.graphite.enable=true
 * monitor.graphite.host=carbon.server.com
 * monitor.graphite.port=2003
 * monitor.graphite.reportInterval=60
 * monitor.graphite.batchSize=100
 * monitor.graphite.prefix=
 * monitor.graphite.ratesTimeUnit=SECONDS
 * monitor.graphite.durationsTimeUnit=MILLISECONDS
 * monitor.graphite.filter.1=authzStore.ldapConnector.*
 * monitor.graphite.filter.2=tokenEndpoint.code.*
 * monitor.graphite.filter.3=tokenEndpoint.refreshToken.*
 * </pre>
 */
@Immutable
public final class MonitorConfiguration implements LoggableConfiguration {


	/**
	 * The prefix for the property names.
	 */
	public static final String PREFIX = "monitor.";
	
	
	/**
	 * The default entry count cache timeout (30 minutes).
	 */
	public static final long DEFAULT_ENTRY_COUNT_CACHE_TIMEOUT = 60 * 30;
	
	
	/**
	 * Timeout for caching entry count results, in seconds. Zero means no
	 * caching, negative disabled readings.
	 */
	public final long entryCountCacheTimeout;


	/**
	 * Enables / disables JMX reporting.
	 */
	public final boolean enableJMX;


	/**
	 * Graphite reporting configuration.
	 */
	public static final class Graphite implements LoggableConfiguration {


		/**
		 * Enables / disables reporting.
		 */
		public final boolean enable;


		/**
		 * The name / IP address of the Carbon server host.
		 */
		public final String host;


		/**
		 * The port of the Carbon server.
		 */
		public final int port;


		/**
		 * The report interval, in seconds.
		 */
		public final int reportInterval;


		/**
		 * Controls batching (pickling) of metrics to the Carbon
		 * server, zero if disabled.
		 */
		public final int batchSize;


		/**
		 * Prefix for the metrics that are sent to the Carbon server.
		 * May be used to send a password or other credential to the
		 * server.
		 */
		public final String prefix;


		/**
		 * The rates time unit.
		 */
		public final TimeUnit ratesTimeUnit;


		/**
		 * The durations time unit.
		 */
		public final TimeUnit durationsTimeUnit;


		/**
		 * The metrics filter (white list with wild card support).
		 */
		public final WildCardMetricFilter filter;


		/**
		 * Creates a new Graphite reporting configuration.
		 *
		 * @param  props The properties. Must not be {@code null}.
		 *
		 * @throws PropertyParseException On a missing or invalid
		 *                                property.
		 */
		public Graphite(final Properties props)
			throws PropertyParseException {

			var pr = new PropertyRetriever(props, true);

			enable = pr.getOptBoolean(PREFIX + "graphite.enable", false);

			if (! enable) {
				host = null;
				port = 0;
				reportInterval = 0;
				batchSize = 0;
				prefix = null;
				ratesTimeUnit = null;
				durationsTimeUnit = null;
				filter = null;
				return;
			}

			host = pr.getString(PREFIX + "graphite.host");
			port = pr.getInt(PREFIX + "graphite.port");
			reportInterval = pr.getInt(PREFIX + "graphite.reportInterval");
			batchSize = pr.getInt(PREFIX + "graphite.batchSize");
			prefix = pr.getOptString(PREFIX + "graphite.prefix", null);
			ratesTimeUnit = pr.getEnum(PREFIX + "graphite.ratesTimeUnit", TimeUnit.class);
			durationsTimeUnit = pr.getEnum(PREFIX + "graphite.durationsTimeUnit", TimeUnit.class);

			List<String> filterWhiteList = new LinkedList<>();

			for (String key: props.stringPropertyNames()) {

				if (key.startsWith(PREFIX + "graphite.filter")) {

					String filterEntry = props.getProperty(key);

					if (filterEntry == null || filterEntry.trim().isEmpty()) {
						continue; // skip
					}

					filterWhiteList.add(filterEntry.trim());
				}
			}

			filter = new WildCardMetricFilter(Collections.unmodifiableList(filterWhiteList));
		}


		@Override
		public void log() {

			Logger log = LogManager.getLogger(LOG_CATEGORY);

			log.info("[CM7002] Graphite reporting enabled: {}", enable);

			if (! enable) {
				return;
			}

			log.info("[CM7003] Graphite reporting host: {}", host);
			log.info("[CM7004] Graphite reporting port: {}", port);
			log.info("[CM7005] Graphite reporting interval: {}", reportInterval);
			log.info("[CM7006] Graphite reporting batch size: {}", batchSize + (batchSize < 1 ? " disabled" : ""));
			log.info("[CM7007] Graphite reporting prefix: {}", prefix);
			log.info("[CM7008] Graphite reporting rates time unit: {}", ratesTimeUnit);
			log.info("[CM7009] Graphite reporting durations time unit: {}", durationsTimeUnit);

			final String filterSetting;

			if (filter.matchesAny()) {
				filterSetting = "ANY";
			} else if (filter.matchesNone()) {
				filterSetting = "NONE";
			} else {
				filterSetting = filter.getWhiteList().toString();
			}

			log.info("[CM7010] Graphite reporting filter: {}", filterSetting);
		}
	}


	/**
	 * The Graphite reporting configuration.
	 */
	public final Graphite graphite;


	/**
	 * Creates a new monitoring configuration from the specified
	 * properties.
	 *
	 * @param props The properties. Must not be {@code null}.
	 *
	 * @throws ConfigurationException On a missing or invalid property.
	 */
	public MonitorConfiguration(final Properties props)
		throws ConfigurationException {

		var pr = new PropertyRetriever(props, true);

		try {
			entryCountCacheTimeout = pr.getOptLong(PREFIX + "entryCountCacheTimeout", DEFAULT_ENTRY_COUNT_CACHE_TIMEOUT);
			enableJMX = pr.getOptBoolean(PREFIX + "enableJMX", false);
			graphite = new Graphite(props);
		} catch (PropertyParseException e) {
			throw new ConfigurationException(e.getMessage() + ": Property: " + e.getPropertyKey(), e);
		}
	}

	
	/**
	 * Logs the configuration details at INFO level.
	 */
	@Override
	public void log() {

		Logger log = LogManager.getLogger(LOG_CATEGORY);
		log.info("[CM7000] Overriding system properties: {}", PropertyFilter.filterWithPrefix(PREFIX, System.getProperties()).stringPropertyNames());
		log.info("[CM7020] Entry count cache timeout: {}s", entryCountCacheTimeout);
		log.info("[CM7001] JMX reporting enabled: {}", enableJMX);
		graphite.log();
	}
}
