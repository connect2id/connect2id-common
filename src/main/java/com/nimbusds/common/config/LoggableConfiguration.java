package com.nimbusds.common.config;


/**
 * Loggable configuration.
 */
public interface LoggableConfiguration {


	/**
	 * The preferred logging category.
	 */
	String LOG_CATEGORY = "MAIN";


	/**
	 * Logs the configuration properties at INFO level.
	 */
	void log();
}
