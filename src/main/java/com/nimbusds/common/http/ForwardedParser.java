package com.nimbusds.common.http;


/**
 * Parses reverse HTTP proxy headers to determine the client IP address.
 */
public class ForwardedParser {
	
	
	/**
	 * Returns the client IP address from a X-Forwarded-For style HTTP
	 * header.
	 *
	 * @param header The HTTP header, {@code null} if not specified.
	 *
	 * @return The client IP address, {@code null} if parsing failed.
	 */
	public static String getClientIPAddressFromXForwardedForHeader(final String header) {
		
		if (header == null || header.trim().isEmpty()) {
			return null;
		}
	
		String[] parts = header.split(",");
		
		String clientIP = parts[0].trim();
		
		if (clientIP.isEmpty()) {
			return null;
		}
		
		return clientIP;
	}
	
	
	/**
	 * Returns the client IP address from a {@code Forwarded} (RFC 7239)
	 * HTTP header.
	 *
	 * @param header The HTTP header, {@code null} if not specified.
	 *
	 * @return The client IP address, {@code null} if parsing failed.
	 */
	public static String getClientIPAddressFromForwardedHeader(final String header) {
		
		if (header == null || header.trim().isEmpty()) {
			return null;
		}
	
		String[] tokens = header.split(";");
		
		for (String t: tokens) {
			
			if (t.toLowerCase().startsWith("for=")) {
				
				String value = t.substring(4);
				
				// strip double quotes
				if (value.startsWith("\"") && value.endsWith("\"") && value.length() >= 2)
					value = value.substring(1, value.length() -1 );
				
				return value;
			}
		}
		
		return null;
	}
	
	
	/**
	 * Prevents public instantiation.
	 */
	private ForwardedParser() {}
}
