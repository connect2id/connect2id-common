package com.nimbusds.common.ldap;


import java.text.ParseException;
import java.util.List;
import java.util.Map;

import com.unboundid.asn1.ASN1OctetString;
import com.unboundid.ldap.sdk.controls.ServerSideSortRequestControl;
import com.unboundid.ldap.sdk.controls.SimplePagedResultsControl;
import com.unboundid.ldap.sdk.controls.SortKey;
import com.unboundid.ldap.sdk.controls.VirtualListViewRequestControl;
import com.unboundid.util.Base64;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.util.NamedParamsRetriever;


/**
 * LDAP control request parser. Provides parsing of JSON-based requests for the
 * following controls:
 *
 * <ul>
 *     <li>Server-side-sort request control (RFC 2891).
 *     <li>Simple paged results control (RFC 2696).
 *     <li>Virtual list view control (draft-ietf-ldapext-ldapv3-vlv)
 * </ul>
 */
public class LDAPControlRequestParser {


	/**
	 * Parses a JSON array (java.util.List) representing a server-side sort 
	 * (RFC 2891) request.
	 *
	 * <p>Example:
	 *
	 * <pre>
	 * [ { "key"          : "sn", 
	 *     "reverseOrder" : false, 
	 *     "orderingRule" : null  }, 
	 *   { "key"          : "givenName",
	 *     "reverseOrder" : false,
	 *     "orderingRule" : null } ]
	 * </pre>
	 *
	 * @param sortParams JSON array of JSON objects with properties "key" 
	 *                   (mandatory string), "reverseOrder" (optional 
	 *                   boolean, defaults to false) and "orderingRule"
	 *                   (optional string, defaults to null). If 
	 *                   {@code null} no control is requested.
	 *
	 * @return The resulting server-side sort request control or 
	 *         {@code null} if none is requested.
	 *
	 * @throws JSONRPC2Error If the parameters are invalid.
	 */
	@SuppressWarnings("unchecked")
	public static ServerSideSortRequestControl 
		parseServerSideSortRequestControl(final List<Object> sortParams)
		throws JSONRPC2Error {
	
		if (sortParams == null || sortParams.isEmpty())
			return null;
		
		SortKey[] sortKeys = new SortKey[sortParams.size()];
		
		for (int i=0; i < sortParams.size(); i++) {
			
			Map<String,Object> entry;
			
			try {
				entry = (Map<String,Object>)sortParams.get(i);
			
			} catch (Exception e) {

				throw JSONRPC2Error.INVALID_PARAMS.
					appendMessage(": Invalid server-side sort control specification");
			}
				
			NamedParamsRetriever np = new NamedParamsRetriever(entry);
			
			String key = np.getString("key");
			boolean reverseOrder = np.getOptBoolean("reverseOrder", false);
			String orderingRule = np.getOptString("orderingRule", true, null);
				
			sortKeys[i] = new SortKey(key, orderingRule, reverseOrder);
		} 
		
		// Cause LDAP error on unsupported control
		boolean isCritical = true;
		
		return new ServerSideSortRequestControl(isCritical, sortKeys);
	}


	/**
	 * Parses a JSON object (java.util.Map) representing a simple paged 
	 * results (RFC 2696) request.
	 *
	 * <p>Example:
	 *
	 * <pre>
	 * { "size"   : 25, 
	 *   "cookie" : "AAAAAAAAABw=" }
	 * </pre>
	 *
	 * @param pageParams JSON object with properties "size" (mandatory
	 *                   integer, must be positive) and "cookie" (optional 
	 *                   BASE64 - encoded string, defaults to empty 
	 *                   string). If {@code null} no control is requested.
	 *
	 * @return The resulting simple paged results control or {@code null}
	 *         if none is requested.
	 *
	 * @throws JSONRPC2Error If the parameters are invalid.
	 */
	public static SimplePagedResultsControl 
		parseSimplePagedResultsControl(final Map<String,Object> pageParams)
		throws JSONRPC2Error {
	
		if (pageParams == null)
			return null;
			
		NamedParamsRetriever np = new NamedParamsRetriever(pageParams);
		
		// Parse page size, must be positive
		int size = np.getInt("size");
		
		if (size < 1)
			throw JSONRPC2Error.INVALID_PARAMS.
				appendMessage(": Invalid simple paged results control specification: Size must be positive");
		
		
		// Parse page cookie, defaults to empty string
		ASN1OctetString cookie = null;
		
		String cookieBase64 = np.getOptString("cookie", true, null);
		
		try {
			if (cookieBase64 != null)
				cookie = new ASN1OctetString(Base64.decode(cookieBase64));
				
		} catch (ParseException e) {
			
			throw JSONRPC2Error.INVALID_PARAMS.
				appendMessage(": Invalid simple paged results control specification: Unexpected cookie format");
		}
		
		// Cause LDAP error on unsupported control
		boolean isCritical = true;
		
		return new SimplePagedResultsControl(size, cookie, isCritical);
	}


	/**
	 * Parses a JSON object (java.util.Map) representing a virtual list
	 * view (draft-ietf-ldapext-ldapv3-vlv-09) request.
	 *
	 * <p>Minimally specified example:
	 *
	 * <pre>
	 * { "after" : 9 }
	 * </pre>
	 *
	 * <p>Fully specified example:
	 *
	 * <pre>
	 * { "offset"          : 11, 
	 *   "before"          : 0,
	 *   "after"           : 9,
	 *   "totalEntryCount" : 123,
	 *   "cookie"          : "AAAAAAAAABw=" }
	 * </pre>
	 *
	 * @param vlvParams JSON object with properties "offset" (optional
	 *                  integer, must be positive, defaults to one), 
	 *                  "before" (optional integer, must be zero or 
	 *                  positive, defaults to zero), "after" (mandatory
	 *                  integer, must be zero or positive), 
	 *                  "totalEntryCount" (optional integer, must be zero 
	 *                  or positive, defaults to zero) and "cookie" 
	 *                  (optional BASE64 - encoded string, defaults to 
	 *                  empty string). If {@code null} no control is 
	 *                  requested.
	 *
	 * @return The resulting virtual list view control or {@code null}
	 *         if none is requested.
	 *
	 * @throws JSONRPC2Error If the parameters are invalid.
	 */
	public static VirtualListViewRequestControl
		parseVirtualListViewControl(final Map<String,Object> vlvParams)
		throws JSONRPC2Error {

		if (vlvParams == null)
			return null;

		NamedParamsRetriever np = new NamedParamsRetriever(vlvParams);

		// Parse offset
		int offset = np.getOptInt("offset", 1);

		if (offset < 1)
			throw JSONRPC2Error.INVALID_PARAMS.
				appendMessage(": Invalid virtual list view control specification: Offset must be positive (1 based)");

		int before = np.getOptInt("before", 0);

		if (before < 0)
			throw JSONRPC2Error.INVALID_PARAMS.
				appendMessage(": Invalid virtual list view control specification: Before count must zero or positive");

		int after = np.getInt("after");

		if (after < 0)
			throw JSONRPC2Error.INVALID_PARAMS.
				appendMessage(": Invalid virtual list view control specification: After count must zero or positive");

		int totalEntryCount = np.getOptInt("totalEntryCount", 0);

		if (totalEntryCount < 0)
			throw JSONRPC2Error.INVALID_PARAMS.
				appendMessage(": Invalid virtual list view control specification: Total entry count estimate must zero or positive");

		// Parse cookie (context ID), defaults to empty string
		ASN1OctetString cookie = null;
		
		String cookieBase64 = np.getOptString("cookie", true, null);
		
		try {
			if (cookieBase64 != null)
				cookie = new ASN1OctetString(Base64.decode(cookieBase64));
				
		} catch (ParseException e) {
			
			throw JSONRPC2Error.INVALID_PARAMS.
				appendMessage(": Invalid virtual list view control specification: Unexpected cookie format");
		}

		// Cause LDAP error on unsupported control
		boolean isCritical = true;

		return new VirtualListViewRequestControl(offset, before, after, totalEntryCount, cookie, isCritical);
	}


	/**
	 * Prevents instantiation.
	 */
	private LDAPControlRequestParser() {

		// Nothing to do
	}
}