package com.nimbusds.common.ldap;


import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPException;


/**
 * LDAP filter template. This class is immutable.
 *
 * <p>Example filter template with a "%u" placeholder:
 *
 * <pre>
 * (|(uid=%u)(mail=%u))
 * </pre>
 */
public class FilterTemplate {


	/**
	 * The filter template.
	 */
	private final String template;
	
	 
	/**
	 * The placeholder.
	 */
	private final String placeholder;
	
	
	/**
	 * Creates a new filter template.
	 *
	 * @param template    The filter template. Must contain one or more
	 *                    placeholders. Must not be {@code null}.
	 * @param placeholder The placeholder, e.g. "%u". Must not be 
	 *                    {@code null}.
	 *
	 * @throws IllegalArgumentException If the template doesn't contain at 
	 *                                  least one placeholder or doesn't 
	 *                                  represent a valid LDAP filter.
	 */
	public FilterTemplate(final String template, final String placeholder) {
	
		if (template == null)
			throw new IllegalArgumentException("The template must not be null");
			
		this.template = template;
	
		if (placeholder == null)
			throw new IllegalArgumentException("The placeholder must not be null");
		
		this.placeholder = placeholder;
		
		
		// Validate
		if (!template.contains(placeholder))
			throw new IllegalArgumentException("Filter template must contain one or more placeholders");
		
		
		String testParam = "user001";
		
		String filter = template.replaceAll(placeholder, testParam);
		
		try {
			Filter.create(filter);
			
		} catch (LDAPException e) {

			throw new IllegalArgumentException("Invalid filter template: " + e.getMessage(), e);
		}
	}
	
	
	/**
	 * Creates a new filter template with a "%u" placeholder.
	 *
	 * @param template The filter template. Must contain one or more
	 *                 "%u" placeholders. Must not be {@code null}.
	 *
	 * @throws IllegalArgumentException If the template doesn't contain at 
	 *                                  least one placeholder or doesn't 
	 *                                  represent a valid LDAP filter.
	 */
	public FilterTemplate(final String template) {
	
		this(template, "%u");
	}
	
	
	/**
	 * Gets the template.
	 *
	 * @return The template.
	 */
	public String getTemplate() {
	
		return template;
	}
	
	
	/**
	 * Gets the placeholder.
	 *
	 * @return The placeholder.
	 */
	public String getPlaceholder() {
	
		return placeholder;
	}


	/**
	 * Applies the specified parameter to this filter template and returns
	 * the resulting LDAP filter string. Special filter characters in the
	 * parameter are escaped.
	 *
	 * <p>Example:
	 *
	 * <p>Template: "(|(uid=%u)(mail=%u))"
	 * 
	 * <p>Placeholder: "%u"
	 *
	 * <p>Parameter: "alice"
	 *
	 * <p>Resulting filter: "(|(uid=alice)(mail=alice))"
	 *
	 * @param param The parameter to apply. Must not be {@code null}.
	 *
	 * @return The resulting LDAP filter string.
	 */
	public String apply(final String param) {
	
		// Escape special chars
		String sanitizedParam = Filter.encodeValue(param);
		
		return template.replaceAll(placeholder, sanitizedParam);
	}
	
	
	/**
	 * Returns a string representation of this filter template.
	 *
	 * @see #getTemplate
	 *
	 * @return The string representation of this filter template.
	 */
	@Override
	public String toString() {
	
		return getTemplate();
	}
}
