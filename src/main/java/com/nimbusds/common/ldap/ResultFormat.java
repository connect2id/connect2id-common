package com.nimbusds.common.ldap;


/**
 * Enumeration of LDAP response result formats.
 */
public enum ResultFormat {

	/**
	 * The result is formatted as JSON.
	 */
	JSON,
	
	
	/**
	 * The result is formatted as LDIF.
	 */
	LDIF
}
