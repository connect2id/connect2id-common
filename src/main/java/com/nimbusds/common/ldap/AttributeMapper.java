package com.nimbusds.common.ldap;


import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.minidev.json.JSONObject;

import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.util.StaticUtils;

import com.nimbusds.langtag.LangTag;
import com.nimbusds.langtag.LangTagUtils;
import net.minidev.json.JSONValue;


/**
 * LDAP attribute mapper. Maps an LDAP entry to a JSON object (and vice versa), 
 * supports attribute renaming and type conversion.
 *
 * <p>Transformation map specification:
 *
 * <ul>
 *     <li>Each map entry specifies the transformation of a single LDAP 
 *         attribute to / from a JSON object member.
 *     <li>The map key represents the JSON object member name. May contain a
 *         single dot to specify a nested JSON object member.
 *     <li>The map value is a map of the following properties:
 *         <ul>
 *             <li>The "ldapAttr" property specifies the LDAP attribute name.
 *             <li>The "ldapValue" property specifies a constant value that is
 *                 always returned instead of looking up the "ldapAttr"
 *                 property.
 *             <li>The optional "ldapType" property specifies the LDAP
 *                 attribute type: "string", "time" or "boolean", defaults to
 *                 "string".
 *             <li>The optional "jsonType" property specifies the JSON value
 *                 type: "string", "string-array", "int", "long", "boolean" or
 *                 "object", defaults to "string".
 *             <li>The optional "langTag" property specifies if the  "string" 
 *                 values may be language-tagged: true or false, defaults to 
 *                 false.
 *             <li>The optional "reMatch" property specifies a regular
 *                 expression matching pattern to apply to the LDAP attribute
 *                 values. Must contain one matching group. Applies only to
 *                 LDAP attributes of type "string".
 *             <li>The optional "split" property, if set to {@code true},
 *                 causes a JSON string value to be split at each space
 *                 character and converted to multiple LDAP attribute values.
 *                 On reverse transformation the multiple LDAP attribute values
 *                 are joined by a space character into a single JSON string.
 *                 Applies only to LDAP attributes of type "string".
 *             <li>The optional "default" property causes an attribute to be
 *                 skipped if its value matches the default one.
 *         </ul>
 *     </li>
 * </ul>
 * 
 * <p>The "time" LDAP attribute type is defined in 
 * <a href="http://tools.ietf.org/html/rfc4517#section-3.3.13">RFC 4517, 
 * section 3.3.13</a>. Can only be mapped to positive long integer JSON values
 * representing the number of milliseconds since the Unix epoch.
 *
 * <p>Example transformation map (as JSON object):
 *
 * <pre>
 * {
 *   "email"    : { "ldapAttr" : "mail" },
 *   "age"      : { "ldapAttr" : "personAge", "jsonType" : "int" },
 *   "name"     : { "ldapAttr" : "cn", "langTag" : true },
 *   "active"   : { "ldapAttr" : "accountActive", "jsonType" : "boolean" },
 *   "updated"  : { "ldapAttr" : "updateTime", "ldapType" : "time", "jsonType" : "long" },
 *   "groups"   : { "ldapAttr" : "isMemberOf", "jsonType" : "string-array", "reMatch" : "cn=([a-zA-Z_0-9 ]+),.*" },
 *   "scope"    : { "ldapAttr" : "scopeValues", "split" : true },
 *   "verified" : { "ldapValue" : "TRUE", "jsonType" : "boolean" },
 *   "geo.lat"  : { "ldapAttr" : "latitude" },
 *   "geo.long" : { "ldapAttr" : "longitude" }
 * }
 * </pre>
 */
public class AttributeMapper {


	/**
	 * The space split pattern.
	 */
	public static final Pattern SPLIT_PATTERN = Pattern.compile(" ");


	/**
	 * JSON object member key. This class is immutable.
	 */
	private static final class Key {


		/**
		 * The raw key value.
		 */
		private final String value;


		/**
		 * The key value parts.
		 */
		private final String[] parts;


		/**
		 * Creates a new JSON object member key.
		 *
		 * @param value The key value. May contain a single dot to
		 *              specify a nested JSON object member. Must not
		 *              be {@code null}.
		 */
		public Key(final String value) {

			if (value == null)
				throw new IllegalArgumentException("The JSON object member key must not be null");

			this.value = value;

			parts = value.split("\\.");

			if (parts.length > 2)
				throw new IllegalArgumentException("Too many dots in JSON object member key");
		}


		/**
		 * Returns the value of this JSON object member key.
		 *
		 * @return The key value.
		 */
		public String value() {

			return value;
		}


		/**
		 * Checks if this key specifies nested JSON object member(s).
		 *
		 * @return {@code true} if the key is nested, else
		 *         {@code false}.
		 */
		public boolean isNested() {

			return parts.length > 1;
		}


		/**
		 * Returns the parts of this JSON object member key.
		 *
		 * @return The key parts, an array of size one if the key is
		 *         not nested.
		 */
		public String[] parts() {

			return parts;
		}


		@Override
		public String toString() {

			return value;
		}


		@Override
		public int hashCode() {

			return value.hashCode();
		}


		@Override
		public boolean equals(final Object other) {

			return other instanceof Key &&
			       toString().equals(other.toString());
		}

	}
	
	
	/**
	 * Specifies an individual directive for mapping between an LDAP 
	 * attribute and a JSON value. This class is immutable.
	 */
	private static final class Directive {
	
		
		/**
		 * The type of the JSON value.
		 */
		private final String jsonType;
		
		
		/**
		 * The name of the LDAP attribute.
		 */
		private final String ldapAttr;


		/**
		 * Constant value to return if an LDAP attribute name is not
		 * specified.
		 */
		private final String ldapValue;
		
		
		/**
		 * The type of the LDAP attribute.
		 */
		private final String ldapType;
		
		
		/**
		 * Specifies if the attribute is language tagged.
		 */
		private final boolean langTagged;


		/**
		 * Optional regular expression matching patter to apply to the
		 * LDAP value(s).
		 */
		private final Pattern matchPattern;


		/**
		 * Optional split setting (join on reverse transformation).
		 */
		private final boolean split;


		/**
		 * Optional default value.
		 */
		private final Object defaultValue;


		/**
		 * Creates a new attribute mapping directive.
		 * 
		 * @param ldapAttr     The name of the LDAP attribute, must not
		 *                     be {@code null}.
		 * @param ldapValue    Constant value to return if an LDAP
		 *                     attribute is not specified.
		 * @param ldapType     The type of the LDAP attribute,
		 *                     {@code null} if not specified (implies
		 *                     "string").
		 * @param jsonType     The type of JSON value, {@code null} if
		 *                     not specified (implies "string").
		 * @param langTagged   If {@code true} the values may be
		 *                     language tagged, else not.
		 * @param matchPattern Optional regular expression matching
		 *                     pattern to apply to the LDAP value(s),
		 *                     {@code null} if not specified.
		 * @param split        Optional split setting, if {@code true}
		 *                     causes a JSON string value to be split
		 *                     at each space character and converted
		 *                     to multiple LDAP attribute values. On
		 *                     reverse transformation the multiple LDAP
		 *                     attribute values are joined by a space
		 *                     character into a single JSON string.
		 * @param defaultValue Optional value, causes an attribute to
		 *                     be skipped if it values matches the
		 *                     default one.
		 */
		public Directive(final String ldapAttr, 
			         final String ldapValue,
				 final String ldapType,
				 final String jsonType,
				 final boolean langTagged,
				 final Pattern matchPattern,
				 final boolean split,
				 final Object defaultValue) {
			
			if (ldapAttr == null && ldapValue == null)
				throw new IllegalArgumentException("Missing LDAP attribute name / value");

			if (ldapAttr != null && ldapValue != null)
				throw new IllegalArgumentException("Either an LDAP attribute name or an LDAP value must be specified, but not both");
			
			this.ldapAttr = ldapAttr;

			this.ldapValue = ldapValue;
			
			
			if (ldapType == null)
				this.ldapType = "string";
			else
				this.ldapType = ldapType;
			
			if (jsonType == null)
				this.jsonType = "string";
			else
				this.jsonType = jsonType;
			
			this.langTagged = langTagged;

			this.matchPattern = matchPattern;

			this.split = split;

			this.defaultValue = defaultValue;
		}
		
		
		/**
		 * Returns the name of the LDAP attribute.
		 * 
		 * @return The LDAP attribute name, {@code null} if an LDAP
		 *         constant value is specified instead.
		 */
		public String ldapAttributeName() {
			
			return ldapAttr;
		}


		/**
		 * Returns the constant LDAP value.
		 *
		 * @return The constant LDAP value, {@code null} if an LDAP
		 *         attribute name is specified instead.
		 */
		public String ldapValue() {

			return ldapValue;
		}
		
		
		/**
		 * Returns the type of the LDAP attribute.
		 * 
		 * @return The LDAP attribute type.
		 */
		public String ldapAttributeType() {
			
			return ldapType;
		}
		
		
		/**
		 * Returns the type of the JSON value.
		 * 
		 * @return The JSON value type.
		 */
		public String jsonValueType() {
			
			return jsonType;
		}
		
		
		/**
		 * Returns {@code true} if the values may be language-tagged.
		 * 
		 * @return {@code true} if the values may be language-tagged.
		 */
		public boolean langTagged() {
			
			return langTagged;
		}


		/**
		 * Returns the regular expression matching pattern for the
		 * LDAP value(s).
		 *
		 * @return The regular expression matching pattern,
		 *         {@code null} if not specified.
		 */
		public Pattern ldapValueMatchingPattern() {

			return matchPattern;
		}


		/**
		 * Returns the split setting.
		 *
		 * @return The split setting.
		 */
		public boolean split() {

			return split;
		}


		/**
		 * Returns the default value.
		 *
		 * @return The default value, {@code null} if none.
		 */
		public Object defaultValue() {

			return defaultValue;
		}
		
		
		/**
		 * Parses an attribute mapping directive from the specified map
		 * representation.
		 * 
		 * <p>Example map representation (as JSON object):
		 * 
		 * <pre>
		 * { "ldapAttr" : "updateTime", "ldapType" : "time", "jsonType" : "long" },
		 * </pre>
		 * 
		 * @param map The map to parse. Must not be {@code null}.
		 * 
		 * @return The attribute mapping directive.
		 * 
		 * @throws ParseException If parsing of the map failed.
		 */
		public static Directive parse(final Map<String,Object> map)
			throws ParseException {

			// Parse LDAP attribute name
			String ldapAttrName;

			try {
				ldapAttrName = (String)map.get("ldapAttr");

			} catch (Exception e) {

				throw new ParseException("Invalid LDAP attribute name", 0);
			}

			// Parse LDAP attribute value
			String ldapValue;

			try {
				ldapValue = (String)map.get("ldapValue");

			} catch (Exception e) {

				throw new ParseException("Invalid LDAP value", 0);
			}

			if (ldapAttrName == null && ldapValue == null)
				throw new ParseException("Missing LDAP attribute name / value", 0);

			if (ldapAttrName != null && ldapValue != null)
				throw new ParseException("Either an LDAP attribute name or an LDAP value must be specified, but not both", 0);


			// Parse LDAP attribute type
			String ldapAttrType = null;

			if (map.containsKey("ldapType")) {

				try {
					ldapAttrType = (String)map.get("ldapType");
				
				} catch (Exception e) {

					throw new ParseException("Invalid LDAP attribute type", 0);
				}
			}

			if (ldapAttrType == null)
				ldapAttrType = "string";

			if (! ldapAttrType.equals("string") &&
			    ! ldapAttrType.equals("time") &&
			    ! ldapAttrType.equals("boolean"))
				throw new ParseException("Invalid attribute LDAP type, must be \"string\", \"time\" or \"boolean\"", 0);
			
			
			// Parse JSON value type
			String jsonType = null;
			
			if (map.containsKey("jsonType")) {

				try {
					jsonType = (String)map.get("jsonType");
				
				} catch (Exception e) {

					throw new ParseException("Invalid JSON value type", 0);
				}
			}

			if (jsonType == null)
				jsonType = "string";

			if (! jsonType.equals("string")       &&
			    ! jsonType.equals("string-array") &&
			    ! jsonType.equals("int")          &&
			    ! jsonType.equals("long")         &&
			    ! jsonType.equals("boolean")      &&
			    ! jsonType.equals("object")         )
				throw new ParseException("Invalid JSON value type, must be \"string\", \"string-array\", \"int\", \"long\",  \"boolean\" or \"object\"", 0);
			
			boolean langTagged = false;
			
			if (map.containsKey("langTag")) {
				
				try {
					langTagged = (Boolean)map.get("langTag");
					
				} catch (Exception e) {
					
					throw new ParseException("Invalid language tag option, must be boolean true or false", 0);
				}
			}

			// Parse regexp matcher
			Pattern reMatch = null;

			if (map.containsKey("reMatch")) {

				try {
					reMatch = Pattern.compile((String)map.get("reMatch"));

				} catch (Exception e) {

					throw new ParseException("Invalid regular expression matching pattern for the LDAP value", 0);
				}

				Matcher m = reMatch.matcher("test");

				if (m.groupCount() != 1)
					throw new ParseException("The regular expression matching pattern for the LDAP value must contain one capturing group", 0);
			}

			// Parse split setting
			boolean split = false;

			if (map.containsKey("split")) {

				try {
					split = ((Boolean)map.get("split"));

				} catch (Exception e) {

					throw new ParseException("Invalid split setting, must be true or false", 0);
				}
			}

			// Default value
			Object defaultValue = null;

			if (map.containsKey("default")) {

				defaultValue = map.get("default");
			}
			
			return new Directive(ldapAttrName, ldapValue, ldapAttrType, jsonType, langTagged, reMatch, split, defaultValue);
		}
	}


	/**
	 * The attribute transformation map.
	 */
	private final Map<Key,Directive> transformMap;


	/**
	 * The derived names of the LDAP attributes.
	 */
	private final String[] ldapAttributes;


	/**
	 * Parses a map of attribute mapping directives from the specified map
	 * representation.
	 *
	 * <p>Example map representation (as JSON object);
	 *
	 * <pre>
	 * {
	 *   "email"    : { "ldapAttr" : "mail" },
	 *   "age"      : { "ldapAttr" : "personAge", "jsonType" : "int" },
	 *   "name"     : { "ldapAttr" : "cn", "langTag" : true },
	 *   "active"   : { "ldapAttr" : "accountActive", "jsonType" : "boolean" },
	 *   "updated"  : { "ldapAttr" : "updateTime", "ldapType" : "time", "jsonType" : "long" },
	 *   "groups"   : { "ldapAttr" : "isMemberOf", "jsonType" : "string-array", "reMatch" : "cn=([a-zA-Z_0-9 ]+),.*" },
	 *   "scope"    : { "ldapAttr" : "scopeValues", "split" : true },
	 *   "verified" : { "ldapValue" : "true", "jsonType" : "boolean" },
	 *   "geo.lat"  : { "ldapAttr" : "latitude" },
	 *   "geo.long" : { "ldapAttr" : "longitude" }
	 * }
	 * </pre>
	 *
	 * @param map The map to parse. Must not be {@code null}.
	 *
	 * @return The map of attribute mapping directives where the keys are
	 *         are the JSON object member names.
	 *
	 * @throws ParseException If parsing of the map failed.
	 */
	private static Map<Key,Directive> parseDirectives(final Map<String,Object> map)
		throws ParseException {

		Map<Key,Directive> directives = new HashMap<>();

		for (Map.Entry<String,Object> mapping: map.entrySet()) {

			Key key = new Key(mapping.getKey());

			if (mapping.getValue() == null)
				throw new ParseException("Missing mapping directive for \"" + key + "\"", 0);

			Directive dir;

			try {
				@SuppressWarnings("unchecked")
				Map<String,Object> dirMap = (Map<String,Object>)mapping.getValue();
				dir = Directive.parse(dirMap);

			} catch (Exception e) {

				throw new ParseException("Invalid mapping directive for \"" + key + "\": " + e.getMessage(), 0);
			}

			directives.put(key, dir);
		}

		return directives;
	}


	/**
	 * Creates a new LDAP attribute mapper.
	 *
	 * @param map The attribute transformation map. Must not be
	 *            {@code null}.
	 *
	 * @throws IllegalArgumentException If the attribute transformation map 
	 *                                  is not valid.
	 */
	public AttributeMapper(final Map<String,Object> map) {

		if (map == null)
			throw new IllegalArgumentException("The attribute transformation map must not be null");
		
		try {
			this.transformMap = parseDirectives(map);
			
		} catch (ParseException e) {
			
			throw new IllegalArgumentException("The attribute transformation map is invalid: " + e.getMessage(), e);
		}


		List<String> ldapAttributeNames = new ArrayList<>();
		
		for (Directive dir: transformMap.values()) {

			String ldapAttrName = dir.ldapAttributeName();

			if (ldapAttrName == null)
				continue;

			ldapAttributeNames.add(ldapAttrName);
		}

		ldapAttributes = ldapAttributeNames.toArray(new String[ldapAttributeNames.size()]);
	}


	/**
	 * Gets the names of the LDAP attributes in the transformation map.
	 *
	 * @return The names of LDAP attributes in the transformation map, 
	 *         empty array if none.
	 */
	public String[] getLDAPAttributeNames() {

		return ldapAttributes;
	}


	/**
	 * Gets the names of the LDAP attributes in the transformation map that
	 * match the specified JSON object member keys.
	 *
	 * @param keys The JSON object member keys. Must not be {@code null}.
	 *
	 * @return The names of the matching LDAP attributes, as a unmodifiable
	 *         list, empty list if none.
	 */
	public List<String> getLDAPAttributeNames(final List<String> keys) {

		List<String> list = new ArrayList<>(keys.size());

		for (String k: keys) {

			String ldapAttrName = getLDAPAttributeName(k);

			if (ldapAttrName != null)
				list.add(ldapAttrName);
		}

		return Collections.unmodifiableList(list);
	}


	/**
	 * Gets the name of the LDAP attribute in the transformation map that
	 * matches the specified JSON object member key.
	 *
	 * @param key The JSON object member key. Must not be {@code null}.
	 *
	 * @return The name of the matching LDAP attribute, {@code null} if not
	 *         found.
	 */
	public String getLDAPAttributeName(final String key) {

		Directive dir = transformMap.get(new Key(key));
		
		if (dir == null)
			return null;
		
		return dir.ldapAttributeName();
	}


	/**
	 * Puts a member into the specified JSON object.
	 *
	 * @param jsonObject The JSON object. Must not be {@code null}.
	 * @param key        The member key. Must not be {@code null}.
	 * @param value      The member value. May be {@code null}.
	 * @param langTag    Optional language tag, {@code null} if not
	 *                   specified.
	 * @param reMatcher  Optional regular expression matching pattern to
	 *                   apply to string and string collection values,
	 *                   {@code null} if not specified.
	 */
	private static void putMember(final JSONObject jsonObject,
				      final Key key,
				      final Object value,
				      final LangTag langTag,
				      final Pattern reMatcher) {

		if (value == null)
			return;

		Object valueToPut = value;

		// Apply regexp matching to value(s)?
		if (reMatcher != null) {
			if (value instanceof String) {

				Matcher matcher = reMatcher.matcher((String)value);

				if (! matcher.matches())
					return;

				valueToPut = matcher.group(1);

			} else if (value instanceof List) {

				@SuppressWarnings("unchecked")
				List<String> valueList = (List<String>)value;

				List<String> processedList = new ArrayList<>(valueList.size());

				for (String v: valueList) {

					Matcher matcher = reMatcher.matcher(v);

					if (matcher.matches())
						processedList.add(matcher.group(1));
				}

				if (processedList.isEmpty())
					return;

				valueToPut = processedList;
			}
		}


		if (! key.isNested()) {

			if (langTag != null)
				jsonObject.put(key + "#" + langTag, valueToPut);
			else
				jsonObject.put(key.value(), valueToPut);
		} else {
			// Any language tag applied to the parent key, see
			// OIDC UserInfo address claim
			String parentKey = key.parts()[0];

			if (langTag != null)
				parentKey +=  "#" + langTag;

			@SuppressWarnings("unchecked")
			JSONObject childJSONObject = (JSONObject)jsonObject.get(parentKey);

			if (childJSONObject == null)
				childJSONObject = new JSONObject();

			childJSONObject.put(key.parts()[1], valueToPut);

			jsonObject.put(parentKey, childJSONObject);
		}
	}


	/**
	 * Transforms the specified LDAP entry. Any transformation exceptions 
	 * are silently ignored.
	 *
	 * @param ldapEntry The LDAP entry to transform. Must not be 
	 *                  {@code null}.
	 *
	 * @return The resulting JSON object.
	 */
	@SuppressWarnings("unchecked")
	public JSONObject transform(final Entry ldapEntry) {

		JSONObject out = new JSONObject();

		for (Map.Entry<Key,Directive> mapping: transformMap.entrySet()) {

			try {
				final Key key = mapping.getKey();
				final Directive dir = mapping.getValue();

				// Resolve the LDAP attribute, possibly with options
				List<Attribute> attrList;

				if (dir.langTagged()) {
					// Language tagged attribute
					attrList = ldapEntry.getAttributesWithOptions(dir.ldapAttributeName(), null);

					if (attrList.isEmpty())
						continue;
				} else {
					// Simple attribute
					attrList = new ArrayList<>(1);

					if (dir.ldapAttributeName() != null) {
						// Get from entry
						Attribute attr = ldapEntry.getAttribute(dir.ldapAttributeName());

						if (attr == null)
							continue;

						attrList.add(attr);
					} else {
						// Apply fixed value
						attrList.add(new Attribute("x-name", dir.ldapValue()));
					}
				}

				// Map the LDAP value to the target JSON value type
				if (dir.jsonValueType().equals("string")) {

					if (dir.langTagged()) {
						// Expect optional lang tag
						for (Attribute a: attrList) {
							
							LangTag langTag = null;
							
							for(String opt: a.getOptions()) {
								
								if (opt.startsWith("lang-")) {
									langTag = LangTag.parse(opt.substring("lang-".length()));
									break;
								}
							}

							putMember(out, key, a.getValue(), langTag, dir.ldapValueMatchingPattern());
						}
					} else {
						// No lang tag
						String jsonString = null;

						if (dir.split()) {
							// Join LDAP values with space char into single JSON string
							StringBuilder sb = new StringBuilder();

							for (String v: attrList.get(0).getValues()) {

								if (v == null)
									continue;

								if (sb.length() > 0)
									sb.append(' ');

								sb.append(v);
							}

							if (sb.length() > 0)
								jsonString = sb.toString();

						} else {
							jsonString = attrList.get(0).getValue();
						}

						if (jsonString.equals(dir.defaultValue()))
							continue; // matches default, skip

						putMember(out, key, jsonString, null, dir.ldapValueMatchingPattern());
					}
					
				} else if (dir.jsonValueType().equals("string-array")) {

					String[] array = attrList.get(0).getValues();

					if (array != null)
						putMember(out, key, Arrays.asList(array), null, dir.ldapValueMatchingPattern());
					
				} else if (dir.jsonValueType().equals("boolean")) {

					Boolean bool = attrList.get(0).getValueAsBoolean();

					if (dir.defaultValue() == bool)
						continue; // matches default, skip

					putMember(out, key, bool, null, dir.ldapValueMatchingPattern());
					
				} else if (dir.jsonValueType().equals("int")) {

					putMember(out, key, attrList.get(0).getValueAsInteger(), null, dir.ldapValueMatchingPattern());
					
				} else if (dir.jsonValueType().equals("long")) {
					
					if (dir.ldapAttributeType().equals("time")) {
						
						String genTimeStr = attrList.get(0).getValue();
						Long timeStamp = StaticUtils.decodeGeneralizedTime(genTimeStr).getTime() / 1000;
						putMember(out, key, timeStamp, null, dir.ldapValueMatchingPattern());
						
					} else {
						putMember(out, key, attrList.get(0).getValueAsLong(), null, dir.ldapValueMatchingPattern());
					}
				} else if (dir.jsonValueType().equals("object")) {

					putMember(out, key, JSONValue.parse(attrList.get(0).getValue()), null, null);
				}

			} catch (Exception e) {

				// ignore and continue
			}
		}

		return out;
	}


	/**
	 * Transforms the specified {@code java.util.Map} representation of an
	 * LDAP entry. Any transformation exceptions are silently ignored.
	 * 
	 * <p>Note: Language tags are not handled by this method.
	 *
	 * @param in The input map, as returned by {@link JSONResultFormatter},
	 *           to transform. Must not be {@code null}.
	 *
	 * @return The resulting JSON object.
	 */
	@SuppressWarnings("unchecked")
	public JSONObject transform(final Map<String,Object> in) {

		JSONObject out = new JSONObject();

		for (Map.Entry<Key,Directive> mapping: transformMap.entrySet()) {

			try {
				final Key key = mapping.getKey();
				final Directive dir = mapping.getValue();

				// Resolve the LDAP attribute, possibly with options
				List<Object> valuesIn;

				if (dir.ldapAttributeName() != null) {

					// From entry
					valuesIn = (List<Object>)in.get(dir.ldapAttributeName());

				} else {
					// Apply fixed value
					valuesIn = new ArrayList<>(1);
					valuesIn.add(dir.ldapValue());
				}
				
				if (valuesIn == null)
					continue;

				Object jsonValue = null;

				if (dir.jsonValueType().equals("string")) {

					if (dir.split()) {

						StringBuilder sb = new StringBuilder();

						for (Object v: valuesIn) {

							if (sb.length() > 0)
								sb.append(' ');

							sb.append((String)v);
						}

						if (sb.length() > 0)
							jsonValue = sb.toString();

					} else {
						jsonValue = valuesIn.get(0);
					}
					
				} else if (dir.jsonValueType().equals("string-array")) {

					jsonValue = valuesIn;
					
				} else if (dir.jsonValueType().equals("boolean")) {

					jsonValue = Boolean.parseBoolean((String)valuesIn.get(0));
					
				} else if (dir.jsonValueType().equals("int")) {

					jsonValue =  Integer.parseInt((String)valuesIn.get(0));
					
				} else if (dir.jsonValueType().equals("long")) {

					if (dir.ldapAttributeType().equals("time")) {
						
						String t = (String)valuesIn.get(0);
						jsonValue = StaticUtils.decodeGeneralizedTime(t).getTime() / 1000;
						
					} else {
						
						jsonValue = Long.parseLong((String)valuesIn.get(0));
					}
				} else if (dir.jsonValueType().equals("object")) {

					jsonValue = (JSONObject)JSONValue.parse((String)valuesIn.get(0));
				}

				if (dir.defaultValue() != null && dir.defaultValue().equals(jsonValue))
					continue; // Skip, matches default value

				// Output
				putMember(out, key, jsonValue, null, dir.ldapValueMatchingPattern());

			} catch (Exception e) {

				// skip
			} 
		}

		return out;
	}


	/**
	 * Performs a reverse transformation of the specified JSON object
	 * representation of an LDAP entry. Any transformation exceptions are 
	 * silently ignored.
	 *
	 * @param jsonObject The JSON object to transform in reverse. Must not 
	 *                   be {@code null}.
	 *
	 * @return The resulting LDAP attributes for the entry.
	 */
	@SuppressWarnings("unchecked")
	public List<Attribute> reverseTransform(final JSONObject jsonObject) {

		List<Attribute> ldapAttrs = new ArrayList<>(jsonObject.size());

		for (Map.Entry<Key,Directive> mapping: transformMap.entrySet()) {

			try {
				final Key key = mapping.getKey();
				final Directive dir = mapping.getValue();

				if (dir.ldapValue() != null)
					continue; // Reverse transform not possible

				if (dir.defaultValue() != null && dir.defaultValue().equals(jsonObject.get(key.value())))
					continue; // Value matches default, skip
				
				if (dir.langTagged() &&
				    dir.jsonValueType().equals("string") && 
				    dir.ldapAttributeType().equals("string")) {
					
					// Extract the LangTags for the key, if any
					Map<LangTag,Object> langTaggedEntries = LangTagUtils.find(key.value(), jsonObject);
					
					for (Map.Entry<LangTag,Object> entry: langTaggedEntries.entrySet()) {
						
						if (entry.getValue() == null)
							continue;

						String jsonString = (String)entry.getValue();

						if (jsonString.isEmpty())
							continue;
						
						String ldapAttributeName = dir.ldapAttributeName();
					
						LangTag langTag = entry.getKey();
					
						if (langTag != null)
							ldapAttributeName += ";lang-" + langTag.toString();
						
						ldapAttrs.add(new Attribute(ldapAttributeName, jsonString));
					}
					
					continue;
				}

				final Object jsonValue = jsonObject.get(key.value());

				if (jsonValue instanceof String
				    && dir.jsonValueType().equals("string")
				    && dir.ldapAttributeType().equals("string")) {

					String jsonString = (String)jsonValue;

					if (jsonString.isEmpty())
						continue;

					String[] ldapValues;

					if (dir.split())
						ldapValues = SPLIT_PATTERN.split(jsonString);
					else
						ldapValues = new String[]{jsonString};

					ldapAttrs.add(new Attribute(dir.ldapAttributeName(), ldapValues));

				} else if (jsonValue instanceof List) {

					List<Object> jsonArray = (List)jsonValue;

					if (jsonArray.isEmpty())
						continue; // Don't output empty values

					List<String> ldapValues = new ArrayList<>();

					for (Object v : jsonArray) {

						if (v instanceof Number // give some leeway here
						    && dir.jsonValueType().equals("long")
						    && dir.jsonValueType().equals("time")) {

							long t = ((Number)jsonValue).longValue();

							// t must be positive, ignore otherwise
							if (t > 0L) {
								// Convert Unix time to LDAP generalised time
								ldapValues.add(StaticUtils.encodeGeneralizedTime(new Date(t*1000)));
							}

						} else {
							ldapValues.add(v.toString());
						}
					}

					ldapAttrs.add(new Attribute(dir.ldapAttributeName(), ldapValues));

				} else if (jsonValue instanceof Number // give some leeway here
					   && dir.jsonValueType().equals("long")
					   && dir.ldapAttributeType().equals("time")) {

					long t = ((Number) jsonValue).longValue();

					// t must be positive, ignore otherwise
					if (t > 0L) {
						// Convert Unix time to LDAP generalised time
						ldapAttrs.add(new Attribute(
							dir.ldapAttributeName(),
							StaticUtils.encodeGeneralizedTime(new Date(t * 1000))));
					}
				} else if (jsonValue instanceof Boolean
					   && dir.jsonValueType().equals("boolean")) {
					// Upper case expected, see https://tools.ietf.org/html/rfc2252#section-6.4
					ldapAttrs.add(new Attribute(dir.ldapAttributeName(), jsonValue.toString().toUpperCase()));

				} else if (! jsonValue.toString().isEmpty()) {
					// All other values types are mapped to LDAP string
					ldapAttrs.add(new Attribute(dir.ldapAttributeName(), jsonValue.toString()));
				}

			} catch (Exception e) {
				// ignore and continue
			}
		}

		return ldapAttrs;
	}
}