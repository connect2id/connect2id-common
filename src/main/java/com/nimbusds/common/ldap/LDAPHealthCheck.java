package com.nimbusds.common.ldap;


import org.apache.logging.log4j.Logger;

import com.codahale.metrics.health.HealthCheck;

import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.LDAPInterface;


/**
 * Simple LDAP connection (individual or pool) health check. Retrieves a
 * specified entry from the directory to determine the LDAP connection health.
 */
public class LDAPHealthCheck extends HealthCheck {


	/**
	 * The LDAP interface.
	 */
	private final LDAPInterface ldap;


	/**
	 * LDAP entry to retrieve for the health check.
	 */
	private final DN testEntry;


	/**
	 * Optional logger for health check errors.
	 */
	private final Logger log;


	/**
	 * Creates a new LDAP connection pool health check.
	 *
	 * @param ldap      The LDAP store. Must not be {@code null}.
	 * @param testEntry LDAP entry to retrieve for the health check. Must
	 *                  not be {@code null}.
	 * @param log       Optional logger for health check errors,
	 *                  {@code null} if not required.
	 */
	public LDAPHealthCheck(final LDAPInterface ldap,
			       final DN testEntry,
			       final Logger log) {

		if (ldap == null) {
			throw new IllegalArgumentException("The LDAP interface must not be null");
		}

		this.ldap = ldap;

		if (testEntry == null) {
			throw new IllegalArgumentException("The LDAP test entry must not be null");
		}

		this.testEntry = testEntry;

		this.log = log;
	}


	@Override
	public HealthCheck.Result check()
		throws Exception {

		final Entry entry;

		try {
			entry = ldap.getEntry(testEntry.toString());

		} catch (LDAPException e) {

			if (log != null) {
				log.warn("LDAP connector health check failure: {}", e.getMessage());
			}

			return HealthCheck.Result.unhealthy(e.getMessage());
		}

		if (entry == null) {

			final String msg = "LDAP connector health check failure: Missing test entry: " + testEntry;

			if (log != null) {
				log.warn(msg);
			}

			return HealthCheck.Result.unhealthy(msg);
		}

		if (log != null) {
			log.debug("LDAP connector health check success: healthy");
		}

		return HealthCheck.Result.healthy();
	}
}