package com.nimbusds.common.ldap;


/**
 * Enumeration of the protocols for directory access.
 */
public enum DirectoryProtocol {

	
	/**
	 * LDAP v3.
	 */
	LDAP,
	
	
	/**
	 * Json2Ldap.
	 */
	JSON2LDAP
}
