package com.nimbusds.common.ldap;


import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.schema.ObjectClassDefinition;
import com.unboundid.ldap.sdk.schema.Schema;


/**
 * LDAP schema utilities.
 */
public class SchemaUtils {


	/**
	 * Returns the schema definition for the specified object class, in the
	 * format described in RFC 4512 section 4.1.1.
	 *
	 * @param ldapCon     The LDAP connection. Must not be {@code null}.
	 * @param objectClass The object class. Must not be {@code null}.
	 *
	 * @return The object class definition, or an error message if an
	 *         exception is encountered.
	 */
	public static String dumpObjectClassDefinition(final LDAPConnection ldapCon,
						       final String objectClass) {

		Schema schema;

		try {
			schema = ldapCon.getSchema();

		} catch (LDAPException e) {

			return e.getMessage();
		}

		if (schema == null) {
			return "LDAP schema not available (check permissions)";
		}

		ObjectClassDefinition objClassDef = schema.getObjectClass(objectClass);

		if (objClassDef == null) {
			return "No such objectClass: " + objectClass;
		}

		return objClassDef.toString();
	}


	/**
	 * Prevents public instantiation.
	 */
	private SchemaUtils() {

	}
}
