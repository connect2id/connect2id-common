package com.nimbusds.common.ldap;


import javax.net.SocketFactory;

import com.unboundid.ldap.sdk.FailoverServerSet;
import com.unboundid.ldap.sdk.LDAPConnectionOptions;
import com.unboundid.ldap.sdk.LDAPURL;
import com.unboundid.ldap.sdk.RoundRobinServerSet;
import com.unboundid.ldap.sdk.ServerSet;
import com.unboundid.ldap.sdk.SingleServerSet;

import com.nimbusds.common.config.ServerSelectionAlgorithm;


/**
 * LDAP server set factory.
 */
public class LDAPServerSetFactory {

	
	/**
	 * Creates an LDAP server set from the specified configuration 
	 * parameters.
	 *
	 * @param url                The LDAP server URLs array. Must not be 
	 *                           {@code null} and contain at least one LDAP
	 *                           server URL.
	 * @param selectionAlgorithm The server selection algorithm, ignored 
	 *                           (may be {@code null}) if a single LDAP 
	 *                           server URL is specified.
	 * @param socketFactory      The socket factory, {@code null} to use the
	 *                           default one.
	 * @param opts               The LDAP connection options, {@code null} 
	 *                           to use the default.
	 */
	public static ServerSet create(final LDAPURL[] url, 
	                               final ServerSelectionAlgorithm selectionAlgorithm,
				       final SocketFactory socketFactory,
				       final LDAPConnectionOptions opts) {
		
		if (url.length == 0)
			throw new IllegalArgumentException("At least one server must be specified in the LDAP URL array");
		
		// Single server?
		if (url.length == 1)
			return new SingleServerSet(url[0].getHost(), 
			                           url[0].getPort(), 
						   socketFactory, 
						   opts);
		
		
		// Multiple servers, build appropriate set
		
		String[] hosts = new String[url.length];
		int[] ports = new int[url.length];
		
		for (int i=0; i<url.length; i++) {
			hosts[i] = url[i].getHost();
			ports[i] = url[i].getPort();
		}
		
		
		if (selectionAlgorithm == ServerSelectionAlgorithm.FAILOVER)
			return new FailoverServerSet(hosts, 
			                             ports, 
						     socketFactory,
						     opts);
		
		else if (selectionAlgorithm == ServerSelectionAlgorithm.ROUND_ROBIN)
			return new RoundRobinServerSet(hosts, 
			                               ports, 
						       socketFactory,
						       opts);
		
		else
			throw new IllegalArgumentException("Unexpected LDAP server selection algorithm: " + selectionAlgorithm);
	}
	
	
	/**
	 * Prevents instantiation.
	 */
	private LDAPServerSetFactory() {
	
		// Nothing to do
	}
}
