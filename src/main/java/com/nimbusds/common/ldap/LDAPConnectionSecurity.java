package com.nimbusds.common.ldap;


/**
 * Enumeration of the transport layer security types for LDAP connections.
 */
public enum LDAPConnectionSecurity {

	
	/**
	 * No security (plain connection).
	 */
	NONE,
	
	
	/**
	 * StartTLS security.
	 */
	STARTTLS,
	
	
	/**
	 * SSL security.
	 */
	SSL
}
