package com.nimbusds.common.ldap;


import java.util.*;

import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchResultReference;


/**
 * Static methods to format complex LDAP result structures as LDIF strings.
 *
 * <p>See the related {@link JSONResultFormatter} class for JSON result 
 * formatting.
 */
public class LDIFResultFormatter {
	
	
	/**
	 * Formats an LDAP directory entry as an LDIF string.
	 *
	 * <p>Format:
	 *
	 * <pre>
	 * dn: entry-DN
	 * attribute-name-1: value-1
	 * attribute-name-1: value-2
	 * attribute-name-1: value-n
	 * ...
	 * attribute-name-2: value-1
	 * ...
	 * </pre>
	 *
	 * @param entry The directory entry.
	 *
	 * @return An LDIF string representing the directory entry.
	 */
	public static String formatEntry(final Entry entry) {
	
		return entry.toLDIFString();
	}
	
	
	/**
	 * Formats an LDAP search result as a JSON object where the matches are
	 * presented as an LDIF string. Otherwise the returned JSON object has
	 * the same top level structure as for JSON-formatted search results.
	 *
	 * <p>Format:
	 *
	 * <pre>
	 * { 
	 *   "matches"    : "dn: result-entry-DN-1\n
	 *                   attribute-name-1: value-1\n
	 *                   attribute-name-1: value-2\n
	 *                   attribute-name-1: value-n\n
	 *                   ...\n
	 *                   attribute-name-2: value-1\n
	 *                   ...\n
	 *                   \n
	 *                   dn: result-entry-DN-2\n
	 *                   ...\n
	 *                   \n
	 *                   dn: result-entry-DN-3\n
	 *                   ...\n",
	 *   "referrals"  : ["url-1", "url-2", "url-3", ...],
	 *   "page"       : { "totalEntryCount" : n,
	 *                    "more"            : true|false,
	 *                    "cookie"          : "..." },
	 *   "vlv"        : { "totalEntryCount" : n,
	 *                    "offset"          : n,
	 *                    "cookie"          : "..." }
	 * }
	 * </pre>
	 *
	 * @param sr The search result.
	 *
	 * @return A JSON object containing the search result matches as LDIF, 
	 *         referrals and optional control data.
	 */
	public static Map<String,Object> formatSearchResult(final SearchResult sr) {

		Map<String,Object> jsonObject = new LinkedHashMap<>();
			
		// A result consists of possible matching entries and referrals
		
		StringBuilder buffer = new StringBuilder();
		
		for (SearchResultEntry entry : sr.getSearchEntries()) {
			entry.toLDIFString(buffer);
			buffer.append("\n"); // delimit entry
		}
		
		jsonObject.put("matches", buffer.toString());
		
		
		List<String> referrals = new LinkedList<>();
		
		for (SearchResultReference ref: sr.getSearchReferences())
			Collections.addAll(referrals, ref.getReferralURLs());
	
		jsonObject.put("referrals", referrals);
		
		// Append any simple-page control or virtual-list-view control results
		LDAPControlResultFormatter.appendSearchControlResults(jsonObject, sr);
		
		return jsonObject;
	}


	/**
	 * Prevents instantiation.
	 */
	private LDIFResultFormatter() {

		// Nothing to do
	}
}
