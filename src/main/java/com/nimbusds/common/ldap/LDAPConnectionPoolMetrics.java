package com.nimbusds.common.ldap;


import java.util.HashMap;
import java.util.Map;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricSet;

import com.unboundid.ldap.sdk.LDAPConnectionPool;


/**
 * LDAP connection metrics.
 */
public class LDAPConnectionPoolMetrics implements MetricSet {


	/**
	 * The metrics map.
	 */
	private final Map<String,Metric> metricMap = new HashMap<>();


	/**
	 * Creates a new LDAP connection metrics.
	 *
	 * @param pool   The LDAP connection pool. Must not be {@code null}.
	 * @param prefix The metrics name prefix. Must not be {@code null}.                    
	 */
	public LDAPConnectionPoolMetrics(final LDAPConnectionPool pool,
					 final String prefix) {

		metricMap.put(prefix + ".maxAvailableConnections", (Gauge<Integer>) () -> pool.getConnectionPoolStatistics().getMaximumAvailableConnections());

		metricMap.put(prefix + ".numAvailableConnections", (Gauge<Integer>) () -> pool.getConnectionPoolStatistics().getNumAvailableConnections());

		metricMap.put(prefix + ".numConnectionsClosedDefunct", (Gauge<Long>) () -> pool.getConnectionPoolStatistics().getNumConnectionsClosedDefunct());

		metricMap.put(prefix + ".numConnectionsClosedExpired", (Gauge<Long>) () -> pool.getConnectionPoolStatistics().getNumConnectionsClosedExpired());

		metricMap.put(prefix + ".numConnectionsClosedUnneeded", (Gauge<Long>) () -> pool.getConnectionPoolStatistics().getNumConnectionsClosedUnneeded());

		metricMap.put(prefix + ".numFailedCheckouts", (Gauge<Long>) () -> pool.getConnectionPoolStatistics().getNumFailedCheckouts());

		metricMap.put(prefix + ".numFailedConnectionAttempts", (Gauge<Long>) () -> pool.getConnectionPoolStatistics().getNumFailedConnectionAttempts());

		metricMap.put(prefix + ".numReleasedValid", (Gauge<Long>) () -> pool.getConnectionPoolStatistics().getNumReleasedValid());

		metricMap.put(prefix + ".numSuccessfulCheckouts", (Gauge<Long>) () -> pool.getConnectionPoolStatistics().getNumSuccessfulCheckouts());

		metricMap.put(prefix + ".numSuccessfulCheckoutsNewConnection", (Gauge<Long>) () -> pool.getConnectionPoolStatistics().getNumSuccessfulCheckoutsNewConnection());

		metricMap.put(prefix + ".numSuccessfulCheckoutsWithoutWaiting", (Gauge<Long>) () -> pool.getConnectionPoolStatistics().getNumSuccessfulCheckoutsWithoutWaiting());

		metricMap.put(prefix + ".numSuccessfulCheckoutsAfterWaiting", (Gauge<Long>) () -> pool.getConnectionPoolStatistics().getNumSuccessfulCheckoutsAfterWaiting());

		metricMap.put(prefix + ".numSuccessfulConnectionAttempts", (Gauge<Long>) () -> pool.getConnectionPoolStatistics().getNumSuccessfulConnectionAttempts());
	}


	@Override
	public Map<String,Metric> getMetrics() {

		return metricMap;
	}
}
