package com.nimbusds.common.monitor;


import com.codahale.metrics.*;
import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.nimbusds.common.config.MonitorConfiguration;

import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;


/**
 * Shared monitor registers for Dropwizard metrics and health checks.
 *
 * @see com.nimbusds.common.servlet.MonitorLauncher
 */
public class MonitorRegistries {


	/**
	 * The metrics registry.
	 */
	private static MetricRegistry metricRegistry;


	/**
	 * The health checks registry.
	 */
	private static HealthCheckRegistry healthCheckRegistry;
	
	
	/**
	 * Timeout for caching entry count results, in seconds. Zero means
	 * infinite (never expire), negative disabled.
	 */
	private static long entryCountCacheTimeout = MonitorConfiguration.DEFAULT_ENTRY_COUNT_CACHE_TIMEOUT;


	/**
	 * Returns the singleton shared registry for metric instances.
	 *
	 * @return The registry.
	 */
	public static MetricRegistry getMetricRegistry() {
		
		if (metricRegistry == null) {
			metricRegistry = new MetricRegistry();
		}
		return metricRegistry;
	}


	/**
	 * Returns the singleton shared registry for health check instances.
	 *
	 * @return The registry.
	 */
	public static HealthCheckRegistry getHealthCheckRegistry() {
		
		if (healthCheckRegistry == null) {
			healthCheckRegistry = new HealthCheckRegistry();
		}
		return healthCheckRegistry;
	}


	/**
	 * Registers a metric set.
	 *
	 * @param metricSet The metric set to register. If {@code null} the
	 *                  method will return immediately.
	 */
	public static void register(final MetricSet metricSet) {

		if (metricSet == null) {
			return;
		}

		for (Map.Entry<String,Metric> entry: metricSet.getMetrics().entrySet()) {
			register(entry.getKey(), entry.getValue());
		}
	}


	/**
	 * Registers, updates or unregisters a metric.
	 *
	 * @param name   The metric name. If {@code null} the method will
	 *               return immediately.
	 * @param metric The metric, {@code null} to unregister. If a metric
	 *               with the same name exists it will be replaced.
	 */
	public static void register(final String name, final Metric metric) {

		if (name == null) {
			return;
		}

		if (metric != null) {
			if (getMetricRegistry().getNames().contains(name)) {
				// remove previously registered metric with same name
				getMetricRegistry().remove(name);
			}
			getMetricRegistry().register(name, metric);
		} else {
			getMetricRegistry().remove(name);
		}
	}


	/**
	 * Registers or updates an entry count gauge according to the
	 * configured {@link #getEntryCountCacheTimeout() cache timeout}.
	 *
	 * @param name     The gauge name. If {@code null} the method will
	 *                 return immediately.
	 * @param supplier The entry count supplier.
	 */
	public static void registerEntryCountGauge(final String name, final Supplier<Integer> supplier) {

		final Metric metricToRegister;
		
		if (entryCountCacheTimeout < 0) {
			// disabled
			metricToRegister = (Gauge<Integer>) () -> -1;
		} else if (entryCountCacheTimeout == 0) {
			// no caching
			metricToRegister = (Gauge<Integer>) supplier::get;
		} else {
			// caching
			metricToRegister = new CachedGauge<Integer>(entryCountCacheTimeout, TimeUnit.SECONDS) {
				@Override
				protected Integer loadValue() {
					return supplier.get();
				}
			};
		}
		
		register(name, metricToRegister);
	}
	
	
	/**
	 * Gets the timeout for entry count gauges.
	 *
	 * @return The timeout for caching entry count results, in seconds.
	 *         Zero means no caching, negative disabled readings.
	 */
	public static long getEntryCountCacheTimeout() {
		return entryCountCacheTimeout;
	}
	
	
	/**
	 * Sets the timeout for entry count gauges.
	 *
	 * @param entryCountCacheTimeout The timeout for caching entry count
	 *                               results, in seconds. Zero means no
	 *                               caching, negative disabled readings.
	 */
	public static void setEntryCountCacheTimeout(final long entryCountCacheTimeout) {
		MonitorRegistries.entryCountCacheTimeout = entryCountCacheTimeout;
	}
	
	
	/**
	 * Registers, updates or unregisters a health check.
	 *
	 * @param name  The health check name. If {@code null} the method will
	 *              return immediately.
	 * @param check The health check, {@code null} to unregister. If a
	 *              metric with the same name exists it will be replaced.
	 */
	public static void register(final String name, final HealthCheck check) {

		if (name == null) {
			return;
		}

		if (check != null) {
			if (getHealthCheckRegistry().getNames().contains(name)) {
				// remove previously registered check with same name
				getHealthCheckRegistry().unregister(name);
			}
			getHealthCheckRegistry().register(name, check);
		} else {
			getHealthCheckRegistry().unregister(name);
		}
	}


	/**
	 * Prevents public instantiation.
	 */
	private MonitorRegistries() { }
}
