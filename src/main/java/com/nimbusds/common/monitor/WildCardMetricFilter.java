package com.nimbusds.common.monitor;


import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricFilter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Wild card metric filter.
 */
public class WildCardMetricFilter implements MetricFilter {


	/**
	 * The wild card.
	 */
	private final List<String> whiteList;


	/**
	 * If {@code true} no metric will match.
	 */
	private final boolean matchNone;


	/**
	 * If {@code true} any metric will match.
	 */
	private final boolean matchAny;


	/**
	 * Creates a new wild card metrics filter.
	 *
	 * @param whiteList The white list entries, {@code null} if none.
	 */
	public WildCardMetricFilter(final List<String> whiteList) {

		this.whiteList = whiteList;
		matchNone = whiteList == null || whiteList.isEmpty();

		if (whiteList != null) {
			// "*" wild card means match any
			for (String entry: whiteList) {
				if (entry == null) {
					continue; // skip
				}
				if ("*".equals(entry.trim())) {
					matchAny = true;
					return;
				}
			}
		}
		matchAny = false;
	}


	/**
	 * Returns the white list.
	 *
	 * @return The white list, {@code null} if none.
	 */
	public List<String> getWhiteList() {

		return whiteList;
	}


	/**
	 * Returns {@code true} if the filter will match any metric.
	 *
	 * @return {@code true} if any metric will be matched, else
	 *         {@code false}.
	 */
	public boolean matchesNone() {

		return matchNone;
	}


	/**
	 * Returns {@code true} if the filter will match no metric.
	 *
	 * @return {@code true} if no metric will be matched, else
	 *         {@code false}.
	 */
	public boolean matchesAny() {

		return matchAny;
	}


	@Override
	public boolean matches(final String name, final Metric metric) {

		// Disregard metric parameter, check name only

		if (matchNone) {
			return false;
		}

		if (matchAny) {
			return true;
		}

		if (name == null || name.isEmpty()) {
			return false;
		}

		for (String acceptedName: whiteList) {

			if (! getMatches(name, acceptedName).isEmpty()) {
				return true;
			}
		}

		return false;
	}


	/**
	 * This method takes four parameters: t1 and t2 (the two strings) and
	 * two indices i1 and i2 (the index to start at in t1 and t2). It
	 * returns the length of the longest common extension starting at i1 in
	 * t1 and at i2 in t2. The method simply iterates over the given
	 * strings, starting at the given indices. It compares the characters,
	 * counts equal characters and returns that number as soon as the
	 * characters differ.
	 */
	public static int longestCommonExtension(final String t1, final int i1, final String t2, int i2) {
		int res = 0;
		for (int i = i1; i < t1.length() && i2 < t2.length(); i++, i2++) {
			if (t1.charAt(i) == t2.charAt(i2))
				res++;
			else
				return res;
		}
		return res;
	}


	/**
	 * Simple computation of the longest common extension (LCE) of two
	 * given strings, beginning at two given indices. The LCE computation
	 * is part of different algorithms in
	 * <a href="http://en.wikipedia.org/wiki/Fuzzy_string_searching">inexact
	 * string matching</a> (Gusfield 1999:196). This simple implementation
	 * has a runtime complexity of O(n). Computation in O(1) is possible
	 * through constant-time retrieval of Lowest Common Ancestors (LCA) in
	 * <a href="http://en.wikipedia.org/wiki/Suffix_tree">suffix trees</a>
	 * (after linear-time preprocessing). See Gusfield 1999:181 for further
	 * reference on constant time LCA retrieval and Gusfield 1999:196 for
	 * constant time LCE computation using the LCA.
	 *
	 * <p>References: Gusfield, Dan (1999), Algorithms on Strings,
	 * Sequences and Trees, Cambridge: University Press.
	 *
	 * <p>An implementation of string matching with wildcards, as described
	 * in Gusfield 1999:199. When used with a longest common extension
	 * computation with suffix trees, it allows for searching a pattern
	 * with k wildcards in a text T of length m in O(km) time (Gusfield
	 * 1999:199). This implementation uses a simple computation of the
	 * longest common extension.
	 */
	public static Collection<String> getMatches(String t, String p) {

		Collection<String> result = new ArrayList<>();
		for (int i = 0; i < t.length(); i++) {

			int j = 0;
			int h = i;
			int n = p.length();

			while (true){

				int L = longestCommonExtension(p, j, t, h);

				if (j + 1 + L == n + 1) {
					result.add(t.substring(i, i + n));
					break;
				}

				if (((j + L) < p.length() && p.charAt(j + L) == '*')
					|| ((h + L) < t.length() && t.charAt(h + L) == '*')) {
					j = j + L + 1;
					h = h + L + 1;
				} else
					break;
			}
		}
		return result;
	}
}
