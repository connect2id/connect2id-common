package com.nimbusds.common.oauth2;


import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.BearerTokenError;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import net.minidev.json.JSONObject;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Master access token validator. Intended for validation of master API access
 * tokens for the Connect2id server and elsewhere.
 */
public interface MasterAccessTokenValidator {
	
	
	/**
	 * Error response: Missing OAuth 2.0 Bearer access token.
	 */
	ErrorResponse MISSING_BEARER_TOKEN = new ErrorResponse(
		BearerTokenError.MISSING_TOKEN.getHTTPStatusCode(),
		BearerTokenError.MISSING_TOKEN.toWWWAuthenticateHeader(),
		"missing_token",
		"Unauthorized: Missing Bearer access token");
	
	
	/**
	 * Error response: Invalid OAuth 2.0 Bearer access token.
	 */
	ErrorResponse INVALID_BEARER_TOKEN = new ErrorResponse(
		BearerTokenError.INVALID_TOKEN.getHTTPStatusCode(),
		BearerTokenError.INVALID_TOKEN.toWWWAuthenticateHeader(),
		BearerTokenError.INVALID_TOKEN.getCode(),
		"Unauthorized: Invalid Bearer access token");
	
	
	/**
	 * Error response: Web API disabled.
	 */
	ErrorResponse WEB_API_DISABLED = new ErrorResponse(
		403,
		null,
		"web_api_disabled",
		"Forbidden: Web API disabled");
	
	
	/**
	 * Bearer token error response.
	 */
	class ErrorResponse {
		
		
		/**
		 * The HTTP status code.
		 */
		private final int statusCode;
		
		
		/**
		 * Optional HTTP response {@code WWW-Authenticate} header.
		 */
		private final String wwwAuthHeader;
		
		
		/**
		 * The HTTP body.
		 */
		private final String body;
		
		
		/**
		 * Creates a new bearer token error response.
		 *
		 * @param statusCode    The HTTP status code.
		 * @param wwwAuthHeader The HTTP response
		 *                      {@code WWW-Authenticate} header,
		 *                      {@code null} if none.
		 * @param bodyErrorCode The error code for the body JSON
		 *                      object.
		 * @param bodyErrorDesc The error description for the body JSON
		 *                      object.
		 */
		public ErrorResponse(final int statusCode,
				     final String wwwAuthHeader,
				     final String bodyErrorCode,
				     final String bodyErrorDesc) {
			
			this.statusCode = statusCode;
			this.wwwAuthHeader = wwwAuthHeader;
			JSONObject bodyErrorObj = new JSONObject();
			bodyErrorObj.put("error", bodyErrorCode);
			bodyErrorObj.put("error_description", bodyErrorDesc);
			this.body = bodyErrorObj.toJSONString();
		}
		
		
		/**
		 * Returns a web application exception for this error response.
		 *
		 * @return The web application exception.
		 */
		public WebApplicationException toWebAppException() {
			
			return new WebApplicationException(toWebAppResponse());
		}


		/**
		 * Returns a web app response for this error response.
		 *
		 * @return The web app response.
		 */
		public Response toWebAppResponse() {

			Response.ResponseBuilder builder = Response.status(statusCode);

			if (wwwAuthHeader != null) {
				builder.header("WWW-Authenticate", wwwAuthHeader);
			}

			return builder.entity(body).type(MediaType.APPLICATION_JSON).build();
		}
		
		
		/**
		 * Applies this error response to the specified HTTP servlet
		 * response.
		 *
		 * @param servletResponse The HTTP servlet response. Must not
		 *                        be {@code null}.
		 *
		 * @throws IOException If the error response couldn't be
		 *                     written.
		 */
		public void apply(final HttpServletResponse servletResponse)
			throws IOException {
			
			servletResponse.setStatus(statusCode);
			
			if (wwwAuthHeader != null) {
				servletResponse.setHeader("WWW-Authenticate", wwwAuthHeader);
			}
			
			if (body != null) {
				servletResponse.setContentType("application/json");
				servletResponse.getWriter().print(body);
			}
		}
	}
	
	
	/**
	 * Computes the SHA-256 hash of the specified Bearer access token.
	 *
	 * @param token The Bearer access token. Must not be {@code null}.
	 * @param salt  Optional salt to use, {@code null} if none.
	 *
	 * @return The computed SHA-256 hash.
	 */
	static byte[] computeSHA256(final BearerAccessToken token, final byte[] salt) {
		
		try {
			MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
			if (salt != null) {
				sha256.update(salt);
			}
			return sha256.digest(token.getValue().getBytes(StandardCharsets.UTF_8));
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * Returns {@code true} if access is disabled (no access token
	 * configured).
	 *
	 * @return {@code true} if access is disabled, else {@code false}.
	 */
	boolean accessIsDisabled();
	
	
	/**
	 * Gets the optional logger.
	 *
	 * @return The logger, {@code null} if not specified.
	 */
	Logger getLogger();
	
	
	/**
	 * Sets the optional logger.
	 *
	 * @param log The logger, {@code null} if not specified.
	 */
	void setLogger(final Logger log);
	
	
	/**
	 * Returns {@code true} if the specified bearer access token is valid.
	 *
	 * @param accessToken The bearer access token to check, {@code null} if
	 *                    not specified.
	 *
	 * @return {@code true} if the specified bearer access token is valid,
	 *         else {@code false}.
	 */
	boolean isValid(final BearerAccessToken accessToken);
	
	
	/**
	 * Validates a bearer access token passed in the specified HTTP
	 * Authorization header value.
	 *
	 * @param authzHeader The HTTP Authorization header value, {@code null}
	 *                    if not specified.
	 *
	 * @throws WebApplicationException If the header value is {@code null},
	 *                                 the web API is disabled, or the
	 *                                 Bearer access token is missing or
	 *                                 invalid.
	 */
	void validateBearerAccessToken(final String authzHeader)
		throws WebApplicationException;
	
	
	/**
	 * Validates a bearer access token passed in the specified HTTP servlet
	 * request.
	 *
	 * @param servletRequest  The HTTP servlet request. Must not be
	 *                        {@code null}.
	 * @param servletResponse The HTTP servlet response. Must not be
	 *                        {@code null}.
	 *
	 * @return {@code true} if the bearer access token was successfully
	 *         validated, {@code false}.
	 *
	 * @throws IOException If the response couldn't be written.
	 */
	boolean validateBearerAccessToken(final HttpServletRequest servletRequest,
					  final HttpServletResponse servletResponse)
		throws IOException;
}
