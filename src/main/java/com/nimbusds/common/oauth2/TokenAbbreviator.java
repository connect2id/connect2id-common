package com.nimbusds.common.oauth2;


import com.nimbusds.oauth2.sdk.token.Token;
import org.apache.commons.lang3.StringUtils;


/**
 * Token abbreviation utility.
 */
public class TokenAbbreviator {
	
	
	/**
	 * Returns the first nine characters of the specified token.
	 *
	 * @param token The token, {@code null} if not specified.
	 *
	 * @return The first nine token characters, or "null".
	 */
	public static String abbreviate(final Token token) {
		
		if (token == null) {
			return "null";
		}
		
		return StringUtils.abbreviate(token.getValue(), 12);
	}
}
