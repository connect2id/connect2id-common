package com.nimbusds.common.oauth2;


import com.nimbusds.jose.crypto.utils.ConstantTimeUtils;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;


/**
 * Abstract access token validator.
 */
abstract class AbstractAccessTokenValidator implements MasterAccessTokenValidator {
	
	
	/**
	 * The expected access token hashes, empty list if access to the web
	 * API is disabled.
	 */
	protected final List<byte[]> expectedTokenHashes = new ArrayList<>();
	
	
	/**
	 * Optional salt for computing the SHA-256 hashes.
	 */
	protected byte[] hashSalt = null;
	
	
	/**
	 * Optional logger.
	 */
	protected Logger log;
	
	
	@Override
	public boolean accessIsDisabled() {
		
		return expectedTokenHashes.isEmpty();
	}
	
	
	@Override
	public boolean isValid(final BearerAccessToken accessToken) {
		
		if (accessToken == null) return false;
		
		final byte[] receivedTokenHash = MasterAccessTokenValidator.computeSHA256(accessToken, hashSalt);
		
		for (byte[] expectedHash: expectedTokenHashes) {
			if (ConstantTimeUtils.areEqual(receivedTokenHash, expectedHash)) {
				return true;
			}
		}
		
		return false;
	}
	
	
	@Override
	public Logger getLogger() {
		return log;
	}
	
	
	@Override
	public void setLogger(final Logger log) {
		this.log = log;
	}
	
	
	/**
	 * Returns the number of configured tokens.
	 *
 	 * @return The number of configured tokens, zero if none.
	 */
	public int getNumberConfiguredTokens() {
		
		return expectedTokenHashes.size();
	}
}
