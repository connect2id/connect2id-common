package com.nimbusds.common.oauth2;


import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.WebApplicationException;
import net.jcip.annotations.ThreadSafe;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.security.SecureRandom;


/**
 * Basic access token validator. Supports servlet-based and JAX-RS based web
 * applications.
 */
@ThreadSafe
public class BasicAccessTokenValidator extends AbstractAccessTokenValidator {


	/**
	 * Creates a new basic access token validator.
	 *
	 * @param accessToken The Bearer access token. If {@code null} access
	 *                    to the web API will be disabled.
	 */
	public BasicAccessTokenValidator(final BearerAccessToken accessToken) {

		this(new BearerAccessToken[]{accessToken});
	}
	
	/**
	 * Creates a new basic access token validator.
	 *
	 * @param accessTokens The Bearer access tokens. If {@code null} access
	 *                     to the web API will be disabled.
	 */
	public BasicAccessTokenValidator(final BearerAccessToken ... accessTokens) {
		
		hashSalt = generate32ByteSalt();
		
		for (BearerAccessToken t: accessTokens) {
			if (t == null) continue;
			expectedTokenHashes.add(MasterAccessTokenValidator.computeSHA256(t, hashSalt));
		}
	}


        /**
	 * Generates a 32 byte salt.
	 *
	 * @return The 32 byte salt.
	 */
	private static byte[] generate32ByteSalt() {
		
		byte[] hashSalt = new byte[32];
		new SecureRandom().nextBytes(hashSalt);
		return hashSalt;
	}
	
	
	@Override
	public void validateBearerAccessToken(final String authzHeader)
		throws WebApplicationException {
		
		// Web API disabled?
		if (accessIsDisabled()) {
			throw WEB_API_DISABLED.toWebAppException();
		}

		if (StringUtils.isBlank(authzHeader)) {
			throw MISSING_BEARER_TOKEN.toWebAppException();
		}

		BearerAccessToken receivedToken;

		try {
			receivedToken = BearerAccessToken.parse(authzHeader);

		} catch (ParseException e) {
			throw MISSING_BEARER_TOKEN.toWebAppException();
		}
		
		if (null != log) {
			log.trace("[CM3000] Validating bearer access token: {}", TokenAbbreviator.abbreviate(receivedToken));
		}

		if (isValid(receivedToken)) {
			return;
		}
		
		throw INVALID_BEARER_TOKEN.toWebAppException();
	}


	@Override
	public boolean validateBearerAccessToken(final HttpServletRequest servletRequest,
						 final HttpServletResponse servletResponse)
		throws IOException {
		
		// Web API disabled?
		if (accessIsDisabled()) {
			WEB_API_DISABLED.apply(servletResponse);
			return false;
		}
		
		BearerAccessToken receivedToken;
		
		if (servletRequest.getHeader("Authorization") != null) {
			
			String authzHeaderValue = servletRequest.getHeader("Authorization");
			
			if (StringUtils.isBlank(authzHeaderValue)) {
				MISSING_BEARER_TOKEN.apply(servletResponse);
				return false;
			}
			
			try {
				receivedToken = BearerAccessToken.parse(authzHeaderValue);
				
			} catch (ParseException e) {
				MISSING_BEARER_TOKEN.apply(servletResponse);
				return false;
			}
			
		} else if (servletRequest.getParameter("access_token") != null) {
			
			String accessTokenValue = servletRequest.getParameter("access_token");
			
			if (StringUtils.isBlank(accessTokenValue)) {
				MISSING_BEARER_TOKEN.apply(servletResponse);
				return false;
			}
			
			receivedToken = new BearerAccessToken(accessTokenValue);
		} else {
			MISSING_BEARER_TOKEN.apply(servletResponse);
			return false;
		}
		
		// Check receivedToken
		if (null != log) {
			log.trace("[CM3000] Validating bearer access token: {}", TokenAbbreviator.abbreviate(receivedToken));
		}
		
		if (isValid(receivedToken)) {
			return true; // pass
		}
		
		INVALID_BEARER_TOKEN.apply(servletResponse);
		return false;
	}
}
