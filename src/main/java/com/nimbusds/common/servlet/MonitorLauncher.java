package com.nimbusds.common.servlet;


import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.codahale.metrics.graphite.GraphiteSender;
import com.codahale.metrics.graphite.PickledGraphite;
import com.codahale.metrics.jmx.JmxReporter;
import com.nimbusds.common.config.ConfigurationException;
import com.nimbusds.common.config.MonitorConfiguration;
import com.nimbusds.common.monitor.MonitorRegistries;
import io.dropwizard.metrics.servlets.HealthCheckServlet;
import io.dropwizard.metrics.servlets.MetricsServlet;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * Monitor launcher.
 *
 * <ul>
 *     <li>Exports the shared {@link com.nimbusds.common.monitor.MonitorRegistries}
 *         into the servlet context.</li>
 *     <li>Starts JMX reporting if configured.</li>
 *     <li>Starts Graphite reporting if configured.</li>
 * </ul>
 */
public class MonitorLauncher implements ServletContextListener {


	/**
	 * The name of the servlet context parameter that specifies the
	 * configuration file location.
	 */
	public static final String CONFIG_CTX_PARAMETER_NAME = "monitor.configurationFile";


	/**
	 * The JMX reporter.
	 */
	protected JmxReporter jmxReporter;


	/**
	 * The Graphite reporter.
	 */
	protected GraphiteReporter graphiteReporter;


	@Override
	public void contextInitialized(final ServletContextEvent sce) {

		Logger log = LogManager.getLogger("MAIN");

		sce.getServletContext().setAttribute(
			MetricsServlet.METRICS_REGISTRY,
			MonitorRegistries.getMetricRegistry());

		sce.getServletContext().setAttribute(
			HealthCheckServlet.HEALTH_CHECK_REGISTRY,
			MonitorRegistries.getHealthCheckRegistry());
		
		log.info("[CM7106] Started Dropwizard metrics and health checks");

		final Properties props;
		try {
			props = ResourceRetriever.getProperties(
				sce.getServletContext(),
				CONFIG_CTX_PARAMETER_NAME,
				true, // ignore missing properties file
				log);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);
		}

		final MonitorConfiguration config;
		try {
			config = new MonitorConfiguration(props);

		} catch (ConfigurationException e) {
			log.error(e.getMessage(), e);
			throw e;
		}

		// Log configuration params
		config.log();

		// Start JMX reporting if configured
		if (config.enableJMX) {
			jmxReporter = JmxReporter.forRegistry(MonitorRegistries.getMetricRegistry()).build();
			jmxReporter.start();
			log.info("[CM7100] Started JMX reporting with {} metrics", MonitorRegistries.getMetricRegistry().getNames().size());
		} else {
			log.info("[CM7101] JMX metrics reporting disabled");
		}
		
		// Set entry count caching config
		MonitorRegistries.setEntryCountCacheTimeout(config.entryCountCacheTimeout);


		// Start Graphite reporting if configured
		if (config.graphite.enable) {
			final GraphiteSender graphite;
			if (config.graphite.batchSize > 0) {
				// With batching (recommended)
				graphite = new PickledGraphite(config.graphite.host, config.graphite.port, config.graphite.batchSize);
			} else {
				// No batching
				graphite = new Graphite(config.graphite.host, config.graphite.port);
			}

			GraphiteReporter.Builder builder = GraphiteReporter.forRegistry(MonitorRegistries.getMetricRegistry());

			if (config.graphite.prefix != null && ! config.graphite.prefix.isEmpty()) {
				builder = builder.prefixedWith(config.graphite.prefix);
			}

			graphiteReporter = builder.convertRatesTo(config.graphite.ratesTimeUnit)
				.convertDurationsTo(config.graphite.durationsTimeUnit)
				.filter(config.graphite.filter)
				.build(graphite);

			graphiteReporter.start(config.graphite.reportInterval, TimeUnit.SECONDS);

			List<String> filteredNames = MonitorRegistries.getMetricRegistry()
				.getMetrics()
				.entrySet()
				.stream()
				.filter(entry -> config.graphite.filter.matches(entry.getKey(), entry.getValue()))
				.map(Map.Entry::getKey)
				.collect(Collectors.toCollection(LinkedList::new));
			
			if (!filteredNames.isEmpty()) {
				log.info("[CM7102] Started Graphite reporting with {} metrics: {}", filteredNames.size(), filteredNames);
			} else {
				log.warn("[CM7102] Started Graphite reporting, but filter matches zero metrics");
			}
		} else {
			log.info("[CM7103] Graphite metrics reporting disabled");
		}
	}


	@Override
	public void contextDestroyed(final ServletContextEvent sce) {

		Logger log = LogManager.getLogger("MAIN");
		
		MonitorRegistries.getHealthCheckRegistry().shutdown();
		
		log.info("[CM7107] Stopped Dropwizard health checks");

		if (jmxReporter != null) {
			jmxReporter.stop();
			String msg = "[CM7104] Stopped JMX metrics reporting";
			System.out.println(msg);
			log.info(msg);
		}

		if (graphiteReporter != null) {
			graphiteReporter.stop();
			String msg = "[CM7105] Stopped Graphite metrics reporting";
			System.out.println(msg);
			log.info(msg);
		}
	}
}
