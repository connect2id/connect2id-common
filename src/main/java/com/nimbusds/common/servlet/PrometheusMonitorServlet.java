package com.nimbusds.common.servlet;


import com.nimbusds.common.monitor.MonitorRegistries;
import com.nimbusds.common.oauth2.MasterAccessTokenValidator;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.dropwizard.DropwizardExports;
import io.prometheus.client.servlet.jakarta.exporter.MetricsServlet;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;


/**
 * Monitor servlet for exposing Dropwizard metrics in Prometheus TSDB format,
 * requires an OAuth 2.0 bearer token for access.
 */
public class PrometheusMonitorServlet extends MetricsServlet {
	
	
	/**
	 * The access token validator.
	 */
	protected MasterAccessTokenValidator tokenValidator;
	
	
	/**
	 * Returns a configured Prometheus collector registry based on a
	 * Dropwizard metric registry.
	 *
	 * @return The Prometheus collector registry.
	 */
	static CollectorRegistry getConfiguredCollectorRegistry() {
		
		var r = new CollectorRegistry();
		r.register(new DropwizardExports(MonitorRegistries.getMetricRegistry()));
		return r;
	}
	
	
	/**
	 * Monitor servlet for exposing Dropwizard metrics in Prometheus TSDB
	 * format.
	 */
	public PrometheusMonitorServlet() {
		
		super(getConfiguredCollectorRegistry());
	}
	
	
	@Override
	public void init(final ServletConfig config) throws ServletException {
		
		super.init(config);
		
		tokenValidator = MonitorServlet.createAccessTokenValidator(config);
		
		LogManager.getLogger("MAIN").info("[CM7111] Loaded Prometheus monitor API servlet");
	}
	
	
	@Override
	protected void doGet(final HttpServletRequest req,
			     final HttpServletResponse resp)
		throws IOException {
		
		if (! tokenValidator.validateBearerAccessToken(req, resp)) {
			return; // invalid or missing token
		}
		
		super.doGet(req, resp);
	}
	
	
	@Override
	protected void service(final HttpServletRequest req,
			       final HttpServletResponse resp)
		throws ServletException, IOException {
		
		if (! tokenValidator.validateBearerAccessToken(req, resp)) {
			return; // invalid or missing token
		}
		
		super.service(req, resp);
	}
}
