package com.nimbusds.common.servlet;


import com.nimbusds.common.config.ConfigurationException;
import com.nimbusds.common.oauth2.MasterAccessTokenValidator;
import com.nimbusds.common.oauth2.SHA256BasedAccessTokenValidator;
import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;
import io.dropwizard.metrics.servlets.AdminServlet;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Properties;


/**
 * Monitor servlet for exposing Dropwizard metrics and health checks, requires
 * an OAuth 2.0 bearer token for access.
 *
 * <p>The access token is defined by a
 * {@link Configuration#API_TOKEN_SHA256_PROPERTY_NAME Java property} obtained
 * from 1) system properties or from 2) a properties file specified by
 * {@link com.nimbusds.common.servlet.MonitorLauncher#CONFIG_CTX_PARAMETER_NAME
 * servlet context parameter}.
 */
public class MonitorServlet extends AdminServlet {
	
	
	/**
	 * The monitor servlet access token configuration.
	 */
	public static class Configuration {
		
		
		/**
		 * The property name for the SHA-256 hash of the API access
		 * token.
		 */
		public static final String API_TOKEN_SHA256_PROPERTY_NAME = "monitor.apiAccessTokenSHA256";
		
		
		/**
		 * The property name for the SHA-256 hash of the secondary API
		 * access token.
		 */
		public static final String SECONDARY_API_TOKEN_SHA256_PROPERTY_NAME = "monitor.secondaryAPIAccessTokenSHA256";
		
		
		/**
		 * The SHA-256 hash based (in hexadecimal format) monitor API
		 * access token validator, if {@code null} the API is disabled.
		 */
		public final SHA256BasedAccessTokenValidator apiAccessTokenSHA256;
		
		
		/**
		 * Creates a new monitor servlet access token configuration.
		 *
		 * @param properties The configuration properties, can be
		 *                   overridden with system properties.
		 *
		 * @throws ConfigurationException On a invalid configuration
		 *                                property.
		 */
		public Configuration(final Properties properties)
			throws ConfigurationException {
			
			// Retrofit SECONDARY_API_TOKEN_SHA256_PROPERTY_NAME from local / sys properties
			String secondaryTokenHash = properties.getProperty(SECONDARY_API_TOKEN_SHA256_PROPERTY_NAME);
			if (secondaryTokenHash != null && ! secondaryTokenHash.trim().isEmpty()) {
				properties.setProperty(API_TOKEN_SHA256_PROPERTY_NAME + ".xxx", secondaryTokenHash);
			}
			
			secondaryTokenHash = System.getProperties().getProperty(SECONDARY_API_TOKEN_SHA256_PROPERTY_NAME);
			if (secondaryTokenHash != null && ! secondaryTokenHash.trim().isEmpty()) {
				properties.setProperty(API_TOKEN_SHA256_PROPERTY_NAME + ".xxx", secondaryTokenHash);
			}
			
			var pr = new PropertyRetriever(properties, true);
			
			try {
				apiAccessTokenSHA256 = SHA256BasedAccessTokenValidator.from(
					pr,
					API_TOKEN_SHA256_PROPERTY_NAME,
					false,
					API_TOKEN_SHA256_PROPERTY_NAME + ".");
			} catch (PropertyParseException e) {
				throw new ConfigurationException(e.getMessage() + ": Property: " + e.getPropertyKey(), e);
			}
		}
	}


	/**
	 * The access token validator.
	 */
	protected MasterAccessTokenValidator tokenValidator;
	
	
	/**
	 * Creates a new access token validator.
	 *
	 * @param config The servlet configuration, used to retrieve the
	 *               monitor configuration properties.
	 *
	 * @return The basic access token validator.
	 *
	 * @throws ServletException If a configuration property is missing or
	 *                          invalid.
	 */
	static MasterAccessTokenValidator createAccessTokenValidator(final ServletConfig config)
		throws ServletException {
		
		Logger log = LogManager.getLogger("MAIN");
		
		try {
			Properties props = ResourceRetriever.getProperties(
				config.getServletContext(),
				MonitorLauncher.CONFIG_CTX_PARAMETER_NAME,
				true, // ignore missing properties file
				log);
			
			return new Configuration(props).apiAccessTokenSHA256;
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ServletException(e.getMessage(), e);
		}
	}


	@Override
	public void init(final ServletConfig config)
		throws ServletException {

		super.init(config);
		
		tokenValidator = createAccessTokenValidator(config);
		
		LogManager.getLogger("MAIN").info("[CM7110] Loaded monitor API servlet");
	}


	@Override
	protected void doGet(final HttpServletRequest req,
			     final HttpServletResponse resp)
		throws ServletException, IOException {

		if (! tokenValidator.validateBearerAccessToken(req, resp)) {
			return; // invalid or missing token, or web API disabled
		}

		super.doGet(req, resp);
	}


	@Override
	protected void service(final HttpServletRequest req,
			       final HttpServletResponse resp)
		throws ServletException, IOException {

		if (! tokenValidator.validateBearerAccessToken(req, resp)) {
			return; // invalid or missing token, or web API disabled
		}

		super.service(req, resp);
	}
}
