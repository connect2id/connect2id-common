package com.nimbusds.common.servlet;


import com.nimbusds.common.http.ForwardedParser;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.Principal;
import java.security.cert.X509Certificate;


/**
 * Helper methods for Log4j message generation.
 */
public class ServletLogUtility {


	/**
	 * The logger.
	 */
	private static final Logger LOG = LogManager.getLogger("MAIN");


	/**
	 * Composes a log message with the method, the client IP address, CORS
	 * origin (if any and a CORS Filter is configured) and the security
	 * client certificate principal (if any) of an HTTP request.
	 *
	 * <p>Example:
	 *
	 * <pre>
	 * HTTP POST request: ip=127.0.0.1 path=/json2ldap
	 * HTTPS POST request: ip=192.168.0.1 path=/authservice
	 * HTTPS CORS GET request: ip=192.168.0.1 origin=192.168.10.20 path=/json2ldap
	 * </pre>
	 *
	 * @param httpRequest The HTTP request. Must not be {@code null}.
	 *
	 * @return The log message.
	 */
	public static String composeLogMessage(final HttpServletRequest httpRequest) {

		final String corsOrigin = String.valueOf(httpRequest.getAttribute("cors.origin"));

		StringBuilder msg = new StringBuilder("HTTP");

		if (httpRequest.isSecure())
			msg.append("S");

		if (! corsOrigin.equals("null"))
			msg.append(" CORS");

		msg.append(' ');
		msg.append(httpRequest.getMethod());

		msg.append(" request: ip=");
		// Try RFC 7239 header
		String clientIP = ForwardedParser.getClientIPAddressFromForwardedHeader(httpRequest.getHeader("Forwarded"));
		if (clientIP == null) {
			// Try X-Forwarded-For header
			clientIP = ForwardedParser.getClientIPAddressFromXForwardedForHeader(httpRequest.getHeader("X-Forwarded-For"));
			if (clientIP == null) {
				// Remote IP address reported by HTTP
				clientIP = httpRequest.getRemoteAddr();
			}
		}
		msg.append(clientIP);

		if (! corsOrigin.equals("null")) {
			msg.append(" origin=");
			msg.append(corsOrigin);
		}

		X509Certificate[] certs =
			(X509Certificate[])httpRequest.getAttribute("javax.servlet.request.X509Certificate");

		if (certs != null) {

			for (int i=0; i < certs.length; i++) {

				Principal principal = certs[i].getSubjectX500Principal();

				if (principal == null)
					continue;

				String principalName = principal.getName();

				msg.append(" principal[" + i + "]=" + principalName);
			}
		}

		msg.append(" path=");
		msg.append(httpRequest.getRequestURI());

		return msg.toString();
	}
	
	
	/**
	 * Logs (at INFO level) the method, the client IP address, CORS origin
	 * (if any and a CORS Filter is configured) and the security client
	 * certificate principal (if any) of an HTTP request.
	 *
	 * <p>Example:
	 *
	 * <pre>
	 * HTTP POST request: ip=127.0.0.1 path=/json2ldap
	 * HTTPS POST request: ip=192.168.0.1 path=/authservice
	 * HTTPS CORS GET request: ip=192.168.0.1 origin=192.168.10.20 path=/json2ldap
	 * </pre>
	 *
	 * @param httpRequest The HTTP request to log. If {@code null} the
	 *                    method will return immediately.
	 */
	public static void log(final HttpServletRequest httpRequest) {

		log(LOG, httpRequest);
	}


	/**
	 * Logs (at INFO level) the method, the client IP address, CORS origin
	 * (if any and a CORS Filter is configured) and the security client
	 * certificate principal (if any) of an HTTP request.
	 *
	 * <p>Example:
	 *
	 * <pre>
	 * HTTP POST request: ip=127.0.0.1 path=/json2ldap
	 * HTTPS POST request: ip=192.168.0.1 path=/authservice
	 * HTTPS CORS GET request: ip=192.168.0.1 origin=192.168.10.20 path=/json2ldap
	 * </pre>
	 *
	 * @param log         The logger. If {@code true} the method will
	 *                    return immediately.
	 * @param httpRequest The HTTP request to log. If {@code null} the
	 *                    method will return immediately.
	 */
	public static void log(final Logger log, final HttpServletRequest httpRequest) {

		if (log == null || httpRequest == null)
			return;
		
		if (! log.isInfoEnabled())
			return;

		log.info(composeLogMessage(httpRequest));
	}


	/**
	 * Prevents public instantiation.
	 */
	private ServletLogUtility() { }
}
