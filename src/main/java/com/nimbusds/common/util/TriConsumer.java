package com.nimbusds.common.util;


import java.util.Objects;


/**
 * Represents an operation that accepts three input arguments and returns no
 * result. Functional interface similar to
 * {@linkplain java.util.function.Consumer} and
 * {@linkplain java.util.function.BiConsumer}.
 */
@FunctionalInterface
public interface TriConsumer<T, U, V> {
	
	
	/**
	 * Performs this operation on the given arguments.
	 */
	void accept(T var1, U var2, V var3);
	
	
	/**
	 * Returns a composed {@linkplain TriConsumer} that performs, in
	 * sequence, this operation followed by the after operation.
	 */
	default TriConsumer<T, U, V> andThen(TriConsumer<? super T, ? super U, ? super V> after) {
		Objects.requireNonNull(after);
		return (l, m, r) -> {
			this.accept(l, m, r);
			after.accept(l, m, r);
		};
	}
}