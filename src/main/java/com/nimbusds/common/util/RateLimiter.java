package com.nimbusds.common.util;


/**
 * Rate limiter interface.
 */
public interface RateLimiter {
	
	
	/**
	 * Acquires a new permit, may cause the thread to block.
	 */
	void acquirePermit();
}
