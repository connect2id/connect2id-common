package com.nimbusds.common.infinispan;


import org.infinispan.Cache;
import org.infinispan.configuration.cache.StoreConfiguration;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.persistence.redis.configuration.CommonRedisStoreConfiguration;
import org.infinispan.persistence.redis.configuration.RedisServerConfiguration;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * Redis cache store checks.
 *
 * <p>See https://github.com/infinispan/infinispan-cachestore-redis notes.
 *
 * <p>When using AWS ElastiCache, an explicit non-zero database index must be
 * specified for all Redis Store configurations. AWS ElastiCache inserts a
 * special ElastiCacheMasterReplicationTimestamp key in the default database
 * (at zero index) to aid replication, which may lead to unexpected
 * unmarshalling IO exceptions when the Infinispan cache needs to iterate over
 * the stored keys.
 */
public class RedisCacheStoreChecks {
	
	
	/**
	 * Configuration warning.
	 */
	public static class ConfigWarning extends Exception {
		
		public ConfigWarning(final String message) {
			super(message);
		}
	}
	
	
	/**
	 * Configuration exception.
	 */
	public static class ConfigError extends RuntimeException {
		
		public ConfigError(final String message) {
			super(message);
		}
	}
	
	
	/**
	 * Checks the configured Redis cache stores to ensure each uses its own
	 * database number and that number is non-zero.
	 *
	 * @param cacheManager The initialised Infinispan cache manager.
	 *
	 * @throws ConfigWarning If a Redis cache store with zero database
	 *                       number was detected.
	 * @throws ConfigError   If a Redis cache store with a clashing
	 *                       database number was detected.
	 */
	public static void check(final EmbeddedCacheManager cacheManager)
		throws ConfigWarning, ConfigError{
		
		Map<String,Set<Integer>> hostToDbIndexMappings = new HashMap<>();
		
		for (String cacheName: cacheManager.getCacheNames()) {
			
			Cache<?,?> cache = cacheManager.getCache(cacheName);
			
			for (StoreConfiguration storeConfig: cache.getCacheConfiguration().persistence().stores()) {
				
				if (! (storeConfig instanceof CommonRedisStoreConfiguration redisStoreConfig)) {
					continue; // skip
				}

				if (redisStoreConfig.database() == 0) {
					throw new ConfigWarning(
						"The Redis Cache store for " + cacheName + " must specify a non-zero " +
						"database number to prevent potential conflicts with meta keys such " +
						"as ElastiCacheMasterReplicationTimestamp that may get put into " +
						"the default (0) database");
				}
				
				for (RedisServerConfiguration redisServerConfig: redisStoreConfig.servers()) {
					
					String host = redisServerConfig.host() + ":" + redisServerConfig.port();
					
					Set<Integer> dbNums = hostToDbIndexMappings.get(host);
					
					if (dbNums == null) {
						dbNums = new HashSet<>();
					}
					
					hostToDbIndexMappings.put(host, dbNums);
					
					if (dbNums.contains(redisStoreConfig.database())) {
						throw new ConfigError("The Redis Cache store " + host +
							" for " + cacheName + " uses a database number " +
							redisStoreConfig.database() + " that is already taken");
					}
					
					dbNums.add(redisStoreConfig.database());
				}
			}
		}
	}
	
	
	/**
	 * Prevents public instantiation.
	 */
	private RedisCacheStoreChecks() {}
}
