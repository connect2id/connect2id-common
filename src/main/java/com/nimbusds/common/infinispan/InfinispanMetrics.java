package com.nimbusds.common.infinispan;


import com.codahale.metrics.Gauge;
import com.nimbusds.common.monitor.MonitorRegistries;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.remoting.transport.Address;

import java.util.List;


/**
 * Infinispan cluster metrics.
 */
public class InfinispanMetrics {
	
	
	/**
	 * Registers Infinispan cluster metrics "infinispan.isCoordinator" and
	 * "infinispan.numClusterMembers" for the specified cache manager.
	 *
	 * @param cacheManager The cache manager.
	 */
	public static void register(final EmbeddedCacheManager cacheManager) {
		
		MonitorRegistries.register("infinispan.isCoordinator", (Gauge<Boolean>) cacheManager::isCoordinator);
		
		MonitorRegistries.register("infinispan.numClusterMembers", (Gauge<Integer>) () -> {
			List<Address> members = cacheManager.getMembers();
			
			if (members == null) {
				return 0; // no cluster
			}
			
			return members.size();
		});
	}
	
	
	/**
	 * Prevents public instantiation.
	 */
	private InfinispanMetrics() {
	}
}
