package com.nimbusds.common.infinispan;


import org.apache.logging.log4j.Logger;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.CacheMode;

import java.util.List;


/**
 * Infinispan cache workarounds.
 *
 * <ul>
 *     <li>Fix around invalidation bug, to force cache load before putIfAbsent,
 *         replace, delete operations. See tracking Connect2id server issue
 *         https://bitbucket.org/connect2id/server/issues/239/race-condition-in-cacheremove-in
 *     <li>Enforcing zero cache size for "stateless mode" to enable Infinispan
 *         to be run in local mode with no in-memory store, using the
 *         configured stores instead.
 * </ul>
 */
public class CacheWorkArounds<K, V> {
	
	
	/**
	 * The handled special modes.
	 */
	public enum Mode {
		INVALIDATION,
		STATELESS
	}
	
	
	/**
	 * Checks if the specified cache is configured in invalidation mode.
	 *
	 * @param cache The cache to check.
	 *
	 * @return {@code true} if the cache is configured in invalidation
	 *         mode, else {@code false}.
	 */
	public static boolean detectInvalidationMode(final Cache<?,?> cache) {
		
		return List.of(CacheMode.INVALIDATION_SYNC, CacheMode.INVALIDATION_ASYNC)
			.contains(cache.getCacheConfiguration().clustering().cacheMode());
	}
	
	
	/**
	 * Checks if the specified cache is configured in "stateless mode".
	 *
	 * @param cache The cache to check.
	 *
	 * @return {@code true} if the cache is configured in "stateless mode",
	 *         else {@code false}.
	 */
	public static boolean detectStatelessMode(final Cache<?,?> cache) {
		
		final boolean isLocal = CacheMode.LOCAL.equals(cache.getCacheConfiguration().clustering().cacheMode());
		
		final boolean isSizeOne =
			(1 == cache.getCacheConfiguration().memory().size()) // legacy
			||
			(1 == cache.getCacheConfiguration().memory().maxCount());

                return isLocal && isSizeOne;
        }
	
	
	/**
	 * The underlying Infinispan cache.
	 */
	private final Cache<K, V> infinispanCache;
	
	
	/**
	 * The special mode, {@code null} if none.
	 */
	private final Mode mode;
	
	
	/**
	 * Creates a new cache workarounds instance.
	 *
	 * @param infinispanCache The Infinispan cache.
	 */
	public CacheWorkArounds(final Cache<K, V> infinispanCache) {
		this(infinispanCache, null);
	}
	
	
	/**
	 * Creates a new cache workarounds instance.
	 *
	 * @param infinispanCache The Infinispan cache.
	 * @param log             Optional logger for the detection,
	 *                        {@code null} if not specified.
	 */
	public CacheWorkArounds(final Cache<K, V> infinispanCache, final Logger log) {
		this.infinispanCache = infinispanCache;
		if (detectInvalidationMode(infinispanCache)) {
			mode = Mode.INVALIDATION;
		} else if (detectStatelessMode(infinispanCache)) {
			mode = Mode.STATELESS;
		} else {
			mode = null;
		}
		if (log != null && mode != null) {
			log.info("[CM8020] Detected Infinispan cache {} in {} mode", infinispanCache.getName(), mode);
		}
	}
	
	
	/**
	 * Returns the special workaround mode.
	 *
	 * @return The workaround mode, {@code null} if none (implies follow
	 *         regular operation).
	 */
	public Mode getMode() {
		return mode;
	}
	
	
	/**
	 * Returns {@code true} if the cache is in invalidation mode.
	 *
	 * @return {@code true} if the cache is in invalidation mode, else
	 *         {@code false}.
	 */
	public boolean isInvalidation() {
		
		return Mode.INVALIDATION.equals(getMode());
	}
	
	
	/**
	 * Returns {@code true} if the cache is in the special "stateless
	 * mode".
	 *
	 * @return {@code true} if the cache is in the "stateless mode", else
	 *         {@code false}.
	 */
	public boolean isStateless() {
		
		return Mode.STATELESS.equals(getMode());
	}
	
	
	/**
	 * If the cache is in the special "stateless mode" causes the
	 * underlying data container (in memory) to be cleared. Should be
	 * called before get, replace, putIfAbsent, remove and iteration
	 * operations.
	 */
	public void clearCacheIfStateless() {
		
		if (! isStateless()) {
			return;
		}
		
		infinispanCache.getAdvancedCache().getDataContainer().clear();
	}
}
