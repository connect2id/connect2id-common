package com.nimbusds.common.infinispan;


import com.codahale.metrics.health.HealthCheck;
import com.nimbusds.common.monitor.MonitorRegistries;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.partitionhandling.AvailabilityMode;


/**
 * Infinispan health checks.
 */
public class InfinispanHealthChecks {
	
	
	/**
	 * Registers Infinispan health checks
	 * "infinispan.[cache-name].availability" for the specified cache
	 * manager.
	 *
	 * @param cacheManager The cache manager.
	 */
	public static void register(final EmbeddedCacheManager cacheManager) {
	
		cacheManager.getCacheNames().forEach(cacheName -> {
			
			var healthCheckName = cacheName + ".availability";
			
			MonitorRegistries.register(healthCheckName, new HealthCheck() {
				@Override
				protected Result check() {
					
					AvailabilityMode am = cacheManager.getCache(cacheName).getAdvancedCache().getAvailability();
					
					if (AvailabilityMode.DEGRADED_MODE.equals(am)) {
						return Result.unhealthy("degraded");
					}
					
					if (AvailabilityMode.AVAILABLE.equals(am)) {
						return Result.healthy();
					}
					
					return Result.unhealthy("unavailable"); // null
				}
			});
		});
	}
	
	
	/**
	 * Prevents public instantiation.
	 */
	private InfinispanHealthChecks() {
	}
}
