package com.nimbusds.common.store;


import junit.framework.TestCase;


/**
 * Tests the store exception.
 */
public class StoreExceptionTest extends TestCase {


	public void testUnchecked() {

		assertTrue(new StoreException("message") instanceof RuntimeException);
	}


	public void testMessage() {

		assertEquals("message", new StoreException("message").getMessage());
	}


	public void testCause() {

		Throwable cause = new Throwable("cause");

		assertEquals(cause, new StoreException("message", cause).getCause());
	}
}
