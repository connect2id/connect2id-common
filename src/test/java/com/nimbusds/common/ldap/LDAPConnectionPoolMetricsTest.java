package com.nimbusds.common.ldap;


import com.codahale.metrics.Gauge;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricSet;
import com.nimbusds.common.monitor.MonitorRegistries;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionPool;
import com.unboundid.ldap.sdk.SingleServerSet;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;


/**
 * Tests the LDAP connection pool metrics.
 */
public class LDAPConnectionPoolMetricsTest extends TestWithLDAPServer {


	@Test
	public void testRegistration()
		throws Exception {

		LDAPConnectionPool pool = new LDAPConnectionPool(new LDAPConnection("localhost", testLDAPServer.getListenPort()), 1, 10);

		final String prefix = "authzStore.ldapConnector";

		MetricSet metricSet = new LDAPConnectionPoolMetrics(pool, prefix);

		Map<String,Metric> metricMap = metricSet.getMetrics();

		assertTrue(metricMap.get(prefix + ".maxAvailableConnections") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numAvailableConnections") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numConnectionsClosedDefunct") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numConnectionsClosedExpired") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numConnectionsClosedUnneeded") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numFailedCheckouts") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numFailedConnectionAttempts") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numReleasedValid") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numSuccessfulCheckouts") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numSuccessfulCheckoutsNewConnection") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numSuccessfulCheckoutsWithoutWaiting") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numSuccessfulCheckoutsAfterWaiting") instanceof Gauge);
		assertTrue(metricMap.get(prefix + ".numSuccessfulConnectionAttempts") instanceof Gauge);

		assertEquals(13, metricMap.size());

		// Test register
		MonitorRegistries.register(metricSet);

		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".maxAvailableConnections"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numAvailableConnections"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numConnectionsClosedDefunct"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numConnectionsClosedExpired"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numConnectionsClosedUnneeded"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numFailedCheckouts"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numFailedConnectionAttempts"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numReleasedValid"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numSuccessfulCheckouts"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numSuccessfulCheckoutsNewConnection"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numSuccessfulCheckoutsWithoutWaiting"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numSuccessfulCheckoutsAfterWaiting"));
		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains(prefix + ".numSuccessfulConnectionAttempts"));
	}


	@Test
	public void testSuccessfulCheckoutAndRelease()
		throws Exception {

		LDAPConnectionPool pool = new LDAPConnectionPool(new LDAPConnection("localhost", testLDAPServer.getListenPort()), 1, 10);
		assertTrue(pool.getCreateIfNecessary());
		final String prefix = "authzStore.ldapConnector";
		MetricSet metricSet = new LDAPConnectionPoolMetrics(pool, prefix);
		MonitorRegistries.register(metricSet);

		// Initial state
		assertEquals(10, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".maxAvailableConnections").getValue());
		assertEquals(1, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numAvailableConnections").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedDefunct").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedExpired").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedUnneeded").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedConnectionAttempts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numReleasedValid").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsNewConnection").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsWithoutWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsAfterWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulConnectionAttempts").getValue());

		// Check out connection and perform operation
		LDAPConnection con = pool.getConnection();
		con.getRootDSE();

		assertEquals(10, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".maxAvailableConnections").getValue());
		assertEquals(0, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numAvailableConnections").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedDefunct").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedExpired").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedUnneeded").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedConnectionAttempts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numReleasedValid").getValue());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsNewConnection").getValue());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsWithoutWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsAfterWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulConnectionAttempts").getValue());

		// Release connection
		pool.releaseConnection(con);

		assertEquals(10, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".maxAvailableConnections").getValue());
		assertEquals(1, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numAvailableConnections").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedDefunct").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedExpired").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedUnneeded").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedConnectionAttempts").getValue());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numReleasedValid").getValue());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsNewConnection").getValue());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsWithoutWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsAfterWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulConnectionAttempts").getValue());

		// Perform implicit checkout via request on pool
		pool.getRootDSE();

		assertEquals(10, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".maxAvailableConnections").getValue());
		assertEquals(1, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numAvailableConnections").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedDefunct").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedExpired").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedUnneeded").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedConnectionAttempts").getValue());
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numReleasedValid").getValue());
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsNewConnection").getValue());
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsWithoutWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsAfterWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulConnectionAttempts").getValue());
	}


	@Test
	public void testInitialConnectionFailure()
		throws Exception {

		// Shutdown LDAP server before creating connection pool
		int port = testLDAPServer.getListenPort();

		testLDAPServer.shutDown(true);

		Thread.sleep(50);

		LDAPConnectionPool pool = new LDAPConnectionPool(
			new SingleServerSet("localhost", port),
			null,
			1,
			10,
			null,
			false); // throwOnConnectFailure - suppress constructor exception!

		Thread.sleep(50);

		assertEquals(1L, pool.getConnectionPoolStatistics().getNumFailedConnectionAttempts()); // LDAP SDK bug fixed in v3.1.1

		final String prefix = "authzStore.ldapConnector";

		MetricSet metricSet = new LDAPConnectionPoolMetrics(pool, prefix);

		MonitorRegistries.register(metricSet);

		assertEquals(10, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".maxAvailableConnections").getValue());
		assertEquals(0, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numAvailableConnections").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedDefunct").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedExpired").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedUnneeded").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedCheckouts").getValue());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedConnectionAttempts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numReleasedValid").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsNewConnection").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsWithoutWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsAfterWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulConnectionAttempts").getValue());

		// Try explicit checkout
		try {
			pool.getConnection();
			fail();
		} catch (Exception e) {
			// ignore
		}

		assertEquals(2L, pool.getConnectionPoolStatistics().getNumFailedConnectionAttempts()); // LDAP SDK bug fixed in v3.1.1

		assertEquals(10, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".maxAvailableConnections").getValue());
		assertEquals(0, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numAvailableConnections").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedDefunct").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedExpired").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedUnneeded").getValue());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedCheckouts").getValue());
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedConnectionAttempts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numReleasedValid").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsNewConnection").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsWithoutWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsAfterWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulConnectionAttempts").getValue());


		// Try implicit checkout
		try {
			pool.getRootDSE();
			fail();
		} catch (Exception e) {
			// ignore
		}

		assertEquals(3L, pool.getConnectionPoolStatistics().getNumFailedConnectionAttempts()); // LDAP SDK bug fixed in v3.1.1

		assertEquals(10, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".maxAvailableConnections").getValue());
		assertEquals(0, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numAvailableConnections").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedDefunct").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedExpired").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedUnneeded").getValue());
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedCheckouts").getValue());
		assertEquals(3L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedConnectionAttempts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numReleasedValid").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsNewConnection").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsWithoutWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsAfterWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulConnectionAttempts").getValue());
	}


	@Test
	public void testConnectionFailure()
		throws Exception {

		// Shutdown LDAP server before creating connection pool
		int port = testLDAPServer.getListenPort();

		LDAPConnectionPool pool = new LDAPConnectionPool(
			new SingleServerSet("localhost", port),
			null,
			1,
			10,
			null,
			false); // throwOnConnectFailure - suppress constructor exception!

		final String prefix = "authzStore.ldapConnector";

		MetricSet metricSet = new LDAPConnectionPoolMetrics(pool, prefix);

		MonitorRegistries.register(metricSet);

		assertEquals(10, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".maxAvailableConnections").getValue());
		assertEquals(1, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numAvailableConnections").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedDefunct").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedExpired").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedUnneeded").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedConnectionAttempts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numReleasedValid").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsNewConnection").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsWithoutWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsAfterWaiting").getValue());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulConnectionAttempts").getValue());

		testLDAPServer.shutDown(true);

		Thread.sleep(50);

		// Try explicit checkout
		try {
			pool.getConnection();
			fail();
		} catch (Exception e) {
			// ignore
		}

		assertEquals(10, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".maxAvailableConnections").getValue());
		assertEquals(0, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numAvailableConnections").getValue());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedDefunct").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedExpired").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedUnneeded").getValue());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedCheckouts").getValue());
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedConnectionAttempts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numReleasedValid").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsNewConnection").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsWithoutWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsAfterWaiting").getValue());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulConnectionAttempts").getValue());


		// Try implicit checkout
		try {
			pool.getRootDSE();
			fail();
		} catch (Exception e) {
			// ignore
		}

		assertEquals(3L, pool.getConnectionPoolStatistics().getNumFailedConnectionAttempts()); // LDAP SDK bug fixed in v3.1.1

		assertEquals(10, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".maxAvailableConnections").getValue());
		assertEquals(0, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numAvailableConnections").getValue());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedDefunct").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedExpired").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numConnectionsClosedUnneeded").getValue());
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedCheckouts").getValue());
		assertEquals(3L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numFailedConnectionAttempts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numReleasedValid").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckouts").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsNewConnection").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsWithoutWaiting").getValue());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulCheckoutsAfterWaiting").getValue());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getGauges().get(prefix + ".numSuccessfulConnectionAttempts").getValue());
	}
}
