package com.nimbusds.common.ldap;


import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


/**
 * Tests the schema utility.
 */
public class SchemaUtilsTest extends TestWithLDAPServer {


	@Test
	public void testDumpTopObjectClass()
		throws Exception {

		assertEquals("( 2.5.6.0 NAME 'top' ABSTRACT MUST objectClass X-ORIGIN 'RFC 4512' )",
			SchemaUtils.dumpObjectClassDefinition(testLDAPServer.getConnection(), "top"));
	}


	@Test
	public void testDumpNoSuchObjectClass()
		throws Exception {

		assertEquals("No such objectClass: XClass", SchemaUtils.dumpObjectClassDefinition(testLDAPServer.getConnection(), "XClass"));
	}
}
