package com.nimbusds.common.ldap;


import com.nimbusds.common.config.CustomKeyStoreConfiguration;
import com.nimbusds.common.config.CustomTrustStoreConfiguration;
import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import junit.framework.TestCase;

import java.util.Properties;


/**
 * Tests the LDAP connection factory.
 */
public class LDAPConnectionFactoryTest extends TestCase {


	private LDAPConnectionFactory createLDAPConnectionFactory()
		throws Exception {

		CustomTrustStoreConfiguration customTrustStore = 
			new CustomTrustStoreConfiguration("", new Properties());

		CustomKeyStoreConfiguration customKeyStore =
			new CustomKeyStoreConfiguration("", new Properties());

		return new LDAPConnectionFactory(customTrustStore, customKeyStore);
	}


	private InMemoryDirectoryServer createTestServer()
		throws LDAPException {

		InMemoryDirectoryServerConfig config =
			new InMemoryDirectoryServerConfig("dc=wonderland,dc=net");

 		config.addAdditionalBindCredentials("cn=Directory Manager", "secret");

 		InMemoryDirectoryServer server = new InMemoryDirectoryServer(config);

 		// Start the server so it will accept client connections.
 		server.startListening();

 		System.out.println("Started test LDAP server on port " + server.getListenPort());

 		return server;
	}


	public void testSocketFactory()
		throws Exception {

		CustomTrustStoreConfiguration ts = 
			new CustomTrustStoreConfiguration("", new Properties());

		CustomKeyStoreConfiguration ks =
			new CustomKeyStoreConfiguration("", new Properties());

		// NONE
		assertNull(LDAPConnectionFactory.getSocketFactory(LDAPConnectionSecurity.NONE, ts, ks, false));

		// Start TLS
		assertNull(LDAPConnectionFactory.getSocketFactory(LDAPConnectionSecurity.STARTTLS, ts, ks, false));

		// SSL
		assertNotNull(LDAPConnectionFactory.getSocketFactory(LDAPConnectionSecurity.SSL, ts, ks, false));
	}


	public void testCreatePlainConnection()
		throws Exception {

		LDAPConnectionFactory factory = createLDAPConnectionFactory();

		assertNotNull(factory.getCustomTrustStoreConfiguration());
		assertNotNull(factory.getCustomKeyStoreConfiguration());

		InMemoryDirectoryServer server = createTestServer();

		String host = "localhost";
		int port = server.getListenPort();

		try {
			LDAPConnection con = factory.createLDAPConnection(host, port, LDAPConnectionSecurity.NONE, 0, false);

			assertNotNull("LDAP connection not null", con);

			assertEquals("LDAP server port", port, con.getConnectedPort());

			assertTrue("LDAP connection active", con.isConnected());

			con.close();

		} finally {

			server.shutDown(true);
		}
	}
}