package com.nimbusds.common.ldap;


import junit.framework.TestCase;


/**
 * Tests the filter template class.
 */
public class FilterTemplateTest extends TestCase {
	

	public void testGoodFilterTemplate() {
	
		String template = "(uid=%u)";
		String placeholder = "%u";
		
		FilterTemplate t = new FilterTemplate(template, placeholder);
		
		assertEquals(template, t.getTemplate());
		assertEquals(placeholder, t.getPlaceholder());
		
		assertEquals(template, t.toString());
		
		assertEquals("(uid=alice)", t.apply("alice"));
	}


	public void testBadFilterTemplate() {
	
		try {
			new FilterTemplate("foobar%u", "%u");
			
			fail("Failed to raise exception");
			
		} catch (IllegalArgumentException e) {
		
			// ok
		}
	}
	
	
	public void testMissingPlaceholder() {
	
		try {
			new FilterTemplate("(uid=alice)", "%u");
			
			fail("Failed to raise exception");
			
		} catch (IllegalArgumentException e) {
		
			// ok
		}
	}
}
