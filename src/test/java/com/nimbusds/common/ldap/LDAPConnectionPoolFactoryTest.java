package com.nimbusds.common.ldap;


import com.nimbusds.common.config.CustomKeyStoreConfiguration;
import com.nimbusds.common.config.CustomTrustStoreConfiguration;
import com.nimbusds.common.config.LDAPServerConnectionPoolDetails;
import com.nimbusds.common.config.ServerSelectionAlgorithm;
import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.LDAPConnectionPool;
import org.junit.Test;

import java.util.Properties;

import static org.junit.Assert.assertEquals;


/**
 * Tests the LDAP connection pool factory.
 */
public class LDAPConnectionPoolFactoryTest extends TestWithLDAPServer {
	

	@Test
	public void testConnect()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("ldap.url", "ldap://localhost:" + testLDAPServer.getListenPort());
		props.setProperty("ldap.security", "NONE");
		props.setProperty("ldap.connectTimeout", "100");
		props.setProperty("ldap.responseTimeout", "200");
		props.setProperty("ldap.connectionPoolInitialSize", "5");

		LDAPServerConnectionPoolDetails config = new LDAPServerConnectionPoolDetails("ldap.", props);
		assertEquals(100, config.connectTimeout);
		assertEquals(200, config.responseTimeout);
		assertEquals(5, config.connectionPoolSize);
		assertEquals(5, config.connectionPoolInitialSize);

		LDAPConnectionPoolFactory factory = new LDAPConnectionPoolFactory(config,
			new CustomTrustStoreConfiguration("ldap", props),
			new CustomKeyStoreConfiguration("ldap", props),
			null);

		LDAPConnectionPool pool = factory.createLDAPConnectionPool();

		pool.getRootDSE();

		assertEquals(100, pool.getConnection().getConnectionOptions().getConnectTimeoutMillis());
		assertEquals(200, pool.getConnection().getConnectionOptions().getResponseTimeoutMillis());
		assertEquals(LDAPServerConnectionPoolDetails.DEFAULT_CONNECTION_POOL_SIZE, pool.getConnectionPoolStatistics().getMaximumAvailableConnections());
		assertEquals(3, pool.getCurrentAvailableConnections()); // TODO why 3 not 5?
		assertEquals(3, pool.getConnectionPoolStatistics().getNumAvailableConnections());
	}


	@Test
	public void testCreateWithLDAPServerDown()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("ldap.url", "ldap://localhost:" + testLDAPServer.getListenPort());
		props.setProperty("ldap.security", "NONE");
		props.setProperty("ldap.connectTimeout", "100");
		props.setProperty("ldap.responseTimeout", "200");

		testLDAPServer.shutDown(true);

		LDAPServerConnectionPoolDetails config = new LDAPServerConnectionPoolDetails("ldap.", props);
		assertEquals(100, config.connectTimeout);
		assertEquals(200, config.responseTimeout);
		assertEquals(LDAPServerConnectionPoolDetails.DEFAULT_CONNECTION_POOL_SIZE, config.connectionPoolSize);
		assertEquals(0, config.connectionPoolInitialSize);

		LDAPConnectionPoolFactory factory = new LDAPConnectionPoolFactory(config,
			new CustomTrustStoreConfiguration("ldap", props),
			new CustomKeyStoreConfiguration("ldap", props),
			null);

		factory.createLDAPConnectionPool();
	}


	@Test
	public void testCreateWithLDAPServerDown_zeroInitialConnections()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("ldap.url", "ldap://localhost:" + testLDAPServer.getListenPort());
		props.setProperty("ldap.security", "NONE");
		props.setProperty("ldap.connectionPoolSize", "10");
		props.setProperty("ldap.connectionPoolInitialSize", "0");
		props.setProperty("ldap.connectTimeout", "100");
		props.setProperty("ldap.responseTimeout", "200");

		testLDAPServer.shutDown(true);

		LDAPServerConnectionPoolDetails config = new LDAPServerConnectionPoolDetails("ldap.", props);
		assertEquals(100, config.connectTimeout);
		assertEquals(200, config.responseTimeout);
		assertEquals(10, config.connectionPoolSize);
		assertEquals(0, config.connectionPoolInitialSize);

		LDAPConnectionPoolFactory factory = new LDAPConnectionPoolFactory(config,
			new CustomTrustStoreConfiguration("ldap", props),
			new CustomKeyStoreConfiguration("ldap", props),
			null);

		LDAPConnectionPool pool = factory.createLDAPConnectionPool();

		assertEquals(0, pool.getCurrentAvailableConnections());
	}


	@Test
	public void testRoundRobin()
		throws Exception {

		InMemoryDirectoryServer secondTestLDAPServer = createLDAPServer();

		Properties props = new Properties();
		props.setProperty("ldap.url", "ldap://localhost:" + testLDAPServer.getListenPort() + " ldap://localhost:" + secondTestLDAPServer.getListenPort());
		props.setProperty("ldap.selectionAlgorithm", "ROUND_ROBIN");
		props.setProperty("ldap.security", "NONE");
		props.setProperty("ldap.connectTimeout", "100");
		props.setProperty("ldap.responseTimeout", "200");
		props.setProperty("ldap.connectionPoolSize", "10");
		props.setProperty("ldap.connectionPoolInitialSize", "5");

		LDAPServerConnectionPoolDetails config = new LDAPServerConnectionPoolDetails("ldap.", props);

		assertEquals(ServerSelectionAlgorithm.ROUND_ROBIN, config.selectionAlgorithm);

		Entry rootEntry = new Entry("dc=wonderland,dc=net");
		rootEntry.addAttribute("objectClass", "top", "domain");
		rootEntry.addAttribute("dc", "wonderland");
		testLDAPServer.add(rootEntry);
		secondTestLDAPServer.add(rootEntry);

		Entry entry1 = new Entry("uid=alice,dc=wonderland,dc=net");
		entry1.setAttribute("objectClass", "top", "account");
		entry1.setAttribute("uid", "alice");
		entry1.setAttribute("description", "1");
		testLDAPServer.add(entry1);

		Entry entry2 = new Entry("uid=alice,dc=wonderland,dc=net");
		entry2.setAttribute("objectClass", "top", "account");
		entry2.setAttribute("uid", "alice");
		entry2.setAttribute("description", "2");
		secondTestLDAPServer.add(entry2);

		LDAPConnectionPoolFactory factory = new LDAPConnectionPoolFactory(config,
			new CustomTrustStoreConfiguration("ldap", props),
			new CustomKeyStoreConfiguration("ldap", props),
			null);

		LDAPConnectionPool pool = factory.createLDAPConnectionPool();

		assertEquals("1", pool.getEntry("uid=alice,dc=wonderland,dc=net").getAttributeValue("description"));
		assertEquals("2", pool.getEntry("uid=alice,dc=wonderland,dc=net").getAttributeValue("description"));
		assertEquals("1", pool.getEntry("uid=alice,dc=wonderland,dc=net").getAttributeValue("description"));
		assertEquals("2", pool.getEntry("uid=alice,dc=wonderland,dc=net").getAttributeValue("description"));

		assertEquals(5, pool.getCurrentAvailableConnections());

		secondTestLDAPServer.shutDown(true);
	}
}
