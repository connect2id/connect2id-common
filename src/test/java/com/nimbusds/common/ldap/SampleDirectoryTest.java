package com.nimbusds.common.ldap;


import com.thetransactioncompany.util.PropertyParseException;
import com.unboundid.ldap.sdk.*;
import jakarta.servlet.Filter;
import jakarta.servlet.*;
import jakarta.servlet.descriptor.JspConfigDescriptor;
import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;


public class SampleDirectoryTest extends TestCase {
	
	
	private static final String EXAMPLE_CONFIG =
		"### Sample LDAP directory configuration ###\n" +
		"\n" +
		"sampleDirectoryServer.enable = true\n" +
		"sampleDirectoryServer.port = 44389\n" +
		"sampleDirectoryServer.schema = /WEB-INF/sampleDirectorySchema.ldif\n" +
		"sampleDirectoryServer.baseDN = dc=wonderland,dc=net\n" +
		"sampleDirectoryServer.content = /WEB-INF/sampleDirectoryContent.ldif\n" +
		"\n";
	
	
	@Override
	public void tearDown()
		throws Exception {
		
		super.tearDown();
		
		System.clearProperty("sampleDirectoryServer.enable");
	}


	private static SampleDirectory.Configuration getConfigurationProperties()
		throws PropertyParseException {

		var props = new Properties();
		props.setProperty("sampleDirectoryServer.enable", "true");
		props.setProperty("sampleDirectoryServer.port", "11389");
		props.setProperty("sampleDirectoryServer.schema", "/WEB-INF/sampleDirectorySchema.ldif");
		props.setProperty("sampleDirectoryServer.baseDN", "dc=wonderland,dc=net");
		props.setProperty("sampleDirectoryServer.content", "/WEB-INF/sampleDirectoryContent.ldif");

                return new SampleDirectory.Configuration(props);
	}
	

	public void testParseConfiguration()
		throws Exception {

		SampleDirectory.Configuration config = getConfigurationProperties();

		assertTrue(config.enable);
		assertEquals(11389, config.port);
		assertEquals(SampleDirectory.Configuration.DEFAULT_OPERATIONS, config.operations);
		assertEquals("/WEB-INF/sampleDirectorySchema.ldif", config.schema);
		assertEquals("dc=wonderland,dc=net", config.baseDN);
		assertEquals("/WEB-INF/sampleDirectoryContent.ldif", config.content);
	}


	public void testParseConfiguration_sysPropOverride()
		throws Exception {
		
		System.setProperty("sampleDirectoryServer.enable", "false");

		SampleDirectory.Configuration config = getConfigurationProperties();

		assertFalse(config.enable);
		assertEquals(SampleDirectory.Configuration.DEFAULT_PORT, config.port);
		assertEquals(SampleDirectory.Configuration.DEFAULT_OPERATIONS, config.operations);
		assertNull(config.schema);
		assertNull(config.baseDN);
		assertNull(config.content);
	}


	private static Properties getConfigurationPropertiesWithOperations(String value) {
		Properties props = new Properties();
		props.setProperty("sampleDirectoryServer.enable", "true");
		props.setProperty("sampleDirectoryServer.port", "11389");
		props.setProperty("sampleDirectoryServer.operations", value);
		props.setProperty("sampleDirectoryServer.schema", "/WEB-INF/sampleDirectorySchema.ldif");
		props.setProperty("sampleDirectoryServer.baseDN", "dc=wonderland,dc=net");
		props.setProperty("sampleDirectoryServer.content", "/WEB-INF/sampleDirectoryContent.ldif");
		return props;
	}
	

	public void testParseConfigurationWithOperations()
		throws Exception {

		Properties props = getConfigurationPropertiesWithOperations("ABANDON ADD BIND COMPARE DELETE EXTENDED MODIFY MODIFY_DN SEARCH UNBIND");

		SampleDirectory.Configuration config = new SampleDirectory.Configuration(props);
		
		assertTrue(config.enable);
		assertEquals(11389, config.port);
		assertEquals(new HashSet<>(Arrays.asList(OperationType.values())), config.operations);
		assertEquals("/WEB-INF/sampleDirectorySchema.ldif", config.schema);
		assertEquals("dc=wonderland,dc=net", config.baseDN);
		assertEquals("/WEB-INF/sampleDirectoryContent.ldif", config.content);
	}


	public void testParseConfigurationWithOperations_commaDelimited()
		throws Exception {

		Properties props = getConfigurationPropertiesWithOperations("ABANDON, ADD, BIND, COMPARE, DELETE, EXTENDED, MODIFY, MODIFY_DN, SEARCH, UNBIND");

		SampleDirectory.Configuration config = new SampleDirectory.Configuration(props);
		
		assertTrue(config.enable);
		assertEquals(11389, config.port);
		assertEquals(new HashSet<>(Arrays.asList(OperationType.values())), config.operations);
		assertEquals("/WEB-INF/sampleDirectorySchema.ldif", config.schema);
		assertEquals("dc=wonderland,dc=net", config.baseDN);
		assertEquals("/WEB-INF/sampleDirectoryContent.ldif", config.content);
	}
	
	
	public void testLifeCycleInServletContext()
		throws Exception {
		
		var sampleDirectory = new SampleDirectory();
		
		ServletContext mockServletContext = getMockServletContext();
		
		sampleDirectory.contextInitialized(new ServletContextEvent(mockServletContext));
		
		LDAPConnection ldapCon = new LDAPConnection("localhost", 44389);
		
		assertTrue(ldapCon.isConnected());
		
		Entry ldapEntry = ldapCon.getEntry("uid=alice,ou=people,dc=wonderland,dc=net");
		assertNotNull(ldapEntry);
		
		sampleDirectory.contextDestroyed(new ServletContextEvent(mockServletContext));
		
		try {
			new LDAPConnection("localhost", 44389);
			fail();
		} catch (LDAPException e) {
			assertTrue(e.getResultCode().equals(ResultCode.CONNECT_ERROR));
		}
	}
	
	
	private static ServletContext getMockServletContext() {
		
		return new ServletContext() {
			@Override
			public String getContextPath() {
				return null;
			}
			
			
			@Override
			public ServletContext getContext(String s) {
				return null;
			}
			
			
			@Override
			public int getMajorVersion() {
				return 0;
			}
			
			
			@Override
			public int getMinorVersion() {
				return 0;
			}
			
			
			@Override
			public int getEffectiveMajorVersion() {
				return 0;
			}
			
			
			@Override
			public int getEffectiveMinorVersion() {
				return 0;
			}
			
			
			@Override
			public String getMimeType(String s) {
				return null;
			}
			
			
			@Override
			public Set<String> getResourcePaths(String s) {
				return null;
			}
			
			
			@Override
			public URL getResource(String s) {
				return null;
			}
			
			
			@Override
			public InputStream getResourceAsStream(String s) {
				
				if ("/WEB-INF/sampleDirectory.properties".equals(s)) {
					return new ByteArrayInputStream(EXAMPLE_CONFIG.getBytes(Charset.forName("UTF-8")));
				}
				
				if ("/WEB-INF/sampleDirectorySchema.ldif".equals(s)) {
					try {
						return new FileInputStream("src/test/sampleDirectorySchema.ldif");
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				}
				
				if ("/WEB-INF/sampleDirectoryContent.ldif".equals(s)) {
					try {
						return new FileInputStream("src/test/sampleDirectoryContent.ldif");
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				}
				
				return null;
			}
			
			
			@Override
			public RequestDispatcher getRequestDispatcher(String s) {
				return null;
			}
			
			
			@Override
			public RequestDispatcher getNamedDispatcher(String s) {
				return null;
			}
			
			
			@Override
			public void log(String s) {
				
			}
			
			
			@Override
			public void log(String s, Throwable throwable) {
				
			}
			
			
			@Override
			public String getRealPath(String s) {
				return null;
			}
			
			
			@Override
			public String getServerInfo() {
				return null;
			}
			
			
			@Override
			public String getInitParameter(String s) {
				if ("sampleDirectory.configurationFile".equals(s))
					return "/WEB-INF/sampleDirectory.properties";
				else
					return null;
			}
			
			
			@Override
			public Enumeration<String> getInitParameterNames() {
				return null;
			}
			
			
			@Override
			public boolean setInitParameter(String s, String s1) {
				return false;
			}
			
			
			@Override
			public Object getAttribute(String s) {
				return null;
			}
			
			
			@Override
			public Enumeration<String> getAttributeNames() {
				return null;
			}
			
			
			@Override
			public void setAttribute(String s, Object o) {
				
			}
			
			
			@Override
			public void removeAttribute(String s) {
				
			}
			
			
			@Override
			public String getServletContextName() {
				return null;
			}
			
			
			@Override
			public ServletRegistration.Dynamic addServlet(String s, String s1) {
				return null;
			}
			
			
			@Override
			public ServletRegistration.Dynamic addServlet(String s, Servlet servlet) {
				return null;
			}
			
			
			@Override
			public ServletRegistration.Dynamic addServlet(String s, Class<? extends Servlet> aClass) {
				return null;
			}
			
			
			@Override
			public <T extends Servlet> T createServlet(Class<T> aClass) throws ServletException {
				return null;
			}
			
			
			@Override
			public ServletRegistration getServletRegistration(String s) {
				return null;
			}
			
			
			@Override
			public Map<String, ? extends ServletRegistration> getServletRegistrations() {
				return null;
			}
			
			
			@Override
			public FilterRegistration.Dynamic addFilter(String s, String s1) {
				return null;
			}
			
			
			@Override
			public FilterRegistration.Dynamic addFilter(String s, Filter filter) {
				return null;
			}
			
			
			@Override
			public FilterRegistration.Dynamic addFilter(String s, Class<? extends Filter> aClass) {
				return null;
			}
			
			
			@Override
			public <T extends Filter> T createFilter(Class<T> aClass) throws ServletException {
				return null;
			}
			
			
			@Override
			public FilterRegistration getFilterRegistration(String s) {
				return null;
			}
			
			
			@Override
			public Map<String, ? extends FilterRegistration> getFilterRegistrations() {
				return null;
			}
			
			
			@Override
			public SessionCookieConfig getSessionCookieConfig() {
				return null;
			}
			
			
			@Override
			public void setSessionTrackingModes(Set<SessionTrackingMode> set) {
				
			}
			
			
			@Override
			public Set<SessionTrackingMode> getDefaultSessionTrackingModes() {
				return null;
			}
			
			
			@Override
			public Set<SessionTrackingMode> getEffectiveSessionTrackingModes() {
				return null;
			}
			
			
			@Override
			public void addListener(String s) {
				
			}
			
			
			@Override
			public <T extends EventListener> void addListener(T t) {
				
			}
			
			
			@Override
			public void addListener(Class<? extends EventListener> aClass) {
				
			}
			
			
			@Override
			public <T extends EventListener> T createListener(Class<T> aClass) {
				return null;
			}
			
			
			@Override
			public JspConfigDescriptor getJspConfigDescriptor() {
				return null;
			}
			
			
			@Override
			public ClassLoader getClassLoader() {
				return null;
			}
			
			
			@Override
			public void declareRoles(String... strings) {
				
			}
			
			
			@Override
			public ServletRegistration.Dynamic addJspFile(String s, String s1) {
				return null;
			}
			
			
			@Override
			public String getVirtualServerName() {
				return null;
			}
			
			
			@Override
			public int getSessionTimeout() {
				return 0;
			}
			
			
			@Override
			public void setSessionTimeout(int i) {
			
			}
			
			
			@Override
			public String getRequestCharacterEncoding() {
				return null;
			}
			
			
			@Override
			public void setRequestCharacterEncoding(String s) {
			
			}
			
			
			@Override
			public String getResponseCharacterEncoding() {
				return null;
			}
			
			
			@Override
			public void setResponseCharacterEncoding(String s) {
			
			}
		};
	}
}
