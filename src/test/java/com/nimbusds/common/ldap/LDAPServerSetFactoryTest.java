package com.nimbusds.common.ldap;


import com.nimbusds.common.config.ServerSelectionAlgorithm;
import com.unboundid.ldap.sdk.*;
import junit.framework.TestCase;


/**
 * Tests the LDAP server set builder.
 */
public class LDAPServerSetFactoryTest extends TestCase {


	public void testSingleSet()
		throws Exception {
	
		LDAPURL url[] = new LDAPURL[1];
		url[0] = new LDAPURL("ldap://localhost:10389");
		
		ServerSet set = LDAPServerSetFactory.create(url, null, null, null);
		
		assertTrue(set instanceof SingleServerSet);
		
		SingleServerSet singleServerSet = (SingleServerSet)set;
		
		assertEquals("localhost", singleServerSet.getAddress());
		assertEquals(10389, singleServerSet.getPort());
	}
	
	
	public void testFailoverSet()
		throws Exception {
	
		LDAPURL url[] = new LDAPURL[2];
		url[0] = new LDAPURL("ldap://localhost:10389");
		url[1] = new LDAPURL("ldap://remotehost:10389");
		
		ServerSet set = LDAPServerSetFactory.create(url, ServerSelectionAlgorithm.FAILOVER, null, null);
		
		assertTrue(set instanceof FailoverServerSet);
	}
	
	
	public void testRoundRobinSet()
		throws Exception {
	
		LDAPURL url[] = new LDAPURL[2];
		url[0] = new LDAPURL("ldap://localhost:10389");
		url[1] = new LDAPURL("ldap://remotehost:10389");
		
		ServerSet set = LDAPServerSetFactory.create(url, ServerSelectionAlgorithm.ROUND_ROBIN, null, null);
		
		assertTrue(set instanceof RoundRobinServerSet);
	}
}
