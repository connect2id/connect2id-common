package com.nimbusds.common.ldap;


import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.listener.InMemoryListenerConfig;
import com.unboundid.ldap.sdk.LDAPException;
import org.junit.After;
import org.junit.Before;


/**
 * Test with LDAP server.
 */
public abstract class TestWithLDAPServer {


	public static InMemoryDirectoryServer createLDAPServer()
		throws LDAPException {

		// Set test LDAP server port
		InMemoryListenerConfig listenerConfig = InMemoryListenerConfig.createLDAPConfig("test-ldap");

		// Set test LDAP server context, user
		InMemoryDirectoryServerConfig dirConfig = new InMemoryDirectoryServerConfig("dc=wonderland,dc=net");
		dirConfig.setListenerConfigs(listenerConfig);
		dirConfig.setEnforceAttributeSyntaxCompliance(true);

		// Start test LDAP server
		InMemoryDirectoryServer ldapServer = new InMemoryDirectoryServer(dirConfig);
		ldapServer.startListening();

		return ldapServer;
	}


	/**
	 * The test in-memory LDAP server.
	 */
	protected InMemoryDirectoryServer testLDAPServer;


	@Before
	public void setUp()
		throws Exception {

		testLDAPServer = createLDAPServer();
	}


	@After
	public void tearDown() {

		testLDAPServer.shutDown(true);
	}
}
