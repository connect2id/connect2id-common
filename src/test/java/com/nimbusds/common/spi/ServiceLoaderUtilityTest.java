package com.nimbusds.common.spi;


import junit.framework.TestCase;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


public class ServiceLoaderUtilityTest extends TestCase {
	
	
	public interface NameGenerator {
		
		String generateName();
	}
	
	
	public static class AliceNameGenerator implements NameGenerator {
		
		
		@Override
		public String generateName() {
			return "Alice";
		}
	}
	
	
	public static class BobNameGenerator implements NameGenerator {
		
		
		@Override
		public String generateName() {
			return "Bob";
		}
	}
	
	
	private static void writeSPIManifest(final Class spi, final Class ... impl)
		throws IOException {
		
		String manifestFileName = "target/classes/META-INF/services/" + spi.getName();
		
		new File("target/classes/META-INF/services").mkdirs();
		
		String spiManifest = "";
		
		for (Class i: impl) {
			 spiManifest += i.getName() + "\n";
		}
		
		Files.write(Paths.get(manifestFileName), spiManifest.getBytes(StandardCharsets.UTF_8));
	}
	
	
	private static void deleteSPIManifest(final Class spi)
		throws IOException {
		
		String manifestFileName = "target/classes/META-INF/services/" + spi.getName();
		
		Files.delete(Paths.get(manifestFileName));
	}
	
	
	public void testLoadSingle_defaultImpl() {
		
		AliceNameGenerator aliceNameGenerator = new AliceNameGenerator();
		
		NameGenerator loadedCodec = ServiceLoaderUtility.loadSingle(
			NameGenerator.class,
			aliceNameGenerator
		);
		
		assertEquals(aliceNameGenerator, loadedCodec);
		
		assertEquals("Alice", loadedCodec.generateName());
	}
	
	
	public void testLoadSingle_defaultImplNull() {
		
		NameGenerator loadedCodec = ServiceLoaderUtility.loadSingle(
			NameGenerator.class,
			null
		);
		
		assertNull(loadedCodec);
	}
	
	
	public void testLoadSingle()
		throws Exception {
		
		try {
			writeSPIManifest(NameGenerator.class, BobNameGenerator.class);
			
			NameGenerator nameGenerator = ServiceLoaderUtility.loadSingle(
				NameGenerator.class,
				new AliceNameGenerator()
			);
			
			assertTrue(nameGenerator instanceof BobNameGenerator);
			
			assertEquals("Bob", nameGenerator.generateName());
			
		} finally {
			deleteSPIManifest(NameGenerator.class);
		}
	}
	
	
	public void testLoadSingle_moreThanOne()
		throws Exception {
		
		try {
			writeSPIManifest(NameGenerator.class, AliceNameGenerator.class, BobNameGenerator.class);
			
			try {
				ServiceLoaderUtility.loadSingle(
					NameGenerator.class,
					new AliceNameGenerator()
				);
			} catch (RuntimeException e) {
				assertEquals("More than one com.nimbusds.common.spi.ServiceLoaderUtilityTest$NameGenerator SPI implementation found: [com.nimbusds.common.spi.ServiceLoaderUtilityTest$AliceNameGenerator, com.nimbusds.common.spi.ServiceLoaderUtilityTest$BobNameGenerator]", e.getMessage());
			}
			
		} finally {
			deleteSPIManifest(NameGenerator.class);
		}
	}
	
	
	public void testLoadMultiple_two()
		throws Exception {
		
		try {
			writeSPIManifest(NameGenerator.class, AliceNameGenerator.class, BobNameGenerator.class);
			
			Set<NameGenerator> nameGenerators = ServiceLoaderUtility.loadMultiple(
				NameGenerator.class,
				null
			);
			
			assertEquals(2, nameGenerators.size());
			
			List<String> names = new LinkedList<>();
			
			for (NameGenerator ng: nameGenerators) {
				names.add(ng.generateName());
			}
			
			assertTrue(names.contains("Alice"));
			assertTrue(names.contains("Bob"));
			assertEquals(2, names.size());
			
		} finally {
			deleteSPIManifest(NameGenerator.class);
		}
	}
	
	
	public void testLoadMultiple_one()
		throws Exception {
		
		try {
			writeSPIManifest(NameGenerator.class, BobNameGenerator.class);
			
			Set<NameGenerator> nameGenerators = ServiceLoaderUtility.loadMultiple(
				NameGenerator.class,
				new AliceNameGenerator()
			);
			
			assertEquals(1, nameGenerators.size());
			
			List<String> names = new LinkedList<>();
			
			for (NameGenerator ng: nameGenerators) {
				names.add(ng.generateName());
			}
			
			assertTrue(names.contains("Bob"));
			assertEquals(1, names.size());
			
		} finally {
			deleteSPIManifest(NameGenerator.class);
		}
	}
	
	
	public void testLoadMultiple_default() {
		
		Set<NameGenerator> nameGenerators = ServiceLoaderUtility.loadMultiple(
			NameGenerator.class,
			new AliceNameGenerator()
		);
		
		assertEquals(1, nameGenerators.size());
		
		List<String> names = new LinkedList<>();
		
		for (NameGenerator ng: nameGenerators) {
			names.add(ng.generateName());
		}
		
		assertTrue(names.contains("Alice"));
		assertEquals(1, names.size());
	}
	
	
	public void testLoadMultiple_defaultNull() {
		
		Set<NameGenerator> nameGenerators = ServiceLoaderUtility.loadMultiple(
			NameGenerator.class,
			null
		);
		
		assertTrue(nameGenerators.isEmpty());
	}
}
