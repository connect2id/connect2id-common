package com.nimbusds.common.servlet;


import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;


/**
 * Resource retriever test.
 */
public class ResourceRetrieverTest extends TestCase {


	public void testGetStreamForServletCtx()
		throws Exception {

		var servletCtx = new MockServletContext();

		servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value".getBytes(StandardCharsets.UTF_8)));

		InputStream is = ResourceRetriever.getStream(servletCtx, "configFile", null);

		assertEquals("app.someProperty: value", IOUtils.toString(is, StandardCharsets.UTF_8));
	}


	public void testGetOptStreamForServletCtx()
		throws Exception {

		var servletCtx = new MockServletContext();

		servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value".getBytes(StandardCharsets.UTF_8)));

		InputStream is = ResourceRetriever.getOptStream(servletCtx, "configFile", null);

		assertEquals("app.someProperty: value", IOUtils.toString(is, StandardCharsets.UTF_8));
	}


	public void testGetStringForServletCtx()
		throws Exception {

		var servletCtx = new MockServletContext();

		servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value\n".getBytes(StandardCharsets.UTF_8)));

		String s = ResourceRetriever.getString(servletCtx, "configFile", null);

		assertEquals("app.someProperty: value\n", s);
	}


	public void testGetPropertiesForServletCtx()
		throws Exception {

		var servletCtx = new MockServletContext();

		servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value\n".getBytes(StandardCharsets.UTF_8)));

		Properties props = ResourceRetriever.getProperties(servletCtx, "configFile", null);
		assertEquals("value", props.getProperty("app.someProperty"));
		assertEquals(1, props.size());
	}


	public void testGetPropertiesForServletCtx_ignoreMissingFile()
		throws Exception {

		for (boolean ignoreMissingFile: List.of(true, false)) {

			var servletCtx = new MockServletContext();

			servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

			servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value\n".getBytes(StandardCharsets.UTF_8)));

			Properties props = ResourceRetriever.getProperties(servletCtx, "configFile", ignoreMissingFile, null);
			assertEquals("value", props.getProperty("app.someProperty"));
			assertEquals(1, props.size());
		}
	}


	public void testGetPropertiesForServletCtx_ignoreMissingFile_true_noSuchResource()
		throws Exception {

		var servletCtx = new MockServletContext();

		servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		final boolean ignoreMissingFile = true;
		Properties props = ResourceRetriever.getProperties(servletCtx, "configFile", ignoreMissingFile, null);
		assertTrue(props.isEmpty());
	}


	public void testGetStreamForServletCtxNoSuchResource() {

		var servletCtx = new MockServletContext();

		servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		try {
			ResourceRetriever.getStream(servletCtx, "configFile", null);
			fail();
		} catch (Exception e) {
			assertEquals("Missing file or invalid path: /WEB-INF/app.properties", e.getMessage());
		}
	}


	public void testGetOptStreamForServletCtxNoSuchResource()
		throws Exception {

		var servletCtx = new MockServletContext();

		servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		assertNull(ResourceRetriever.getOptStream(servletCtx, "configFile", null));
	}


	public void testGetStreamForServletCtxNoSuchParam() {

		var servletCtx = new MockServletContext();

		servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value".getBytes(StandardCharsets.UTF_8)));

		try {
			ResourceRetriever.getStream(servletCtx, "configFile", null);
			fail();
		} catch (Exception e) {
			assertEquals("Missing servlet context (web.xml) init parameter: configFile", e.getMessage());
		}
	}


	public void testGetStreamForServletConfig()
		throws Exception {

		var servletConfig = new MockServletConfig();

		servletConfig.servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		servletConfig.servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value".getBytes(StandardCharsets.UTF_8)));

		InputStream is = ResourceRetriever.getStream(servletConfig, "configFile", null);

		assertEquals("app.someProperty: value", IOUtils.toString(is, StandardCharsets.UTF_8));
	}


	public void testGetPropertiesForServletConfig()
		throws Exception {

		var servletConfig = new MockServletConfig();

		servletConfig.servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		servletConfig.servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value\n".getBytes(StandardCharsets.UTF_8)));

		Properties props = ResourceRetriever.getProperties(servletConfig, "configFile", null);
		assertEquals("value", props.getProperty("app.someProperty"));
		assertEquals(1, props.size());
	}


	public void testGetStreamForServletConfigNoSuchResource() {

		var servletConfig = new MockServletConfig();

		servletConfig.servletCtx.initParams.put("configFile", "/WEB-INF/app.properties");

		try {
			ResourceRetriever.getStream(servletConfig, "configFile", null);
			fail();
		} catch (Exception e) {
			assertEquals("Missing file or invalid path: /WEB-INF/app.properties", e.getMessage());
		}
	}


	public void testGetStreamForServletConfigNoSuchParam() {

		var servletConfig = new MockServletConfig();

		servletConfig.servletCtx.resourceStreams.put("/WEB-INF/app.properties", new ByteArrayInputStream("app.someProperty: value".getBytes(StandardCharsets.UTF_8)));

		try {
			ResourceRetriever.getStream(servletConfig, "configFile", null);
			fail();
		} catch (Exception e) {
			assertEquals("Missing servlet (web.xml) init parameter: configFile", e.getMessage());
		}
	}
}
