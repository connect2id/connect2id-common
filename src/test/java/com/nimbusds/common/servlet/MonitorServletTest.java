package com.nimbusds.common.servlet;


import com.nimbusds.common.monitor.MonitorRegistries;
import com.nimbusds.common.oauth2.MasterAccessTokenValidator;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import io.dropwizard.metrics.servlets.AdminServlet;
import io.dropwizard.metrics.servlets.HealthCheckServlet;
import io.dropwizard.metrics.servlets.MetricsServlet;
import jakarta.servlet.ServletException;
import jakarta.ws.rs.WebApplicationException;
import junit.framework.TestCase;
import org.apache.commons.codec.binary.Hex;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;


public class MonitorServletTest extends TestCase {


	public void testInheritance() {

		assertTrue(new MonitorServlet() instanceof AdminServlet);
	}


	public void testAPITokenPropertyNames() {

		assertEquals("monitor.apiAccessTokenSHA256", MonitorServlet.Configuration.API_TOKEN_SHA256_PROPERTY_NAME);
		assertEquals("monitor.secondaryAPIAccessTokenSHA256", MonitorServlet.Configuration.SECONDARY_API_TOKEN_SHA256_PROPERTY_NAME);
	}


	public void testInit()
		throws Exception {

		var servlet = new MonitorServlet();
		var servletConfig = new MockServletConfig();

		servletConfig.servletCtx.setAttribute(MetricsServlet.METRICS_REGISTRY, MonitorRegistries.getMetricRegistry());
		servletConfig.servletCtx.setAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY, MonitorRegistries.getHealthCheckRegistry());

		String configFile = "/WEB-INF/monitor.properties";
		servletConfig.servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		var token = new BearerAccessToken(32);
		String tokenHash = Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(token, null));
		String propertyString = "monitor.apiAccessTokenSHA256: " + tokenHash + "\n";

		servletConfig.servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		servlet.init(servletConfig);

		assertFalse(servlet.tokenValidator.accessIsDisabled());
		
		servlet.tokenValidator.validateBearerAccessToken(token.toAuthorizationHeader());
		
		try {
			servlet.tokenValidator.validateBearerAccessToken(new BearerAccessToken().toAuthorizationHeader());
			fail();
		} catch (WebApplicationException e) {
			assertEquals(401, e.getResponse().getStatus());
		}
	}


	public void testInitWithSecondaryToken()
		throws Exception {

		var servlet = new MonitorServlet();
		var servletConfig = new MockServletConfig();

		servletConfig.servletCtx.setAttribute(MetricsServlet.METRICS_REGISTRY, MonitorRegistries.getMetricRegistry());
		servletConfig.servletCtx.setAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY, MonitorRegistries.getHealthCheckRegistry());

		String configFile = "/WEB-INF/monitor.properties";
		servletConfig.servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		BearerAccessToken t1 = new BearerAccessToken(32);
		BearerAccessToken t2 = new BearerAccessToken(32);
		
		String t1Hash = Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(t1, null));
		String t2Hash = Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(t2, null));
		
		String propertyString =
			"monitor.apiAccessTokenSHA256: " + t1Hash + "\n" +
			"monitor.secondaryAPIAccessTokenSHA256: " + t2Hash + "\n";

		servletConfig.servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		servlet.init(servletConfig);

		assertFalse(servlet.tokenValidator.accessIsDisabled());
		
		servlet.tokenValidator.validateBearerAccessToken(t1.toAuthorizationHeader());
		servlet.tokenValidator.validateBearerAccessToken(t2.toAuthorizationHeader());
		
		try {
			servlet.tokenValidator.validateBearerAccessToken(new BearerAccessToken().toAuthorizationHeader());
			fail();
		} catch (WebApplicationException e) {
			assertEquals(401, e.getResponse().getStatus());
		}
	}


	public void testInitWithSystemProperty()
		throws Exception {

		var servlet = new MonitorServlet();
		var servletConfig = new MockServletConfig();

		servletConfig.servletCtx.setAttribute(MetricsServlet.METRICS_REGISTRY, MonitorRegistries.getMetricRegistry());
		servletConfig.servletCtx.setAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY, MonitorRegistries.getHealthCheckRegistry());

		var configFile = "/WEB-INF/monitor.properties";
		servletConfig.servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		var propertyString = "monitor.apiAccessTokenSHA256: \n"; // not set

		servletConfig.servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		var token = new BearerAccessToken(32);
		
		String tokenHash = Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(token, null));

		System.getProperties().setProperty("monitor.apiAccessTokenSHA256", tokenHash); // override

		servlet.init(servletConfig);

		System.getProperties().remove("monitor.apiAccessToken");
		
		assertFalse(servlet.tokenValidator.accessIsDisabled());
		
		servlet.tokenValidator.validateBearerAccessToken(token.toAuthorizationHeader());
	}


	public void testInitWithSecondaryTokenViaSystemProperty()
		throws Exception {

		var servlet = new MonitorServlet();
		var servletConfig = new MockServletConfig();

		servletConfig.servletCtx.setAttribute(MetricsServlet.METRICS_REGISTRY, MonitorRegistries.getMetricRegistry());
		servletConfig.servletCtx.setAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY, MonitorRegistries.getHealthCheckRegistry());

		var configFile = "/WEB-INF/monitor.properties";
		servletConfig.servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		var propertyString = "monitor.apiAccessTokenSHA256: \n";

		servletConfig.servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		BearerAccessToken t1 = new BearerAccessToken(32);
		BearerAccessToken t2 = new BearerAccessToken(32);
		
		String t1Hash = Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(t1, null));
		String t2Hash = Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(t2, null));

		System.getProperties().setProperty("monitor.apiAccessTokenSHA256", t1Hash); // override
		System.getProperties().setProperty("monitor.secondaryAPIAccessTokenSHA256", t2Hash); // override

		servlet.init(servletConfig);

		System.getProperties().remove("monitor.apiAccessTokenSHA256");
		System.getProperties().remove("monitor.secondaryAPIAccessTokenSHA256");
		
		assertFalse(servlet.tokenValidator.accessIsDisabled());
		
		servlet.tokenValidator.validateBearerAccessToken(t1.toAuthorizationHeader());
		servlet.tokenValidator.validateBearerAccessToken(t2.toAuthorizationHeader());
		
		try {
			servlet.tokenValidator.validateBearerAccessToken(new BearerAccessToken().toAuthorizationHeader());
			fail();
		} catch (WebApplicationException e) {
			assertEquals(401, e.getResponse().getStatus());
		}
	}


	public void testInitCtxParamMissing() {

		var servlet = new MonitorServlet();
		var servletConfig = new MockServletConfig();

		servletConfig.servletCtx.setAttribute(MetricsServlet.METRICS_REGISTRY, MonitorRegistries.getMetricRegistry());
		servletConfig.servletCtx.setAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY, MonitorRegistries.getHealthCheckRegistry());

		var configFile = "/WEB-INF/monitor.properties";

		var token = new BearerAccessToken("01234567890123456789012345678901"); // 32 chars
		var propertyString = "monitor.apiAccessTokenSHA256: " + token.getValue() + "\n";

		servletConfig.servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		try {
			servlet.init(servletConfig);
			fail();
		} catch (Exception e) {
			assertEquals("Missing servlet context (web.xml) init parameter: monitor.configurationFile", e.getMessage());
		}
	}


	public void testInitPropertiesFileMissing()
		throws Exception {

		var servlet = new MonitorServlet();
		var servletConfig = new MockServletConfig();

		servletConfig.servletCtx.setAttribute(MetricsServlet.METRICS_REGISTRY, MonitorRegistries.getMetricRegistry());
		servletConfig.servletCtx.setAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY, MonitorRegistries.getHealthCheckRegistry());

		var configFile = "/WEB-INF/monitor.properties";
		servletConfig.servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);
		servlet.init(servletConfig);

		assertTrue(servlet.tokenValidator.accessIsDisabled());
	}
}
