package com.nimbusds.common.servlet;


import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;


public class MockServletConfig implements ServletConfig {


	public final Map<String,String> initParams = new HashMap<>();


	public String serlvetName = "MockServlet";


	public MockServletContext servletCtx = new MockServletContext();


	@Override
	public String getServletName() {

		return serlvetName;
	}


	@Override
	public ServletContext getServletContext() {

		return servletCtx;
	}


	@Override
	public String getInitParameter(String s) {

		return servletCtx.getInitParameter(s);
	}


	@Override
	public Enumeration<String> getInitParameterNames() {

		return servletCtx.getInitParameterNames();
	}
}
