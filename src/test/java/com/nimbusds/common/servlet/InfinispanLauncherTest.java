package com.nimbusds.common.servlet;


import jakarta.servlet.ServletContextEvent;
import junit.framework.TestCase;
import org.infinispan.manager.EmbeddedCacheManager;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;


public class InfinispanLauncherTest extends TestCase {


	public void testConstants() {

		assertEquals("infinispan.configurationFile", InfinispanLauncher.INFINISPAN_CONFIG_FILE_SETTING);
		assertEquals("org.infinispan.manager.EmbeddedCacheManager", InfinispanLauncher.INFINISPAN_CTX_ATTRIBUTE_NAME);
	}


	// TODO
	public void _testInitBlankConfig()
		throws Exception {

		MockServletContext servletContext = new MockServletContext();

		servletContext.initParams.put(InfinispanLauncher.INFINISPAN_CONFIG_FILE_SETTING, "/WEB-INF/infinispan.xml");

		String infinispanXmlConfig = "<infinispan/>";

		servletContext.resourceStreams.put("/WEB-INF/infinispan.xml", new ByteArrayInputStream(infinispanXmlConfig.getBytes(StandardCharsets.UTF_8)));

		InfinispanLauncher launcher = new InfinispanLauncher();

		launcher.contextInitialized(new ServletContextEvent(servletContext));

		assertTrue(servletContext.getAttribute(InfinispanLauncher.INFINISPAN_CTX_ATTRIBUTE_NAME) instanceof EmbeddedCacheManager);

		EmbeddedCacheManager cacheManager = (EmbeddedCacheManager)servletContext.getAttribute(InfinispanLauncher.INFINISPAN_CTX_ATTRIBUTE_NAME);

		assertFalse(cacheManager.getCacheManagerConfiguration().isClustered());

		cacheManager.getCache("test");

		launcher.contextDestroyed(new ServletContextEvent(servletContext));
	}


	public void testInitClusteredFromJavaSystemProperty() {

		MockServletContext servletContext = new MockServletContext();
		
		System.setProperty(InfinispanLauncher.INFINISPAN_CONFIG_FILE_SETTING, "/WEB-INF/infinispan.xml");

		String infinispanXmlConfig = "\n" +
			"<infinispan xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
			"            xsi:schemaLocation=\"urn:infinispan:config:14.0 http://www.infinispan.org/schemas/infinispan-config-14.0.xsd\"\n" +
			"            xmlns=\"urn:infinispan:config:14.0\">\n" +
			"\n" +
			"    <!-- For more information see http://connect2id.com/products/server/docs/config/infinispan -->\n" +
			"\n" +
			"    <jgroups>\n" +
			"        <stack-file name=\"jgroups-config\" path=\"default-configs/default-jgroups-udp.xml\"/>\n" +
			"    </jgroups>\n" +
			"\n" +
			"    <cache-container name=\"c2id.cluster\" default-cache=\"sessionStore.sessionMap\" statistics=\"true\">\n" +
			"        <transport stack=\"jgroups-config\"/>\n" +
			"\n" +
			"        <!-- Session store objects -->\n" +
			"        <replicated-cache name=\"sessionStore.sessionMap\">\n" +
			"            <expiration interval=\"-1\"/>\n" +
			"        </replicated-cache>\n" +
			"        <replicated-cache name=\"sessionStore.subjectMap\">\n" +
			"            <expiration interval=\"-1\"/>\n" +
			"        </replicated-cache>\n" +
			"    </cache-container>\n" +
			"\n" +
			"</infinispan>";

		servletContext.resourceStreams.put("/WEB-INF/infinispan.xml", new ByteArrayInputStream(infinispanXmlConfig.getBytes(StandardCharsets.UTF_8)));

		InfinispanLauncher launcher = new InfinispanLauncher();

		launcher.contextInitialized(new ServletContextEvent(servletContext));

		assertTrue(servletContext.getAttribute(InfinispanLauncher.INFINISPAN_CTX_ATTRIBUTE_NAME) instanceof EmbeddedCacheManager);

		EmbeddedCacheManager cacheManager = (EmbeddedCacheManager)servletContext.getAttribute(InfinispanLauncher.INFINISPAN_CTX_ATTRIBUTE_NAME);

		assertTrue(cacheManager.getCacheManagerConfiguration().isClustered());

		cacheManager.getCache("sessionStore.sessionMap");

		launcher.contextDestroyed(new ServletContextEvent(servletContext));
		
		System.clearProperty(InfinispanLauncher.INFINISPAN_CONFIG_FILE_SETTING);
	}


	public void testInitClusteredFromServletContext() {

		MockServletContext servletContext = new MockServletContext();

		servletContext.initParams.put(InfinispanLauncher.INFINISPAN_CONFIG_FILE_SETTING, "/WEB-INF/infinispan.xml");

		String infinispanXmlConfig = "\n" +
			"<infinispan xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
			"            xsi:schemaLocation=\"urn:infinispan:config:14.0 http://www.infinispan.org/schemas/infinispan-config-14.0.xsd\"\n" +
			"            xmlns=\"urn:infinispan:config:14.0\">\n" +
			"\n" +
			"    <!-- For more information see http://connect2id.com/products/server/docs/config/infinispan -->\n" +
			"\n" +
			"    <jgroups>\n" +
			"        <stack-file name=\"jgroups-config\" path=\"default-configs/default-jgroups-udp.xml\"/>\n" +
			"    </jgroups>\n" +
			"\n" +
			"    <cache-container name=\"c2id.cluster\" default-cache=\"sessionStore.sessionMap\" statistics=\"true\">\n" +
			"        <transport stack=\"jgroups-config\"/>\n" +
			"\n" +
			"        <!-- Session store objects -->\n" +
			"        <replicated-cache name=\"sessionStore.sessionMap\">\n" +
			"            <expiration interval=\"-1\"/>\n" +
			"        </replicated-cache>\n" +
			"        <replicated-cache name=\"sessionStore.subjectMap\">\n" +
			"            <expiration interval=\"-1\"/>\n" +
			"        </replicated-cache>\n" +
			"    </cache-container>\n" +
			"\n" +
			"</infinispan>";

		servletContext.resourceStreams.put("/WEB-INF/infinispan.xml", new ByteArrayInputStream(infinispanXmlConfig.getBytes(StandardCharsets.UTF_8)));

		InfinispanLauncher launcher = new InfinispanLauncher();

		launcher.contextInitialized(new ServletContextEvent(servletContext));

		assertTrue(servletContext.getAttribute(InfinispanLauncher.INFINISPAN_CTX_ATTRIBUTE_NAME) instanceof EmbeddedCacheManager);

		EmbeddedCacheManager cacheManager = (EmbeddedCacheManager)servletContext.getAttribute(InfinispanLauncher.INFINISPAN_CTX_ATTRIBUTE_NAME);

		assertTrue(cacheManager.getCacheManagerConfiguration().isClustered());

		cacheManager.getCache("sessionStore.sessionMap");

		launcher.contextDestroyed(new ServletContextEvent(servletContext));
	}
}
