package com.nimbusds.common.servlet;


import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.nimbusds.common.monitor.MonitorRegistries;
import io.dropwizard.metrics.servlets.HealthCheckServlet;
import io.dropwizard.metrics.servlets.MetricsServlet;
import jakarta.servlet.ServletContextEvent;
import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;


public class MonitorLauncherTest extends TestCase {


	public void testContextInitialized() {

		var launcher = new MonitorLauncher();

		var servletCtx = new MockServletContext();

		var configFile = "/WEB-INF/monitor.properties";
		servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		var propertyString =
			"monitor.enableJMX: false\n" +
			"monitor.graphite.enable: false\n";

		servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		launcher.contextInitialized(new ServletContextEvent(servletCtx));

		assertTrue(servletCtx.getAttribute(MetricsServlet.METRICS_REGISTRY)
			instanceof MetricRegistry);

		assertTrue(servletCtx.getAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY)
			instanceof HealthCheckRegistry);

		assertNull(launcher.jmxReporter);

		launcher.contextDestroyed(new ServletContextEvent(servletCtx));
	}


	public void testContextInitialized_missingPropertiesFile() {

		var launcher = new MonitorLauncher();

		var servletCtx = new MockServletContext();

		var configFile = "/WEB-INF/monitor.properties";
		servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		launcher.contextInitialized(new ServletContextEvent(servletCtx));

		assertTrue(servletCtx.getAttribute(MetricsServlet.METRICS_REGISTRY)
			instanceof MetricRegistry);

		assertTrue(servletCtx.getAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY)
			instanceof HealthCheckRegistry);

		assertNull(launcher.jmxReporter);

		launcher.contextDestroyed(new ServletContextEvent(servletCtx));
	}


	public void testContextInitializedWithJMXReporter() {

		var launcher = new MonitorLauncher();

		var servletCtx = new MockServletContext();

		var configFile = "/WEB-INF/monitor.properties";
		servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		String propertyString =
			"monitor.enableJMX: true\n" +
				"monitor.graphite.enable: false\n";

		servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		launcher.contextInitialized(new ServletContextEvent(servletCtx));

		assertTrue(servletCtx.getAttribute(MetricsServlet.METRICS_REGISTRY)
			instanceof MetricRegistry);

		assertTrue(servletCtx.getAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY)
			instanceof HealthCheckRegistry);

		assertNotNull(launcher.jmxReporter);
		assertNull(launcher.graphiteReporter);

		launcher.contextDestroyed(new ServletContextEvent(servletCtx));
	}


	public void testContextInitializedWithGraphiteReporter() {

		var launcher = new MonitorLauncher();

		var servletCtx = new MockServletContext();

		var configFile = "/WEB-INF/monitor.properties";
		servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		var propertyString =
			"monitor.enableJMX: false\n" +
			"monitor.graphite.enable: true\n" +
			"monitor.graphite.enable: true\n" +
			"monitor.graphite.host: localhost\n"  +
			"monitor.graphite.port: 2003\n"  +
			"monitor.graphite.reportInterval: 60\n"  +
			"monitor.graphite.batchSize: 100\n"  +
			"monitor.graphite.prefix: c2id-server-1.\n"  +
			"monitor.graphite.ratesTimeUnit: SECONDS\n"  +
			"monitor.graphite.durationsTimeUnit: MILLISECONDS\n"  +
			"monitor.graphite.filter.1: authzStore.ldapConnector.*\n"  +
			"monitor.graphite.filter.2: tokenEndpoint.code.*\n"  +
			"monitor.graphite.filter.3: tokenEndpoint.refreshToken.*\n";

		servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		launcher.contextInitialized(new ServletContextEvent(servletCtx));

		assertTrue(servletCtx.getAttribute(MetricsServlet.METRICS_REGISTRY)
			instanceof MetricRegistry);

		assertTrue(servletCtx.getAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY)
			instanceof HealthCheckRegistry);

		assertNull(launcher.jmxReporter);
		assertNotNull(launcher.graphiteReporter);

		launcher.contextDestroyed(new ServletContextEvent(servletCtx));
	}


	public void testSystemPropertyOverride() {

		var launcher = new MonitorLauncher();

		var servletCtx = new MockServletContext();

		var configFile = "/WEB-INF/monitor.properties";
		servletCtx.initParams.put(MonitorLauncher.CONFIG_CTX_PARAMETER_NAME, configFile);

		var propertyString =
			"monitor.entryCountCacheTimeout: -1\n" +
			"monitor.enableJMX: false\n" +
			"monitor.graphite.enable: true\n" +
			"monitor.graphite.host: localhost\n"  +
			"monitor.graphite.port: 2003\n"  +
			"monitor.graphite.reportInterval: 60\n"  +
			"monitor.graphite.batchSize: 100\n"  +
			"monitor.graphite.prefix: c2id-server-1.\n"  +
			"monitor.graphite.ratesTimeUnit: SECONDS\n"  +
			"monitor.graphite.durationsTimeUnit: MILLISECONDS\n"  +
			"monitor.graphite.filter.1: authzStore.ldapConnector.*\n"  +
			"monitor.graphite.filter.2: tokenEndpoint.code.*\n"  +
			"monitor.graphite.filter.3: tokenEndpoint.refreshToken.*\n";

		servletCtx.resourceStreams.put(configFile, new ByteArrayInputStream(propertyString.getBytes(StandardCharsets.UTF_8)));

		System.setProperty("monitor.entryCountCacheTimeout", "0");
		System.setProperty("monitor.enableJMX", "true");
		System.setProperty("monitor.graphite.enable", "false");

		launcher.contextInitialized(new ServletContextEvent(servletCtx));

		assertTrue(servletCtx.getAttribute(MetricsServlet.METRICS_REGISTRY)
			instanceof MetricRegistry);

		assertTrue(servletCtx.getAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY)
			instanceof HealthCheckRegistry);
		
		assertEquals(0, MonitorRegistries.getEntryCountCacheTimeout());
		assertNotNull(launcher.jmxReporter);
		assertNull(launcher.graphiteReporter);
		

		launcher.contextDestroyed(new ServletContextEvent(servletCtx));

		// clean up
		System.getProperties().remove("monitor.enableJMX");
		System.getProperties().remove("monitor.graphite.enable");
	}
}
