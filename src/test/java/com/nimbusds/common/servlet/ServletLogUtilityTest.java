package com.nimbusds.common.servlet;


import junit.framework.TestCase;


public class ServletLogUtilityTest extends TestCase {


	public void testNullArguments() {

		ServletLogUtility.log(null);
		ServletLogUtility.log(null, null);
	}
	
	
	public void testXForwardedFor() {
		
		var request = new MockHttpServletRequest();
		request.method = "POST";
		request.secure = true;
		request.headers.put("X-Forwarded-For", "203.0.113.195, 70.41.3.18, 150.172.238.178");
		request.requestURI = "/token";
		
		assertEquals("HTTPS POST request: ip=203.0.113.195 path=/token", ServletLogUtility.composeLogMessage(request));
	}
	
	
	public void testNoReverseProxy() {
		
		var request = new MockHttpServletRequest();
		request.method = "POST";
		request.secure = true;
		request.remoteAddr = "203.0.113.195";
		request.requestURI = "/token";
		
		assertEquals("HTTPS POST request: ip=203.0.113.195 path=/token", ServletLogUtility.composeLogMessage(request));
	}
}
