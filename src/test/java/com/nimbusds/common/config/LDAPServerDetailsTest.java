package com.nimbusds.common.config;


import com.nimbusds.common.ldap.LDAPConnectionSecurity;
import com.thetransactioncompany.util.PropertyParseException;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.LDAPURL;
import junit.framework.TestCase;

import java.util.Properties;


/**
 * Tests the LDAP server details configuration.
 */
public class LDAPServerDetailsTest extends TestCase {


	public void testConfigDefaults() {
		
		Properties props = new Properties();

		props.setProperty("abc.ldapServer.url", "ldap://localhost:1389");
		
		LDAPServerDetails config = null;

		try {
			config = new LDAPServerDetails("abc.ldapServer.", props);

		} catch (PropertyParseException e) {

			fail(e.getMessage() + ": " + e.getPropertyKey());
		}

		assertEquals(1, config.url.length);
		assertEquals("ldap://localhost:1389", config.url[0].toString());

		// defaults
		assertNull(config.selectionAlgorithm);
		assertEquals(LDAPConnectionSecurity.STARTTLS, config.security);
		assertEquals(0, config.connectTimeout);
		assertEquals(0, config.responseTimeout);
		assertFalse(config.trustSelfSignedCerts);

		// test log
		config.log();
	}


	public void testConfigDefaultsMultipleURLs() {
		
		Properties props = new Properties();

		props.setProperty("abc.ldapServer.url", "ldap://localhost:1389 ldap://remotehost:1389");
		
		LDAPServerDetails config = null;

		try {
			config = new LDAPServerDetails("abc.ldapServer.", props);

		} catch (PropertyParseException e) {

			fail(e.getMessage() + ": " + e.getPropertyKey());
		}

		assertEquals(2, config.url.length);
		assertEquals("ldap://localhost:1389", config.url[0].toString());

		// defaults
		assertEquals(ServerSelectionAlgorithm.FAILOVER, config.selectionAlgorithm);
		assertEquals(LDAPConnectionSecurity.STARTTLS, config.security);
		assertEquals(0, config.connectTimeout);
		assertFalse(config.trustSelfSignedCerts);

		// test log
		config.log();
	}


	public void testConfigDefaultsNoURL() {
		
		Properties props = new Properties();

		LDAPServerDetails config = null;

		boolean requireURL = false;

		try {
			config = new LDAPServerDetails("abc.ldapServer.", props, requireURL);

		} catch (PropertyParseException e) {

			fail(e.getMessage() + ": " + e.getPropertyKey());
		}

		// defaults
		assertNull(config.url);
		assertNull(config.selectionAlgorithm);
		assertEquals(LDAPConnectionSecurity.STARTTLS, config.security);
		assertEquals(0, config.connectTimeout);
		assertFalse(config.trustSelfSignedCerts);

		// test log
		config.log();
	}


	public void testMissingURLException() {

		Properties props = new Properties();

		try {
			new LDAPServerDetails("abc.ldapServer.", props);

			fail("Failed to raise missing LDAP URL exception [1]");

		} catch (PropertyParseException e) {

			// ok
		}

		boolean requireURL = true;

		try {
			new LDAPServerDetails("abc.ldapServer.", props, requireURL);

			fail("Failed to raise missing LDAP URL exception [2]");

		} catch (PropertyParseException e) {

			// ok
		}
	}


	public void testConfigWithOptionalParams() {
		
		Properties props = new Properties();

		props.setProperty("abc.ldapServer.url", "ldap://localhost:1389 ldap://remotehost:1389");
		props.setProperty("abc.ldapServer.selectionAlgorithm", "ROUND_ROBIN");
		props.setProperty("abc.ldapServer.security", "NONE");
		props.setProperty("abc.ldapServer.connectTimeout", "250");
		props.setProperty("abc.ldapServer.trustSelfSignedCerts", "true");
		
		LDAPServerDetails config = null;

		try {
			config = new LDAPServerDetails("abc.ldapServer.", props);

		} catch (PropertyParseException e) {

			fail(e.getMessage() + ": " + e.getPropertyKey());
		}

		assertEquals(2, config.url.length);
		assertEquals("ldap://localhost:1389", config.url[0].toString());
		assertEquals("ldap://remotehost:1389", config.url[1].toString());

		// defaults
		assertEquals(ServerSelectionAlgorithm.ROUND_ROBIN, config.selectionAlgorithm);
		assertEquals(LDAPConnectionSecurity.NONE, config.security);
		assertEquals(250, config.connectTimeout);
		assertTrue(config.trustSelfSignedCerts);

		// test log
		config.log();
	}


	public void testAltConstructorSingleURL()
		throws LDAPException {

		LDAPURL url = new LDAPURL("ldap://localhost:1389");
		LDAPServerDetails config = new LDAPServerDetails(url,
			LDAPConnectionSecurity.STARTTLS,
			500,
			250,
			true);
		
		assertEquals("ldap://localhost:1389", config.url[0].toString());
		assertNull(config.selectionAlgorithm);
		assertEquals(LDAPConnectionSecurity.STARTTLS, config.security);
		assertEquals(500, config.connectTimeout);
		assertEquals(250, config.responseTimeout);
		assertTrue(config.trustSelfSignedCerts);
	}


	public void testAltConstructorURLArray()
		throws LDAPException {

		LDAPURL url1 = new LDAPURL("ldap://localhost:1389");
		LDAPURL url2 = new LDAPURL("ldap://remotehost:1389");

		LDAPURL[] urlArray = new LDAPURL[2];
		urlArray[0] = url1;
		urlArray[1] = url2;

		LDAPServerDetails config = new LDAPServerDetails(urlArray,
			ServerSelectionAlgorithm.ROUND_ROBIN,
			LDAPConnectionSecurity.STARTTLS,
			1000,
			500,
			true);

		assertEquals(2, config.url.length);
		assertEquals(url1, config.url[0]);
		assertEquals(url2, config.url[1]);
		assertEquals(ServerSelectionAlgorithm.ROUND_ROBIN, config.selectionAlgorithm);
		assertEquals(LDAPConnectionSecurity.STARTTLS, config.security);
		assertEquals(1000, config.connectTimeout);
		assertEquals(500, config.responseTimeout);
		assertTrue(config.trustSelfSignedCerts);
	}
}
