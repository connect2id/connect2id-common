package com.nimbusds.common.config;


import junit.framework.TestCase;

import java.util.Arrays;
import java.util.Collections;


public class StringListParserTest extends TestCase {
	
	
	public void testParse_null() {
		
		assertTrue(StringListParser.parse(null).isEmpty());
	}
	
	
	public void testParse_empty() {
		
		assertTrue(StringListParser.parse("").isEmpty());
	}
	
	
	public void testParse_blank() {
		
		assertTrue(StringListParser.parse(" ").isEmpty());
	}
	
	
	public void testParse_oneItem() {
		
		assertEquals(Collections.singletonList("one"), StringListParser.parse("one"));
	}
	
	
	public void testParse_twoItems_spaceSep() {
		
		assertEquals(Arrays.asList("one", "two"), StringListParser.parse("one two"));
	}
	
	
	public void testParse_twoItems_commaSep() {
		
		assertEquals(Arrays.asList("one", "two"), StringListParser.parse("one,two"));
	}
	
	
	public void testParse_twoItems_commaAndSpaceSep() {
		
		assertEquals(Arrays.asList("one", "two"), StringListParser.parse("one, two"));
	}
	
	
	public void testParse_twoItems_commaAndSpaceSep_trailingSpace() {
		
		assertEquals(Arrays.asList("one", "two"), StringListParser.parse("one, two "));
	}
	
	
	public void testParse_twoItems_commaAndSpaceSep_trailingComma() {
		
		assertEquals(Arrays.asList("one", "two"), StringListParser.parse("one, two,"));
	}
}
