package com.nimbusds.common.config;


import junit.framework.TestCase;


/**
 * Tests the configuration exception.
 */
public class ConfigurationExceptionTest extends TestCase {


	public void testUnchecked() {

		ConfigurationException e = new ConfigurationException("message");
		assertTrue(e instanceof RuntimeException);
	}


	public void testMessage() {

		ConfigurationException e = new ConfigurationException("message");
		assertEquals("message", e.getMessage());
	}


	public void testCause() {

		Throwable cause = new Throwable("cause");
		ConfigurationException e = new ConfigurationException("message", cause);
		assertEquals(cause, e.getCause());
	}
}
