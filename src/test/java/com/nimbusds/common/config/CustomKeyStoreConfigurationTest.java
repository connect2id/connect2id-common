package com.nimbusds.common.config;


import com.thetransactioncompany.util.PropertyParseException;
import junit.framework.TestCase;

import java.util.Properties;


/**
 * Tests the custom key store configuration.
 */
public class CustomKeyStoreConfigurationTest extends TestCase {
	
	
	public static Properties getProperties() {
	
		Properties props = new Properties();
		
		props.setProperty("json2ldap.customKeyStore.enable", "true");
		props.setProperty("json2ldap.customKeyStore.file", "/home/json2ldap/keystore.jks");
		props.setProperty("json2ldap.customKeyStore.type", "JKS");
		props.setProperty("json2ldap.customKeyStore.password", "secret");
		
		return props;
	}
	
	
	public void testConfiguration()
		throws Exception {
	
		Properties props = getProperties();
	
		CustomKeyStoreConfiguration customKeyStore = null;
		
		String prefix = "json2ldap.customKeyStore.";
		
		try {
			customKeyStore = new CustomKeyStoreConfiguration(prefix, props);
		
		} catch (PropertyParseException e) {
			fail(e.getMessage());
		}
		
		assertTrue(customKeyStore.enable);
		assertEquals("/home/json2ldap/keystore.jks", customKeyStore.file);
		assertEquals("JKS", customKeyStore.type);
		assertEquals("secret", customKeyStore.password);
	}
}



