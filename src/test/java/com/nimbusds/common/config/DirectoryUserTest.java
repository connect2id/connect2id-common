package com.nimbusds.common.config;


import com.thetransactioncompany.util.PropertyParseException;
import com.unboundid.ldap.sdk.DN;
import junit.framework.TestCase;

import java.util.Properties;


/**
 * Tests the directory user configuration.
 */
public class DirectoryUserTest extends TestCase {


	public void testConfigDefinedUser()
		throws Exception {
		
		Properties props = new Properties();
		props.setProperty("searchUser.dn", "cn=alice,ou=people,dc=example,dc=com");
		props.setProperty("searchUser.password", "secret");
		
		DirectoryUser config = null;

		try {
			config = new DirectoryUser("searchUser.", props);

		} catch (PropertyParseException e) {

			fail(e.getMessage() + ": " + e.getPropertyKey());
		}

		assertTrue(new DN("cn=alice,ou=people,dc=example,dc=com").equals(config.dn));
		assertEquals("secret", config.password);

		// test log
		config.log();
	}
	
	
	public void testConfigAnonymousUser()
		throws Exception {
		
		Properties props = new Properties();
		props.setProperty("searchUser.dn", "");
		props.setProperty("searchUser.password", "");
		
		DirectoryUser config = null;

		try {
			config = new DirectoryUser("searchUser.", props);

		} catch (PropertyParseException e) {

			fail(e.getMessage() + ": " + e.getPropertyKey());
		}

		assertTrue(DN.NULL_DN.equals(config.dn));
		assertEquals("", config.password);

		// test log
		config.log();
	}


	public void testConfigDirect()
		throws Exception {

		DN dn = new DN("cn=alice,ou=people,dc=wonderland,dc=net");
		String password = "secret";

		DirectoryUser config = new DirectoryUser(dn, password);

		assertEquals(dn, config.dn);
		assertEquals(password, config.password);

		// test log
		config.log();
	}
}
