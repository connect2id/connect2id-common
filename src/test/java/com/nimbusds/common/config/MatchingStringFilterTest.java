package com.nimbusds.common.config;

import com.thetransactioncompany.util.PropertyParseException;
import junit.framework.TestCase;

import java.util.List;
import java.util.Properties;


public class MatchingStringFilterTest extends TestCase {


        public void testConstant() {

                assertEquals("*", MatchingStringFilter.WILDCARD);
        }


        public void testPropertiesConstructor_defaultMatchNone() throws PropertyParseException {

                var props = new Properties();

                var filter = new MatchingStringFilter("op.claimNames", props);

                List<String> strings = List.of("a", "b", "c");

                assertTrue(filter.filter(strings).isEmpty());

                assertEquals("[]", filter.toString());
        }


        public void testPropertiesConstructor_defaultFilter_matchAll() throws PropertyParseException {

                var props = new Properties();

                var filter = new MatchingStringFilter("op.claimNames", props, new MatchingStringFilter(true));

                List<String> strings = List.of("a", "b", "c");

                assertEquals(strings, filter.filter(strings));

                assertEquals("* (all)", filter.toString());
        }


        public void testPropertiesConstructor_defaultFilter_matchSpecific() throws PropertyParseException {

                var props = new Properties();
                props.setProperty("op.claimNames", "b, c, e");
                var defaultFilter = new MatchingStringFilter("op.claimNames", props);

                props = new Properties();

                var filter = new MatchingStringFilter("op.claimNames", props, defaultFilter);

                List<String> strings = List.of("a", "b", "c");

                assertEquals(List.of("b", "c"), filter.filter(strings));

                assertEquals("[b, c, e]", filter.toString());
        }


        public void testPropertiesConstructor_wildcard() throws PropertyParseException {

                for (String propValue: List.of("*", " *", "* ", " * ")) {

                        var props = new Properties();
                        props.setProperty("op.claimNames", propValue);

                        var filter = new MatchingStringFilter("op.claimNames", props);

                        List<String> strings = List.of("a", "b", "c");

                        assertEquals(strings, filter.filter(strings));

                        assertEquals("* (all)", filter.toString());
                }
        }


        public void testAllConstructor_false() {

                var filter = new MatchingStringFilter(false);
                List<String> strings = List.of("a", "b", "c");
                assertTrue(filter.filter(strings).isEmpty());
                assertEquals("[]", filter.toString());
        }


        public void testAllConstructor_true() {

                var filter = new MatchingStringFilter(true);
                List<String> strings = List.of("a", "b", "c");
                assertEquals(strings, filter.filter(strings));
                assertEquals("* (all)", filter.toString());
        }


        public void testSpecificMatches() throws PropertyParseException {

                var props = new Properties();
                props.setProperty("op.claimNames", "b, c, e");
                var filter = new MatchingStringFilter("op.claimNames", props);
                assertEquals(List.of("b", "c"), filter.filter(List.of("a", "b", "c", "d")));
                assertEquals("[b, c, e]", filter.toString());
        }


        public void testNullInput() throws PropertyParseException {

                var props = new Properties();
                props.setProperty("op.claimNames", "b, c, e");
                var filter = new MatchingStringFilter("op.claimNames", props);
                assertTrue(filter.filter(null).isEmpty());
                assertEquals("[b, c, e]", filter.toString());
        }


        public void testEmptyListInput() throws PropertyParseException {

                var props = new Properties();
                props.setProperty("op.claimNames", "b, c, e");
                var filter = new MatchingStringFilter("op.claimNames", props);
                assertTrue(filter.filter(List.of()).isEmpty());
                assertEquals("[b, c, e]", filter.toString());
        }
}
