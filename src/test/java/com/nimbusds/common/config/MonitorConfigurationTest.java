package com.nimbusds.common.config;


import junit.framework.TestCase;
import org.junit.Before;

import java.util.Properties;
import java.util.concurrent.TimeUnit;


/**
 * Tests the monitor configuration.
 */
public class MonitorConfigurationTest extends TestCase {
	
	
	@Before
	@Override
	public void setUp() throws Exception {
		super.setUp();
		
		for (String propName: System.getProperties().stringPropertyNames()) {
			if (propName.startsWith(MonitorConfiguration.PREFIX)) {
				System.clearProperty(propName);
			}
		}
	}
	
	
	public void testConstants() {
		
		assertEquals(30 * 60, MonitorConfiguration.DEFAULT_ENTRY_COUNT_CACHE_TIMEOUT);
	}


	public void testEmpty() {

		MonitorConfiguration config = new MonitorConfiguration(new Properties());

		assertEquals(MonitorConfiguration.DEFAULT_ENTRY_COUNT_CACHE_TIMEOUT, config.entryCountCacheTimeout);
		assertFalse(config.enableJMX);
		assertFalse(config.graphite.enable);
		assertNull(config.graphite.host);
		assertEquals(0, config.graphite.port);
		assertEquals(0, config.graphite.reportInterval);
		assertEquals(0, config.graphite.batchSize);
		assertNull(config.graphite.prefix);
		assertNull(config.graphite.ratesTimeUnit);
		assertNull(config.graphite.durationsTimeUnit);
		assertNull(config.graphite.filter);

		config.log();
	}
	
	
	public void testExampleConfig() {
		
		Properties props = new Properties();
		props.setProperty("monitor.entryCountCacheTimeout", "-1");
		props.setProperty("monitor.enableJMX", "true");
		props.setProperty("monitor.graphite.enable", "true");
		props.setProperty("monitor.graphite.host", "carbon.server.com");
		props.setProperty("monitor.graphite.port", "2003");
		props.setProperty("monitor.graphite.reportInterval", "60");
		props.setProperty("monitor.graphite.batchSize", "100");
		props.setProperty("monitor.graphite.prefix", "c2id-server-1.");
		props.setProperty("monitor.graphite.ratesTimeUnit", "SECONDS");
		props.setProperty("monitor.graphite.durationsTimeUnit", "MILLISECONDS");
		props.setProperty("monitor.graphite.filter.1", "authzStore.ldapConnector.*");
		props.setProperty("monitor.graphite.filter.2", "tokenEndpoint.code.*");
		props.setProperty("monitor.graphite.filter.3", "tokenEndpoint.refreshToken.*");

		MonitorConfiguration config = new MonitorConfiguration(props);

		assertEquals(-1L, config.entryCountCacheTimeout);
		assertTrue(config.enableJMX);
		assertTrue(config.graphite.enable);
		assertEquals("carbon.server.com", config.graphite.host);
		assertEquals(2003, config.graphite.port);
		assertEquals(60, config.graphite.reportInterval);
		assertEquals(100, config.graphite.batchSize);
		assertEquals("c2id-server-1.", config.graphite.prefix);
		assertEquals(TimeUnit.SECONDS, config.graphite.ratesTimeUnit);
		assertEquals(TimeUnit.MILLISECONDS, config.graphite.durationsTimeUnit);
		assertTrue(config.graphite.filter.getWhiteList().contains("authzStore.ldapConnector.*"));
		assertTrue(config.graphite.filter.getWhiteList().contains("tokenEndpoint.code.*"));
		assertTrue(config.graphite.filter.getWhiteList().contains("tokenEndpoint.refreshToken.*"));
		assertEquals(3, config.graphite.filter.getWhiteList().size());

		config.log();
	}


	public void testGraphite_noPrefix() {

		Properties props = new Properties();
		props.setProperty("monitor.enableJMX", "true");
		props.setProperty("monitor.graphite.enable", "true");
		props.setProperty("monitor.graphite.host", "carbon.server.com");
		props.setProperty("monitor.graphite.port", "2003");
		props.setProperty("monitor.graphite.reportInterval", "60");
		props.setProperty("monitor.graphite.batchSize", "100");
		// props.setProperty("monitor.graphite.prefix", "");
		props.setProperty("monitor.graphite.ratesTimeUnit", "SECONDS");
		props.setProperty("monitor.graphite.durationsTimeUnit", "MILLISECONDS");
		props.setProperty("monitor.graphite.filter.1", "authzStore.ldapConnector.*");
		props.setProperty("monitor.graphite.filter.2", "tokenEndpoint.code.*");
		props.setProperty("monitor.graphite.filter.3", "tokenEndpoint.refreshToken.*");

		MonitorConfiguration config = new MonitorConfiguration(props);

		assertTrue(config.enableJMX);
		assertTrue(config.graphite.enable);
		assertEquals("carbon.server.com", config.graphite.host);
		assertEquals(2003, config.graphite.port);
		assertEquals(60, config.graphite.reportInterval);
		assertEquals(100, config.graphite.batchSize);
		assertNull(config.graphite.prefix);
		assertEquals(TimeUnit.SECONDS, config.graphite.ratesTimeUnit);
		assertEquals(TimeUnit.MILLISECONDS, config.graphite.durationsTimeUnit);
		assertTrue(config.graphite.filter.getWhiteList().contains("authzStore.ldapConnector.*"));
		assertTrue(config.graphite.filter.getWhiteList().contains("tokenEndpoint.code.*"));
		assertTrue(config.graphite.filter.getWhiteList().contains("tokenEndpoint.refreshToken.*"));
		assertEquals(3, config.graphite.filter.getWhiteList().size());

		config.log();
	}


	public void testGraphite_emptyPrefix() {

		Properties props = new Properties();
		props.setProperty("monitor.enableJMX", "true");
		props.setProperty("monitor.graphite.enable", "true");
		props.setProperty("monitor.graphite.host", "carbon.server.com");
		props.setProperty("monitor.graphite.port", "2003");
		props.setProperty("monitor.graphite.reportInterval", "60");
		props.setProperty("monitor.graphite.batchSize", "100");
		props.setProperty("monitor.graphite.prefix", "   ");
		props.setProperty("monitor.graphite.ratesTimeUnit", "SECONDS");
		props.setProperty("monitor.graphite.durationsTimeUnit", "MILLISECONDS");
		props.setProperty("monitor.graphite.filter.1", "authzStore.ldapConnector.*");
		props.setProperty("monitor.graphite.filter.2", "tokenEndpoint.code.*");
		props.setProperty("monitor.graphite.filter.3", "tokenEndpoint.refreshToken.*");

		MonitorConfiguration config = new MonitorConfiguration(props);

		assertTrue(config.enableJMX);
		assertTrue(config.graphite.enable);
		assertEquals("carbon.server.com", config.graphite.host);
		assertEquals(2003, config.graphite.port);
		assertEquals(60, config.graphite.reportInterval);
		assertEquals(100, config.graphite.batchSize);
		assertNull(config.graphite.prefix);
		assertEquals(TimeUnit.SECONDS, config.graphite.ratesTimeUnit);
		assertEquals(TimeUnit.MILLISECONDS, config.graphite.durationsTimeUnit);
		assertTrue(config.graphite.filter.getWhiteList().contains("authzStore.ldapConnector.*"));
		assertTrue(config.graphite.filter.getWhiteList().contains("tokenEndpoint.code.*"));
		assertTrue(config.graphite.filter.getWhiteList().contains("tokenEndpoint.refreshToken.*"));
		assertEquals(3, config.graphite.filter.getWhiteList().size());

		config.log();
	}


	public void testGraphite_trimFilterStrings() {

		Properties props = new Properties();
		props.setProperty("monitor.enableJMX", "true");
		props.setProperty("monitor.graphite.enable", "true");
		props.setProperty("monitor.graphite.host", "carbon.server.com");
		props.setProperty("monitor.graphite.port", "2003");
		props.setProperty("monitor.graphite.reportInterval", "60");
		props.setProperty("monitor.graphite.batchSize", "100");
		props.setProperty("monitor.graphite.prefix", "c2id-server-1.");
		props.setProperty("monitor.graphite.ratesTimeUnit", "SECONDS");
		props.setProperty("monitor.graphite.durationsTimeUnit", "MILLISECONDS");
		props.setProperty("monitor.graphite.filter.1", "   authzStore.ldapConnector.*   ");
		props.setProperty("monitor.graphite.filter.2", "   tokenEndpoint.code.*   ");
		props.setProperty("monitor.graphite.filter.3", "   tokenEndpoint.refreshToken.*   ");

		MonitorConfiguration config = new MonitorConfiguration(props);

		assertTrue(config.graphite.filter.getWhiteList().contains("authzStore.ldapConnector.*"));
		assertTrue(config.graphite.filter.getWhiteList().contains("tokenEndpoint.code.*"));
		assertTrue(config.graphite.filter.getWhiteList().contains("tokenEndpoint.refreshToken.*"));
		assertEquals(3, config.graphite.filter.getWhiteList().size());

		config.log();
	}


	public void testGraphite_filterMatchAllOverride() {

		Properties props = new Properties();
		props.setProperty("monitor.enableJMX", "true");
		props.setProperty("monitor.graphite.enable", "true");
		props.setProperty("monitor.graphite.host", "carbon.server.com");
		props.setProperty("monitor.graphite.port", "2003");
		props.setProperty("monitor.graphite.reportInterval", "60");
		props.setProperty("monitor.graphite.batchSize", "100");
		props.setProperty("monitor.graphite.prefix", "c2id-server-1.");
		props.setProperty("monitor.graphite.ratesTimeUnit", "SECONDS");
		props.setProperty("monitor.graphite.durationsTimeUnit", "MILLISECONDS");
		props.setProperty("monitor.graphite.filter.1", "*"); // should override the rest
		props.setProperty("monitor.graphite.filter.2", "tokenEndpoint.code.*");
		props.setProperty("monitor.graphite.filter.3", "tokenEndpoint.refreshToken.*");

		MonitorConfiguration config = new MonitorConfiguration(props);

		assertTrue(config.graphite.filter.matchesAny());
		assertFalse(config.graphite.filter.matchesNone());
		assertEquals(3, config.graphite.filter.getWhiteList().size());

		config.log();
	}


	public void testGraphite_filterNone() {

		Properties props = new Properties();
		props.setProperty("monitor.enableJMX", "true");
		props.setProperty("monitor.graphite.enable", "true");
		props.setProperty("monitor.graphite.host", "carbon.server.com");
		props.setProperty("monitor.graphite.port", "2003");
		props.setProperty("monitor.graphite.reportInterval", "60");
		props.setProperty("monitor.graphite.batchSize", "100");
		props.setProperty("monitor.graphite.prefix", "c2id-server-1.");
		props.setProperty("monitor.graphite.ratesTimeUnit", "SECONDS");
		props.setProperty("monitor.graphite.durationsTimeUnit", "MILLISECONDS");
		props.setProperty("monitor.graphite.filter", "");

		MonitorConfiguration config = new MonitorConfiguration(props);

		assertFalse(config.graphite.filter.matchesAny());
		assertTrue(config.graphite.filter.matchesNone());
		assertTrue(config.graphite.filter.getWhiteList().isEmpty());

		config.log();
	}


	public void testGraphite_disabled() {

		Properties props = new Properties();
		props.setProperty("monitor.enableJMX", "true");
		props.setProperty("monitor.graphite.enable", "false");
		props.setProperty("monitor.graphite.host", "carbon.server.com");
		props.setProperty("monitor.graphite.port", "2003");
		props.setProperty("monitor.graphite.reportInterval", "60");
		props.setProperty("monitor.graphite.batchSize", "100");
		props.setProperty("monitor.graphite.prefix", "c2id-server-1.");
		props.setProperty("monitor.graphite.ratesTimeUnit", "SECONDS");
		props.setProperty("monitor.graphite.durationsTimeUnit", "MILLISECONDS");
		props.setProperty("monitor.graphite.filter.1", "authzStore.ldapConnector.*");
		props.setProperty("monitor.graphite.filter.2", "tokenEndpoint.code.*");
		props.setProperty("monitor.graphite.filter.3", "tokenEndpoint.refreshToken.*");

		MonitorConfiguration config = new MonitorConfiguration(props);

		assertTrue(config.enableJMX);
		assertFalse(config.graphite.enable);
		assertNull(config.graphite.host);
		assertEquals(0, config.graphite.port);
		assertEquals(0, config.graphite.reportInterval);
		assertEquals(0, config.graphite.batchSize);
		assertNull(config.graphite.prefix);
		assertNull(config.graphite.ratesTimeUnit);
		assertNull(config.graphite.durationsTimeUnit);
		assertNull(config.graphite.filter);

		config.log();
	}
}
