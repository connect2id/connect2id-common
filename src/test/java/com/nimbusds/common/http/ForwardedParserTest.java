package com.nimbusds.common.http;


import junit.framework.TestCase;


public class ForwardedParserTest extends TestCase {
	
	
	public void testGetClientIPAddressFromXForwardedForHeader() {
		
		assertEquals("1.2.3.4", ForwardedParser.getClientIPAddressFromXForwardedForHeader("1.2.3.4"));
		assertEquals("1.2.3.4", ForwardedParser.getClientIPAddressFromXForwardedForHeader("1.2.3.4, 192.168.1.101"));
		
		assertNull(ForwardedParser.getClientIPAddressFromXForwardedForHeader(""));
	}
	
	
	public void testGetClientIPAddressFromForwardedHeader() {
		
		// Examples from https://tools.ietf.org/html/rfc7239
		assertEquals("_gazonk", ForwardedParser.getClientIPAddressFromForwardedHeader("for=\"_gazonk\""));
		assertEquals("[2001:db8:cafe::17]:4711", ForwardedParser.getClientIPAddressFromForwardedHeader("For=\"[2001:db8:cafe::17]:4711\""));
		assertEquals("192.0.2.60", ForwardedParser.getClientIPAddressFromForwardedHeader("for=192.0.2.60;proto=http;by=203.0.113.43"));
		assertEquals("192.0.2.60", ForwardedParser.getClientIPAddressFromForwardedHeader("for=192.0.2.60; proto=http; by=203.0.113.43"));
		
		assertNull(ForwardedParser.getClientIPAddressFromXForwardedForHeader(""));
	}
}
