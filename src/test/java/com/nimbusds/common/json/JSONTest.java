package com.nimbusds.common.json;


import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import junit.framework.TestCase;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class JSONTest extends TestCase {

	
	public void testInt() {
		
		assertEquals("1", JSON.toJSON(1));
		assertEquals("-1", JSON.toJSON(-1));
		
		assertEquals("13", JSON.toJSON(13));
		assertEquals("-13", JSON.toJSON(-13));
	}

	
	public void testBoolean() {
		
		assertEquals("true", JSON.toJSON(true));
		assertEquals("false", JSON.toJSON(false));
	}

	
	public void testString() {
		
		assertEquals("\"BG\"", JSON.toJSON("BG"));
		assertEquals("\"БГ\"", JSON.toJSON("БГ"));
	}
	
	
	public void testJSONArray() {
		
		assertEquals("[\"a\",\"b\",\"c\"]", JSON.toJSON(Arrays.asList("a", "b", "c")));
		assertEquals("[\"а\",\"б\",\"в\"]", JSON.toJSON(Arrays.asList("а", "б", "в")));
	}
	
	
	public void testJSONObject() {
		
		Map<String,Object> o = new HashMap<>();
		o.put("country", "BG");
		assertEquals("{\"country\":\"BG\"}", JSON.toJSON(o));
	}
	
	
	public void testJSONObject_nullValues() {
		
		Map<String,Object> o = new HashMap<>();
		o.put("email", null);
		assertEquals("{\"email\":null}", JSON.toJSON(o));
	}
	
	
	public void testJSONObjectUnicode() {
		
		Map<String,Object> o = new HashMap<>();
		o.put("country", "БГ");
		assertEquals("{\"country\":\"БГ\"}", JSON.toJSON(o));
	}
	
	
	public void testTilde() {
		
		Map<String,Object> o = new HashMap<>();
		o.put("tilde", "ВД~");

		assertEquals("{\"tilde\":\"ВД~\"}", JSON.toJSON(o));
	}
	
	
	public void testSpecialLatinChar() {
		
		Map<String,Object> o = new HashMap<>();
		o.put("char", "æ");

		assertEquals("{\"char\":\"æ\"}", JSON.toJSON(o));
	}
	
	
	public void testNull() {
		
		assertEquals("null", JSON.toJSON(null));
	}
	
	
	public void testMultiByteUnicodeChar() throws ParseException {
	
		byte[] charBytes = new byte[]{(byte)0xF0, (byte)0xA0, (byte)0x9C, (byte)0xB1};
		
		String s = new String(charBytes, StandardCharsets.UTF_8);
		
		Map<String, Object> jsonObject = new HashMap<>();
		jsonObject.put("s", s);
		
		String json = JSON.toJSON(jsonObject);
		
//		System.out.println(json);
		
		Map<String, Object> parsedJsonObject = JSONObjectUtils.parse(json);
		assertEquals(s, parsedJsonObject.get("s"));
		assertEquals(jsonObject, parsedJsonObject);
	}
}
