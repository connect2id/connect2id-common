package com.nimbusds.common.json;


import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import junit.framework.TestCase;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import java.io.StringWriter;


/**
 * Tests the JSON object writer.
 */
public class JSONObjectWriterTest extends TestCase {


	public void testNullWriter() {

		try {
			new JSONObjectWriter<>(null, new KeyExtractorImpl());
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The writer must not be null", e.getMessage());
		}
	}


	public void testNullKeyExtractor() {

		try {
			new JSONObjectWriter<AppendedElement>(new StringWriter(), null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The key extractor must not be null", e.getMessage());
		}
	}


	public void testWriteEmpty()
		throws Exception {

		StringWriter writer = new StringWriter();

		JSONObjectWriter<AppendedElement> jsonObjectWriter = new JSONObjectWriter<>(writer, new KeyExtractorImpl());

		jsonObjectWriter.writeStart();
		jsonObjectWriter.writeEnd();

		assertEquals("{}", writer.toString());
	}


	public void testWriteOneElement()
		throws Exception {

		StringWriter writer = new StringWriter();

		JSONObjectWriter<AppendedElement> jsonObjectWriter = new JSONObjectWriter<>(writer, new KeyExtractorImpl());

		jsonObjectWriter.writeStart();

		jsonObjectWriter.accept(new AppendedElement("apples", "fuji"));

		jsonObjectWriter.writeEnd();

		JSONObject jsonObject = JSONObjectUtils.parseJSONObject(writer.toString());
		assertEquals(1, jsonObject.size());

		JSONArray a = (JSONArray)jsonObject.get("apples");
		assertEquals("fuji", a.get(0));
		assertEquals(1, a.size());
	}


	public void testWriteTwoElements()
		throws Exception {

		StringWriter writer = new StringWriter();

		JSONObjectWriter<AppendedElement> jsonObjectWriter = new JSONObjectWriter<>(writer, new KeyExtractorImpl());

		jsonObjectWriter.writeStart();

		jsonObjectWriter.accept(new AppendedElement("apples", "fuji", "toko"));
		jsonObjectWriter.accept(new AppendedElement("pears", "gold", "smith"));

		jsonObjectWriter.writeEnd();

		JSONObject jsonObject = JSONObjectUtils.parseJSONObject(writer.toString());
		assertEquals(2, jsonObject.size());

		JSONArray a = (JSONArray)jsonObject.get("apples");
		assertEquals("fuji", a.get(0));
		assertEquals("toko", a.get(1));
		assertEquals(2, a.size());

		a = (JSONArray)jsonObject.get("pears");
		assertEquals("gold", a.get(0));
		assertEquals("smith", a.get(1));
		assertEquals(2, a.size());
	}


	public void testWriteThreeElements()
		throws Exception {

		StringWriter writer = new StringWriter();

		JSONObjectWriter<AppendedElement> jsonObjectWriter = new JSONObjectWriter<>(writer, new KeyExtractorImpl());

		jsonObjectWriter.writeStart();

		jsonObjectWriter.accept(new AppendedElement("apples", "fuji", "toko", "orin"));
		jsonObjectWriter.accept(new AppendedElement("pears", "gold", "smith"));
		jsonObjectWriter.accept(new AppendedElement("plums", "elena", "tegera"));

		jsonObjectWriter.writeEnd();

		JSONObject jsonObject = JSONObjectUtils.parseJSONObject(writer.toString());
		assertEquals(3, jsonObject.size());

		JSONArray a = (JSONArray)jsonObject.get("apples");
		assertEquals("fuji", a.get(0));
		assertEquals("toko", a.get(1));
		assertEquals("orin", a.get(2));
		assertEquals(3, a.size());

		a = (JSONArray)jsonObject.get("pears");
		assertEquals("gold", a.get(0));
		assertEquals("smith", a.get(1));
		assertEquals(2, a.size());

		a = (JSONArray)jsonObject.get("plums");
		assertEquals("elena", a.get(0));
		assertEquals("tegera", a.get(1));
		assertEquals(2, a.size());
	}
}
