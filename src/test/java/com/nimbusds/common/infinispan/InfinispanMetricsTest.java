package com.nimbusds.common.infinispan;


import com.nimbusds.common.monitor.MonitorRegistries;
import junit.framework.TestCase;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class InfinispanMetricsTest extends TestCase {
	

	EmbeddedCacheManager cacheManager;
	
	
	@Before
	public void setUp() {
		
		cacheManager = new DefaultCacheManager();
	}
	
	
	@After
	public void tearDown() {
		
		if (cacheManager != null)
			cacheManager.stop();
	}
	
	
	@Test
	public void testRegisterMetrics() {
		
		InfinispanMetrics.register(cacheManager);
		
		assertFalse((Boolean)MonitorRegistries.getMetricRegistry().getGauges().get("infinispan.isCoordinator").getValue());
		assertEquals(0, ((Integer)MonitorRegistries.getMetricRegistry().getGauges().get("infinispan.numClusterMembers").getValue()).intValue());
	}
}
