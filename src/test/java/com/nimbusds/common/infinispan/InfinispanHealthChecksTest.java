package com.nimbusds.common.infinispan;


import com.nimbusds.common.monitor.MonitorRegistries;
import junit.framework.TestCase;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class InfinispanHealthChecksTest extends TestCase {
	

	EmbeddedCacheManager cacheManager;
	
	
	@Before
	public void setUp() {
		
		cacheManager = new DefaultCacheManager();
		
		for (String healthCheckName: MonitorRegistries.getHealthCheckRegistry().getNames()) {
			
			MonitorRegistries.getHealthCheckRegistry().unregister(healthCheckName);
		}
	}
	
	
	@After
	public void tearDown() {
		
		if (cacheManager == null) return;
		
		MonitorRegistries.getHealthCheckRegistry().unregister("cacheA.availability");
		MonitorRegistries.getHealthCheckRegistry().unregister("cacheB.availability");
	}
	
	
	@Test
	public void testRegisterHealthChecks() {
		
		InfinispanHealthChecks.register(cacheManager);
		
		assertTrue(MonitorRegistries.getHealthCheckRegistry().getNames().isEmpty());
		
		ConfigurationBuilder b = new ConfigurationBuilder();
		b.clustering()
			.cacheMode(CacheMode.LOCAL)
			.create();
		cacheManager.defineConfiguration("cacheA", b.build());
		
		Cache<String,String> cacheA = cacheManager.getCache("cacheA");
		
		cacheA.put("key", "value");
		
		InfinispanHealthChecks.register(cacheManager);
		
		assertTrue(MonitorRegistries.getHealthCheckRegistry().getNames().contains("cacheA.availability"));
		assertEquals(1, MonitorRegistries.getHealthCheckRegistry().getNames().size());
		
		assertTrue(MonitorRegistries.getHealthCheckRegistry().runHealthCheck("cacheA.availability").isHealthy());
		
		b = new ConfigurationBuilder();
		b.clustering()
			.cacheMode(CacheMode.LOCAL)
			.create();
		cacheManager.defineConfiguration("cacheB", b.build());
		
		Cache<String,String> cacheB = cacheManager.getCache("cacheB");
		
		cacheB.put("key", "value");
		
		InfinispanHealthChecks.register(cacheManager);
		
		assertTrue(MonitorRegistries.getHealthCheckRegistry().getNames().contains("cacheA.availability"));
		assertTrue(MonitorRegistries.getHealthCheckRegistry().getNames().contains("cacheB.availability"));
		assertEquals(2, MonitorRegistries.getHealthCheckRegistry().getNames().size());
		
		assertTrue(MonitorRegistries.getHealthCheckRegistry().runHealthCheck("cacheA.availability").isHealthy());
		assertTrue(MonitorRegistries.getHealthCheckRegistry().runHealthCheck("cacheB.availability").isHealthy());
	}
}
