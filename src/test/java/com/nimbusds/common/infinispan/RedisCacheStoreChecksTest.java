package com.nimbusds.common.infinispan;


import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.persistence.redis.configuration.CommonRedisStoreConfiguration;
import org.infinispan.persistence.redis.configuration.advanced.RedisStoreConfigurationBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


public class RedisCacheStoreChecksTest {
	
	
	private EmbeddedCacheManager cacheManager;
	

	@Before
	public void setUp() {
		
		cacheManager = new DefaultCacheManager(
			new GlobalConfigurationBuilder()
				.build());
	}
	
	
	@After
	public void tearDown() {
		
		if (cacheManager != null)
			cacheManager.stop();
	}
	
	
	@Test
	public void testSingleOK() throws RedisCacheStoreChecks.ConfigWarning {
		
		ConfigurationBuilder b = new ConfigurationBuilder();
		b.persistence()
			.addStore(RedisStoreConfigurationBuilder.class)
			.segmented(false)
			.shared(false)
			.preload(false)
			.purgeOnStartup(true)
			.database(1)
			.topology(CommonRedisStoreConfiguration.Topology.SERVER)
			.addServer()
			.host("127.0.0.1")
			.create();
		
		cacheManager.defineConfiguration("map-1", b.build());
		
		cacheManager.getCache("map-1");
		
		RedisCacheStoreChecks.check(cacheManager);
	}
	
	
	@Test
	public void testSingleRejectZeroDB() {
		
		ConfigurationBuilder b = new ConfigurationBuilder();
		b.persistence()
			.addStore(RedisStoreConfigurationBuilder.class)
			.segmented(false)
			.shared(false)
			.preload(false)
			.purgeOnStartup(true)
			.database(0)
			.topology(CommonRedisStoreConfiguration.Topology.SERVER)
			.addServer()
			.host("127.0.0.1")
			.create();
		
		cacheManager.defineConfiguration("map-1", b.build());
		
		cacheManager.getCache("map-1");
		
		try {
			RedisCacheStoreChecks.check(cacheManager);
			fail();
		} catch (RedisCacheStoreChecks.ConfigWarning e) {
			assertEquals("The Redis Cache store for map-1 must specify a non-zero database number to prevent potential conflicts with meta keys such as ElastiCacheMasterReplicationTimestamp that may get put into the default (0) database", e.getMessage());
		}
	}
	
	
	@Test
	public void testTwoOK() throws RedisCacheStoreChecks.ConfigWarning {
		
		ConfigurationBuilder b = new ConfigurationBuilder();
		b.persistence()
			.addStore(RedisStoreConfigurationBuilder.class)
			.segmented(false)
			.shared(false)
			.preload(false)
			.purgeOnStartup(true)
			.database(1)
			.topology(CommonRedisStoreConfiguration.Topology.SERVER)
			.addServer()
			.host("127.0.0.1")
			.create();
		
		cacheManager.defineConfiguration("map-1", b.build());
		
		b = new ConfigurationBuilder();
		b.persistence()
			.addStore(RedisStoreConfigurationBuilder.class)
			.segmented(false)
			.shared(false)
			.preload(false)
			.purgeOnStartup(true)
			.database(2)
			.topology(CommonRedisStoreConfiguration.Topology.SERVER)
			.addServer()
			.host("127.0.0.1")
			.create();
		
		cacheManager.defineConfiguration("map-2", b.build());
		
		cacheManager.getCache("map-1");
		cacheManager.getCache("map-2");
		
		RedisCacheStoreChecks.check(cacheManager);
	}
	
	
	@Test
	public void testSameDbNumbersOnDifferentServersOK() throws RedisCacheStoreChecks.ConfigWarning {
		
		ConfigurationBuilder b = new ConfigurationBuilder();
		b.persistence()
			.addStore(RedisStoreConfigurationBuilder.class)
			.segmented(false)
			.shared(false)
			.preload(false)
			.purgeOnStartup(true)
			.database(1)
			.topology(CommonRedisStoreConfiguration.Topology.SERVER)
			.addServer()
			.host("127.0.0.1")
			.create();
		
		cacheManager.defineConfiguration("map-1", b.build());
		
		b = new ConfigurationBuilder();
		b.persistence()
			.addStore(RedisStoreConfigurationBuilder.class)
			.segmented(false)
			.shared(false)
			.preload(false)
			.purgeOnStartup(true)
			.database(1)
			.topology(CommonRedisStoreConfiguration.Topology.SERVER)
			.addServer()
			.host("localhost")
			.create();
		
		cacheManager.defineConfiguration("map-2", b.build());
		
		cacheManager.getCache("map-1");
		cacheManager.getCache("map-2");
		
		RedisCacheStoreChecks.check(cacheManager);
	}
	
	
	@Test
	public void testTwoRejectSameDbNumbers() throws RedisCacheStoreChecks.ConfigWarning {
		
		ConfigurationBuilder b = new ConfigurationBuilder();
		b.persistence()
			.addStore(RedisStoreConfigurationBuilder.class)
			.segmented(false)
			.shared(false)
			.preload(false)
			.purgeOnStartup(true)
			.database(1)
			.topology(CommonRedisStoreConfiguration.Topology.SERVER)
			.addServer()
			.host("127.0.0.1")
			.create();
		
		cacheManager.defineConfiguration("map-1", b.build());
		
		b = new ConfigurationBuilder();
		b.persistence()
			.addStore(RedisStoreConfigurationBuilder.class)
			.segmented(false)
			.shared(false)
			.preload(false)
			.purgeOnStartup(true)
			.database(1)
			.topology(CommonRedisStoreConfiguration.Topology.SERVER)
			.addServer()
			.host("127.0.0.1")
			.create();
		
		cacheManager.defineConfiguration("map-2", b.build());
		
		cacheManager.getCache("map-1");
		cacheManager.getCache("map-2");
		
		try {
			RedisCacheStoreChecks.check(cacheManager);
			fail();
		} catch (RedisCacheStoreChecks.ConfigError e) {
			assertEquals("The Redis Cache store 127.0.0.1:6379 for map-1 uses a database number 1 that is already taken", e.getMessage());
		}
	}
}
