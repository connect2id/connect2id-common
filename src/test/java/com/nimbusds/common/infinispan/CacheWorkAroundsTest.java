package com.nimbusds.common.infinispan;


import junit.framework.TestCase;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.eviction.EvictionType;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;

import java.util.UUID;


public class CacheWorkAroundsTest extends TestCase {
	
	
	EmbeddedCacheManager cacheManager;
	
	
	@After
	public void tearDown() {
		
		if (cacheManager == null) return;
		
		cacheManager.stop();
	}
	
	
	Cache<String, String> createCacheLegacy(final CacheMode cacheMode, final int size) {
		
		cacheManager = new DefaultCacheManager(
			new GlobalConfigurationBuilder()
				.transport()
				.defaultTransport()
				.clusterName(UUID.randomUUID().toString())
				.build());
		
		ConfigurationBuilder b = new ConfigurationBuilder();
		b.clustering()
			.cacheMode(cacheMode)
			.create();
		b.memory()
			.evictionType(EvictionType.COUNT) // deprecated
			.size(size) // deprecated
			.create();
		
		cacheManager.defineConfiguration("test-cache", b.build());
		
		return cacheManager.getCache("test-cache");
	}
	
	
	Cache<String, String> createCache(final CacheMode cacheMode, final int size) {
		
		cacheManager = new DefaultCacheManager(
			new GlobalConfigurationBuilder()
				.transport()
				.defaultTransport()
				.clusterName(UUID.randomUUID().toString())
				.build());
		
		ConfigurationBuilder b = new ConfigurationBuilder();
		b.clustering()
			.cacheMode(cacheMode)
			.create();
		b.memory()
			.whenFull(EvictionStrategy.REMOVE)
			.maxCount(size)
			.create();
		
		cacheManager.defineConfiguration("test-cache", b.build());
		
		return cacheManager.getCache("test-cache");
	}
	
	
	public void testInvalidationSync() {
		
		Cache<String,String> sampleCache = createCacheLegacy(CacheMode.INVALIDATION_SYNC, -1);
		
		CacheWorkArounds<String,String> cacheWithWorkArounds = new CacheWorkArounds<>(sampleCache);
		
		assertFalse(cacheWithWorkArounds.isStateless());
		assertTrue(cacheWithWorkArounds.isInvalidation());
		assertEquals(CacheWorkArounds.Mode.INVALIDATION, cacheWithWorkArounds.getMode());
		
		assertTrue(sampleCache.isEmpty());
		sampleCache.put("key", "value");
		assertEquals(1, sampleCache.size());
		
		cacheWithWorkArounds.clearCacheIfStateless();
		
		assertEquals(1, sampleCache.size());
		assertEquals(1, sampleCache.getAdvancedCache().getDataContainer().size());
	}
	
	
	public void testInvalidationAsync() {
		
		Cache<String,String> sampleCache = createCacheLegacy(CacheMode.INVALIDATION_ASYNC, -1);
		
		CacheWorkArounds<String,String> cacheWithWorkArounds = new CacheWorkArounds<>(sampleCache);
		
		assertFalse(cacheWithWorkArounds.isStateless());
		assertTrue(cacheWithWorkArounds.isInvalidation());
		assertEquals(CacheWorkArounds.Mode.INVALIDATION, cacheWithWorkArounds.getMode());
		
		assertTrue(sampleCache.isEmpty());
		sampleCache.put("key", "value");
		assertEquals(1, sampleCache.size());
		
		cacheWithWorkArounds.clearCacheIfStateless();
		
		assertEquals(1, sampleCache.size());
		assertEquals(1, sampleCache.getAdvancedCache().getDataContainer().size());
	}
	
	
	public void testStatelessMode() {
		
		Cache<String,String> sampleCache = createCache(CacheMode.LOCAL, 1);
		
		assertEquals(CacheMode.LOCAL, sampleCache.getCacheConfiguration().clustering().cacheMode());
		assertEquals("Legacy", 1L, sampleCache.getCacheConfiguration().memory().size());
		assertEquals("New API", 1L, sampleCache.getCacheConfiguration().memory().maxCount());
		
		CacheWorkArounds<String,String> cacheWithWorkArounds = new CacheWorkArounds<>(sampleCache);
		
		assertTrue(cacheWithWorkArounds.isStateless());
		assertFalse(cacheWithWorkArounds.isInvalidation());
		assertEquals(CacheWorkArounds.Mode.STATELESS, cacheWithWorkArounds.getMode());
		
		assertTrue(sampleCache.isEmpty());
		sampleCache.put("key", "value");
		assertEquals(1, sampleCache.size());
		
		cacheWithWorkArounds.clearCacheIfStateless();
		
		assertTrue(sampleCache.isEmpty());
		assertEquals(0, sampleCache.getAdvancedCache().getDataContainer().size());
	}
	
	
	public void testStatelessModeLegacy() {
		
		Cache<String,String> sampleCache = createCacheLegacy(CacheMode.LOCAL, 1);
		
		assertEquals(CacheMode.LOCAL, sampleCache.getCacheConfiguration().clustering().cacheMode());
		assertEquals(1L, sampleCache.getCacheConfiguration().memory().size());
		
		CacheWorkArounds<String,String> cacheWithWorkArounds = new CacheWorkArounds<>(sampleCache);
		
		assertTrue(cacheWithWorkArounds.isStateless());
		assertFalse(cacheWithWorkArounds.isInvalidation());
		assertEquals(CacheWorkArounds.Mode.STATELESS, cacheWithWorkArounds.getMode());
		
		assertTrue(sampleCache.isEmpty());
		sampleCache.put("key", "value");
		assertEquals(1, sampleCache.size());
		
		cacheWithWorkArounds.clearCacheIfStateless();
		
		assertTrue(sampleCache.isEmpty());
		assertEquals(0, sampleCache.getAdvancedCache().getDataContainer().size());
	}
	
	
	public void testNormalLocalMode() {
		
		Cache<String,String> sampleCache = createCacheLegacy(CacheMode.LOCAL, -1);
		
		assertEquals(CacheMode.LOCAL, sampleCache.getCacheConfiguration().clustering().cacheMode());
		assertEquals(-1L, sampleCache.getCacheConfiguration().memory().maxCount());
		
		CacheWorkArounds<String,String> cacheWithWorkArounds = new CacheWorkArounds<>(sampleCache);
		
		assertFalse(cacheWithWorkArounds.isStateless());
		assertFalse(cacheWithWorkArounds.isInvalidation());
		assertNull(cacheWithWorkArounds.getMode());
		
		assertTrue(sampleCache.isEmpty());
		sampleCache.put("key", "value");
		assertEquals(1, sampleCache.size());
		
		cacheWithWorkArounds.clearCacheIfStateless();
		
		assertEquals(1, sampleCache.size());
		assertEquals(1, sampleCache.getAdvancedCache().getDataContainer().size());
	}
}
