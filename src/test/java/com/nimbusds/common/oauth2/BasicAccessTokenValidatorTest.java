package com.nimbusds.common.oauth2;


import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.BearerTokenError;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import junit.framework.TestCase;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


public class BasicAccessTokenValidatorTest extends TestCase {


	public void testConstructor() {

		BearerAccessToken accessToken = new BearerAccessToken();

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(accessToken);

		assertFalse(validator.accessIsDisabled());
	}


	public void testConstructorVarArgs() {

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(
			new BearerAccessToken(),
			new BearerAccessToken()
		);

		assertFalse(validator.accessIsDisabled());
	}


	public void testConstructorNull() {

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator((BearerAccessToken) null);
		
		assertTrue(validator.accessIsDisabled());
	}


	public void testConstructorNullVarArgs() {

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator((BearerAccessToken) null, (BearerAccessToken) null);
		
		assertTrue(validator.accessIsDisabled());
	}


	public void testPass() {

		BearerAccessToken accessToken = new BearerAccessToken();

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(accessToken);

		validator.validateBearerAccessToken(accessToken.toAuthorizationHeader());
	}


	public void testPassVarArgs() {

		BearerAccessToken t1 = new BearerAccessToken();
		BearerAccessToken t2 = new BearerAccessToken();

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(t1, t2);

		validator.validateBearerAccessToken(t1.toAuthorizationHeader());
		validator.validateBearerAccessToken(t2.toAuthorizationHeader());
	}
	
	
	public void testPass_servletAPI_authzHeader()
		throws IOException {
		
		BearerAccessToken accessToken = new BearerAccessToken();
		
		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(accessToken);
		
		HttpServletRequest request = new MockHttpServletRequest() {
			@Override
			public String getHeader(String s) {
				
				if ("Authorization".equalsIgnoreCase(s)) {
					return accessToken.toAuthorizationHeader();
				} else {
					return null;
				}
			}
		};
		
		assertTrue(validator.validateBearerAccessToken(request, new MockHttpServletResponse()));
	}
	
	
	public void testPassVarArgs_servletAPI_authzHeader()
		throws IOException {
		
		BearerAccessToken t1 = new BearerAccessToken();
		BearerAccessToken t2 = new BearerAccessToken();
		
		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(t1, t2);
		
		HttpServletRequest request = new MockHttpServletRequest() {
			@Override
			public String getHeader(String s) {
				
				if ("Authorization".equalsIgnoreCase(s)) {
					return t1.toAuthorizationHeader();
				} else {
					return null;
				}
			}
		};
		
		assertTrue(validator.validateBearerAccessToken(request, new MockHttpServletResponse()));
		
		request = new MockHttpServletRequest() {
			@Override
			public String getHeader(String s) {
				
				if ("Authorization".equalsIgnoreCase(s)) {
					return t2.toAuthorizationHeader();
				} else {
					return null;
				}
			}
		};
		
		assertTrue(validator.validateBearerAccessToken(request, new MockHttpServletResponse()));
	}
	
	
	public void testPass_servletAPI_queryParameter()
		throws IOException {
		
		BearerAccessToken accessToken = new BearerAccessToken();
		
		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(accessToken);
		
		HttpServletRequest request = new MockHttpServletRequest() {
			@Override
			public String getParameter(String s) {
				return s.equalsIgnoreCase("access_token") ? accessToken.getValue() : null;
			}
		};
		
		assertTrue(validator.validateBearerAccessToken(request, new MockHttpServletResponse()));
	}
	
	
	public void testPassVarArgs_servletAPI_queryParameter()
		throws IOException {
		
		BearerAccessToken t1 = new BearerAccessToken();
		BearerAccessToken t2 = new BearerAccessToken();
		
		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(t1, t2);
		
		HttpServletRequest request = new MockHttpServletRequest() {
			@Override
			public String getParameter(String s) {
				return s.equalsIgnoreCase("access_token") ? t1.getValue() : null;
			}
		};
		
		assertTrue(validator.validateBearerAccessToken(request, new MockHttpServletResponse()));
		
		request = new MockHttpServletRequest() {
			@Override
			public String getParameter(String s) {
				return s.equalsIgnoreCase("access_token") ? t2.getValue() : null;
			}
		};
		
		assertTrue(validator.validateBearerAccessToken(request, new MockHttpServletResponse()));
	}


	public void testWebAPIDisabled() {

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator((BearerAccessToken) null);

		BearerAccessToken accessToken = new BearerAccessToken();

		try {
			validator.validateBearerAccessToken(accessToken.toAuthorizationHeader());

			fail();

		} catch (WebApplicationException e) {
			// ok
			assertEquals(Response.Status.FORBIDDEN.getStatusCode(), e.getResponse().getStatus());

			assertNull(e.getResponse().getHeaderString("WWW-Authenticate"));

			assertEquals("application/json", e.getResponse().getMetadata().getFirst("content-type").toString());

			JSONObject jsonObject = (JSONObject)JSONValue.parse((String)e.getResponse().getEntity());
			assertEquals("web_api_disabled", (String)jsonObject.get("error"));
		}
	}


	public void testAccessDisabled() {

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator((BearerAccessToken) null);

		try {
			validator.validateBearerAccessToken(null);

			fail();

		} catch (WebApplicationException e) {
			// ok
			assertEquals(BasicAccessTokenValidator.WEB_API_DISABLED.toWebAppException().getResponse().getStatus(), e.getResponse().getStatus());

			assertNull(e.getResponse().getHeaderString("WWW-Authenticate"));

			assertEquals("application/json", e.getResponse().getMetadata().getFirst("content-type").toString());

			JSONObject jsonObject = (JSONObject)JSONValue.parse((String)e.getResponse().getEntity());
			assertEquals("web_api_disabled", (String)jsonObject.get("error"));
		}
	}


	public void testAccessDisabled_invalidHeader() {

		BasicAccessTokenValidator validator = new BasicAccessTokenValidator((BearerAccessToken) null);

		try {
			validator.validateBearerAccessToken("Authorization ");

			fail();

		} catch (WebApplicationException e) {
			// ok
			assertEquals(BasicAccessTokenValidator.WEB_API_DISABLED.toWebAppException().getResponse().getStatus(), e.getResponse().getStatus());
			
			assertNull(e.getResponse().getHeaderString("WWW-Authenticate"));
			
			assertEquals("application/json", e.getResponse().getMetadata().getFirst("content-type").toString());
			
			JSONObject jsonObject = (JSONObject)JSONValue.parse((String)e.getResponse().getEntity());
			assertEquals("web_api_disabled", (String)jsonObject.get("error"));
		}
	}


	public void testBadToken_authzHeader() {

		// Generate two random tokens
		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(new BearerAccessToken());

		BearerAccessToken otherAccessToken = new BearerAccessToken();

		try {
			validator.validateBearerAccessToken(otherAccessToken.toAuthorizationHeader());

			fail();

		} catch (WebApplicationException e) {
			// ok
			assertEquals(BearerTokenError.INVALID_TOKEN.getHTTPStatusCode(), e.getResponse().getStatus());

			assertEquals(BearerTokenError.INVALID_TOKEN.toWWWAuthenticateHeader(), e.getResponse().getHeaderString("WWW-Authenticate"));

			assertEquals("application/json", e.getResponse().getMetadata().getFirst("content-type").toString());

			JSONObject jsonObject = (JSONObject)JSONValue.parse((String)e.getResponse().getEntity());
			assertEquals("invalid_token", (String)jsonObject.get("error"));
		}
	}
	
	
	public void testBadToken_servletAPI_authzHeader()
		throws Exception {
		
		// Generate two random tokens
		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(new BearerAccessToken());
		
		BearerAccessToken otherAccessToken = new BearerAccessToken();
		
		HttpServletRequest request = new MockHttpServletRequest() {
			@Override
			public String getHeader(String s) {
				
				if ("Authorization".equalsIgnoreCase(s)) {
					return otherAccessToken.toAuthorizationHeader();
				} else {
					return null;
				}
			}
		};
		
		final AtomicInteger statusCode = new AtomicInteger();
		
		final AtomicReference<String> wwwAuthHeader = new AtomicReference<>();
		
		final AtomicReference<String> contentType = new AtomicReference<>();
		
		final StringWriter body = new StringWriter();
		
		HttpServletResponse response = new MockHttpServletResponse() {
			
			@Override
			public void setStatus(int i) {
				statusCode.set(i);
			}
			
			
			@Override
			public void setHeader(String s, String s1) {
				if ("WWW-Authenticate".equalsIgnoreCase(s)) {
					wwwAuthHeader.set(s1);
				}
			}
			
			
			@Override
			public void setContentType(String s) {
				contentType.set(s);
			}
			
			
			@Override
			public PrintWriter getWriter() throws IOException {
				return new PrintWriter(body);
			}
		};
		
		
		assertFalse(validator.validateBearerAccessToken(request, response));
		
		assertEquals(401, statusCode.get());
		assertEquals("Bearer error=\"invalid_token\", error_description=\"Invalid access token\"", wwwAuthHeader.get());
		assertEquals("application/json", contentType.get());
		JSONObject errorObject = (JSONObject)JSONValue.parse(body.toString());
		assertEquals("invalid_token", errorObject.get("error"));
		assertEquals("Unauthorized: Invalid Bearer access token", errorObject.get("error_description"));
		assertEquals(2, errorObject.size());
	}
	
	
	public void testBadToken_servletAPI_queryParameter()
		throws Exception {
		
		// Generate two random tokens
		BasicAccessTokenValidator validator = new BasicAccessTokenValidator(new BearerAccessToken());
		
		BearerAccessToken otherAccessToken = new BearerAccessToken();
		
		HttpServletRequest request = new MockHttpServletRequest() {
			@Override
			public String getParameter(String s) {
				return "access_token".equalsIgnoreCase(s) ? otherAccessToken.getValue() : null;
			}
		};
		
		final AtomicInteger statusCode = new AtomicInteger();
		
		final AtomicReference<String> wwwAuthHeader = new AtomicReference<>();
		
		final AtomicReference<String> contentType = new AtomicReference<>();
		
		final StringWriter body = new StringWriter();
		
		HttpServletResponse response = new MockHttpServletResponse() {
			
			@Override
			public void setStatus(int i) {
				statusCode.set(i);
			}
			
			
			@Override
			public void setHeader(String s, String s1) {
				if ("WWW-Authenticate".equalsIgnoreCase(s)) {
					wwwAuthHeader.set(s1);
				}
			}
			
			
			@Override
			public void setContentType(String s) {
				contentType.set(s);
			}
			
			
			@Override
			public PrintWriter getWriter() throws IOException {
				return new PrintWriter(body);
			}
		};
		
		
		assertFalse(validator.validateBearerAccessToken(request, response));
		
		assertEquals(401, statusCode.get());
		assertEquals("Bearer error=\"invalid_token\", error_description=\"Invalid access token\"", wwwAuthHeader.get());
		assertEquals("application/json", contentType.get());
		JSONObject errorObject = (JSONObject)JSONValue.parse(body.toString());
		assertEquals("invalid_token", errorObject.get("error"));
		assertEquals("Unauthorized: Invalid Bearer access token", errorObject.get("error_description"));
		assertEquals(2, errorObject.size());
	}
}
