package com.nimbusds.common.oauth2;


import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.BearerTokenError;
import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import junit.framework.TestCase;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;
import org.apache.commons.codec.binary.Hex;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


public class SHA256BasedAccessTokenValidatorTest extends TestCase {


	@Override
	public void tearDown() {
		System.clearProperty("env");
	}


	public void testDemoTokenConstants() {

		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", SHA256BasedAccessTokenValidator.DEMO_ACCESS_TOKEN.getValue());
		assertEquals(SHA256BasedAccessTokenValidator.DEMO_ACCESS_TOKEN_HASH, Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(SHA256BasedAccessTokenValidator.DEMO_ACCESS_TOKEN, null)));
	}


	public void testConstructor() {

		var accessToken = new BearerAccessToken();
		
		var validator = new SHA256BasedAccessTokenValidator(Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(accessToken, null)));

		assertFalse(validator.accessIsDisabled());
		
		assertEquals(1, validator.getNumberConfiguredTokens());
	}


	public void testConstructorVarArgs() {

		var validator = new SHA256BasedAccessTokenValidator(
			Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(new BearerAccessToken(), null)),
			Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(new BearerAccessToken(), null))
		);

		assertFalse(validator.accessIsDisabled());
		
		assertEquals(2, validator.getNumberConfiguredTokens());
	}
	
	
	public void testConstructorMainPlusAdditional() {
		
		var mainToken = new BearerAccessToken();
		String mainHash = Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(mainToken, null));
		List<BearerAccessToken> additionalTokens = new LinkedList<>();
		List<String> additionalHashes = new LinkedList<>();
		for (int i=0; i<5; i++) {
			var token = new BearerAccessToken();
			additionalTokens.add(token);
			additionalHashes.add(Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(token, null)));
		}
		
		var validator = new SHA256BasedAccessTokenValidator(mainHash, additionalHashes);
		
		assertFalse(validator.accessIsDisabled());
		
		assertEquals(1 + additionalTokens.size(), validator.getNumberConfiguredTokens());
		
		assertEquals(1 + additionalTokens.size(), validator.expectedTokenHashes.size());
		
		validator.validateBearerAccessToken(mainToken.toAuthorizationHeader());
		
		for (BearerAccessToken token: additionalTokens) {
			validator.validateBearerAccessToken(token.toAuthorizationHeader());
		}
		
		try {
			validator.validateBearerAccessToken(new BearerAccessToken().toAuthorizationHeader());
			fail();
		} catch (WebApplicationException e) {
			assertEquals("HTTP 401 Unauthorized", e.getMessage());
		}
	}
	
	
	public void testConstructorMainPlusAdditional_nullAdditional() {
		
		var mainToken = new BearerAccessToken();
		String mainHash = Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(mainToken, null));
		
		var validator = new SHA256BasedAccessTokenValidator(mainHash, null);
		
		assertFalse(validator.accessIsDisabled());
		
		assertEquals(1, validator.expectedTokenHashes.size());
		
		validator.validateBearerAccessToken(mainToken.toAuthorizationHeader());
	}
	
	
	public void testConstructorMainPlusAdditional_emptyAdditional() {
		
		var mainToken = new BearerAccessToken();
		String mainHash = Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(mainToken, null));
		
		var validator = new SHA256BasedAccessTokenValidator(mainHash, Collections.emptyList());
		
		assertFalse(validator.accessIsDisabled());
		
		assertEquals(1, validator.expectedTokenHashes.size());
		
		validator.validateBearerAccessToken(mainToken.toAuthorizationHeader());
	}
	
	
	public void testFromProperties_required_missing() {
		
		var properties = new Properties();
		
		try {
			SHA256BasedAccessTokenValidator.from(
				new PropertyRetriever(properties),
				"sessionStore.apiAccessTokenSHA256",
				true,
				null);
			fail();
		} catch (PropertyParseException e) {
			assertEquals("Missing property", e.getMessage());
			assertEquals("sessionStore.apiAccessTokenSHA256", e.getPropertyKey());
			assertNull(e.getPropertyValue());
		}
	}
	
	
	public void testFromProperties_optional_disabled() throws PropertyParseException {
		
		var properties = new Properties();
		
		SHA256BasedAccessTokenValidator validator = SHA256BasedAccessTokenValidator.from(
			new PropertyRetriever(properties),
			"sessionStore.apiAccessTokenSHA256",
			false,
			null);
		
		assertTrue(validator.accessIsDisabled());
	}
	
	
	public void testFromProperties_disabled_ignoreAdditional() throws PropertyParseException {
		
		var properties = new Properties();
		
		List<BearerAccessToken> additionalTokens = new LinkedList<>();
		for (int i=1; i<=5; i++) {
			var token = new BearerAccessToken();
			additionalTokens.add(token);
			properties.setProperty("sessionStore.apiAccessTokenSHA256." + i, Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(token, null)));
		}
		
		SHA256BasedAccessTokenValidator validator = SHA256BasedAccessTokenValidator.from(
			new PropertyRetriever(properties),
			"sessionStore.apiAccessTokenSHA256",
			false,
			"sessionStore.apiAccessTokenSHA256.");
		
		assertTrue(validator.accessIsDisabled());
	}
	
	
	public void testFromProperties_enabled() throws PropertyParseException {
		
		var mainToken = new BearerAccessToken();
		String mainHash = Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(mainToken, null));
		
		var properties = new Properties();
		properties.setProperty("sessionStore.apiAccessTokenSHA256", mainHash);
		
		SHA256BasedAccessTokenValidator validator = SHA256BasedAccessTokenValidator.from(
			new PropertyRetriever(properties),
			"sessionStore.apiAccessTokenSHA256",
			true,
			null);
		
		assertFalse(validator.accessIsDisabled());
		
		validator.validateBearerAccessToken(mainToken.toAuthorizationHeader());
	}


	public void testFromProperties_enabled_demoToken() throws PropertyParseException {

		var properties = new Properties();
		properties.setProperty("sessionStore.apiAccessTokenSHA256", SHA256BasedAccessTokenValidator.DEMO_ACCESS_TOKEN_HASH);

		SHA256BasedAccessTokenValidator validator = SHA256BasedAccessTokenValidator.from(
			new PropertyRetriever(properties),
			"sessionStore.apiAccessTokenSHA256",
			true,
			null);

		assertFalse(validator.accessIsDisabled());

		validator.validateBearerAccessToken(SHA256BasedAccessTokenValidator.DEMO_ACCESS_TOKEN.toAuthorizationHeader());
	}
	
	
	public void testFromProperties_enabled_withAdditional() throws PropertyParseException {
		
		var properties = new Properties();
		
		var mainToken = new BearerAccessToken();
		String mainHash = Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(mainToken, null));
		properties.setProperty("sessionStore.apiAccessTokenSHA256", mainHash);
		
		List<BearerAccessToken> additionalTokens = new LinkedList<>();
		for (int i=1; i<=5; i++) {
			var token = new BearerAccessToken();
			additionalTokens.add(token);
			properties.setProperty("sessionStore.apiAccessTokenSHA256." + i, Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(token, null)));
		}
		
		SHA256BasedAccessTokenValidator validator = SHA256BasedAccessTokenValidator.from(
			 new PropertyRetriever(properties),
			"sessionStore.apiAccessTokenSHA256",
			true,
			"sessionStore.apiAccessTokenSHA256.");
		
		assertFalse(validator.accessIsDisabled());
		
		validator.validateBearerAccessToken(mainToken.toAuthorizationHeader());
		
		for (BearerAccessToken token: additionalTokens) {
			validator.validateBearerAccessToken(token.toAuthorizationHeader());
		}
	}


	public void testFromProperties_enabled_withAdditionalDemoToken() throws PropertyParseException {

		var properties = new Properties();

		var mainToken = new BearerAccessToken();
		String mainHash = Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(mainToken, null));
		properties.setProperty("sessionStore.apiAccessTokenSHA256", mainHash);
		properties.setProperty("sessionStore.apiAccessTokenSHA256.1", SHA256BasedAccessTokenValidator.DEMO_ACCESS_TOKEN_HASH);

		SHA256BasedAccessTokenValidator validator = SHA256BasedAccessTokenValidator.from(
			 new PropertyRetriever(properties),
			"sessionStore.apiAccessTokenSHA256",
			true,
			"sessionStore.apiAccessTokenSHA256.");

		assertFalse(validator.accessIsDisabled());

		validator.validateBearerAccessToken(mainToken.toAuthorizationHeader());
		validator.validateBearerAccessToken(SHA256BasedAccessTokenValidator.DEMO_ACCESS_TOKEN.toAuthorizationHeader());
	}


	public void testFromProperties_prodEnv_rejectDemoToken()  {

		System.setProperty("env", "prod");

		var properties = new Properties();
		properties.setProperty("sessionStore.apiAccessTokenSHA256", SHA256BasedAccessTokenValidator.DEMO_ACCESS_TOKEN_HASH);

                try {
                        SHA256BasedAccessTokenValidator.from(
                                new PropertyRetriever(properties),
                                "sessionStore.apiAccessTokenSHA256",
                                true,
                                null);
			fail();
                } catch (PropertyParseException e) {
                        assertEquals("The web API is configured with the demo access token, generate a new token for production use", e.getMessage());
                }
        }


	public void testFromProperties_prodEnv_rejectAdditionalDemoToken()  {

		System.setProperty("env", "prod");

		var properties = new Properties();
		properties.setProperty("sessionStore.apiAccessTokenSHA256", Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(new BearerAccessToken(), null)));
		properties.setProperty("sessionStore.apiAccessTokenSHA256.1", SHA256BasedAccessTokenValidator.DEMO_ACCESS_TOKEN_HASH);

                try {
                        SHA256BasedAccessTokenValidator.from(
                                new PropertyRetriever(properties),
                                "sessionStore.apiAccessTokenSHA256",
                                true,
                                "sessionStore.apiAccessTokenSHA256.");
			fail();
                } catch (PropertyParseException e) {
                        assertEquals("The web API is configured with the demo access token, generate a new token for production use", e.getMessage());
                }
        }
	
	
	public void testConstructorMainPlusAdditional_disabled() {
		
		var validator = new SHA256BasedAccessTokenValidator(null, Collections.emptyList());
		
		assertTrue(validator.accessIsDisabled());
		
		assertEquals(0, validator.expectedTokenHashes.size());
		
		try {
			validator.validateBearerAccessToken(new BearerAccessToken().toAuthorizationHeader());
			fail();
		} catch (WebApplicationException e) {
			assertEquals("HTTP 403 Forbidden", e.getMessage());
		}
	}


	public void testConstructorNull() {

		var validator = new SHA256BasedAccessTokenValidator((String) null);
		
		assertTrue(validator.accessIsDisabled());
	}


	public void testConstructorNullVarArgs() {

		var validator = new SHA256BasedAccessTokenValidator((String) null, (String) null);
		
		assertTrue(validator.accessIsDisabled());
	}


	public void testPass() {

		var accessToken = new BearerAccessToken();

		var validator = new SHA256BasedAccessTokenValidator(Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(accessToken, null)));

		validator.validateBearerAccessToken(accessToken.toAuthorizationHeader());
	}


	public void testPassVarArgs() {

		var t1 = new BearerAccessToken();
		var t2 = new BearerAccessToken();

		var validator = new SHA256BasedAccessTokenValidator(
			Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(t1, null)),
			Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(t2, null))
		);

		validator.validateBearerAccessToken(t1.toAuthorizationHeader());
		validator.validateBearerAccessToken(t2.toAuthorizationHeader());
	}
	
	
	public void testPass_servletAPI_authzHeader()
		throws IOException {
		
		var accessToken = new BearerAccessToken();
		
		var validator = new SHA256BasedAccessTokenValidator(Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(accessToken, null)));
		
		HttpServletRequest request = new MockHttpServletRequest() {
			@Override
			public String getHeader(String s) {
				
				if ("Authorization".equalsIgnoreCase(s)) {
					return accessToken.toAuthorizationHeader();
				} else {
					return null;
				}
			}
		};
		
		assertTrue(validator.validateBearerAccessToken(request, new MockHttpServletResponse()));
	}
	
	
	public void testPassVarArgs_servletAPI_authzHeader()
		throws IOException {
		
		var t1 = new BearerAccessToken();
		var t2 = new BearerAccessToken();
		
		var validator = new SHA256BasedAccessTokenValidator(
			Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(t1, null)),
			Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(t2, null))
		);
		
		HttpServletRequest request = new MockHttpServletRequest() {
			@Override
			public String getHeader(String s) {
				
				if ("Authorization".equalsIgnoreCase(s)) {
					return t1.toAuthorizationHeader();
				} else {
					return null;
				}
			}
		};
		
		assertTrue(validator.validateBearerAccessToken(request, new MockHttpServletResponse()));
		
		request = new MockHttpServletRequest() {
			@Override
			public String getHeader(String s) {
				
				if ("Authorization".equalsIgnoreCase(s)) {
					return t2.toAuthorizationHeader();
				} else {
					return null;
				}
			}
		};
		
		assertTrue(validator.validateBearerAccessToken(request, new MockHttpServletResponse()));
	}
	
	
	public void testPass_servletAPI_queryParameter()
		throws IOException {
		
		var accessToken = new BearerAccessToken();
		
		var validator = new SHA256BasedAccessTokenValidator(Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(accessToken, null)));
		
		HttpServletRequest request = new MockHttpServletRequest() {
			@Override
			public String getParameter(String s) {
				return s.equalsIgnoreCase("access_token") ? accessToken.getValue() : null;
			}
		};
		
		assertTrue(validator.validateBearerAccessToken(request, new MockHttpServletResponse()));
	}
	
	
	public void testPassVarArgs_servletAPI_queryParameter()
		throws IOException {
		
		var t1 = new BearerAccessToken();
		var t2 = new BearerAccessToken();
		
		var validator = new SHA256BasedAccessTokenValidator(
			Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(t1, null)),
			Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(t2, null))
		);
		
		HttpServletRequest request = new MockHttpServletRequest() {
			@Override
			public String getParameter(String s) {
				return s.equalsIgnoreCase("access_token") ? t1.getValue() : null;
			}
		};
		
		assertTrue(validator.validateBearerAccessToken(request, new MockHttpServletResponse()));
		
		request = new MockHttpServletRequest() {
			@Override
			public String getParameter(String s) {
				return s.equalsIgnoreCase("access_token") ? t2.getValue() : null;
			}
		};
		
		assertTrue(validator.validateBearerAccessToken(request, new MockHttpServletResponse()));
	}


	public void testWebAPIDisabled() {

		var validator = new SHA256BasedAccessTokenValidator((String) null);

		var accessToken = new BearerAccessToken();

		try {
			validator.validateBearerAccessToken(accessToken.toAuthorizationHeader());

			fail();

		} catch (WebApplicationException e) {
			// ok
			assertEquals(Response.Status.FORBIDDEN.getStatusCode(), e.getResponse().getStatus());

			assertNull(e.getResponse().getHeaderString("WWW-Authenticate"));

			assertEquals("application/json", e.getResponse().getMetadata().getFirst("content-type").toString());

			JSONObject jsonObject = (JSONObject)JSONValue.parse((String)e.getResponse().getEntity());
			assertEquals("web_api_disabled", (String)jsonObject.get("error"));
		}
	}


	public void testAccessDisabled() {

		var validator = new SHA256BasedAccessTokenValidator((String) null);

		try {
			validator.validateBearerAccessToken(null);

			fail();

		} catch (WebApplicationException e) {
			// ok
			assertEquals(SHA256BasedAccessTokenValidator.WEB_API_DISABLED.toWebAppException().getResponse().getStatus(), e.getResponse().getStatus());

			assertNull(e.getResponse().getHeaderString("WWW-Authenticate"));

			assertEquals("application/json", e.getResponse().getMetadata().getFirst("content-type").toString());

			JSONObject jsonObject = (JSONObject)JSONValue.parse((String)e.getResponse().getEntity());
			assertEquals("web_api_disabled", (String)jsonObject.get("error"));
		}
	}


	public void testAccessDisabled_invalidHeader() {

		var validator = new SHA256BasedAccessTokenValidator((String) null);

		try {
			validator.validateBearerAccessToken("Authorization ");

			fail();

		} catch (WebApplicationException e) {
			// ok
			assertEquals(SHA256BasedAccessTokenValidator.WEB_API_DISABLED.toWebAppException().getResponse().getStatus(), e.getResponse().getStatus());
			
			assertNull(e.getResponse().getHeaderString("WWW-Authenticate"));
			
			assertEquals("application/json", e.getResponse().getMetadata().getFirst("content-type").toString());
			
			JSONObject jsonObject = (JSONObject)JSONValue.parse((String)e.getResponse().getEntity());
			assertEquals("web_api_disabled", (String)jsonObject.get("error"));
		}
	}


	public void testBadToken_authzHeader() {

		// Generate two random tokens
		var validator = new SHA256BasedAccessTokenValidator(Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(new BearerAccessToken(), null)));

		var otherAccessToken = new BearerAccessToken();

		try {
			validator.validateBearerAccessToken(otherAccessToken.toAuthorizationHeader());

			fail();

		} catch (WebApplicationException e) {
			// ok
			assertEquals(BearerTokenError.INVALID_TOKEN.getHTTPStatusCode(), e.getResponse().getStatus());

			assertEquals(BearerTokenError.INVALID_TOKEN.toWWWAuthenticateHeader(), e.getResponse().getHeaderString("WWW-Authenticate"));

			assertEquals("application/json", e.getResponse().getMetadata().getFirst("content-type").toString());

			JSONObject jsonObject = (JSONObject)JSONValue.parse((String)e.getResponse().getEntity());
			assertEquals("invalid_token", (String)jsonObject.get("error"));
		}
	}


	public void testTokenTooShort_authzHeader() {

		String tokenValue = "";
		for (int i=0; i < 32 - 1; i++) {
			tokenValue += "a";
		}
		
		var token = new BearerAccessToken(tokenValue);
		
		var validator = new SHA256BasedAccessTokenValidator(Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(token, null)));

		try {
			validator.validateBearerAccessToken(token.toAuthorizationHeader());

			fail();

		} catch (WebApplicationException e) {
			// ok
			assertEquals(BearerTokenError.INVALID_TOKEN.getHTTPStatusCode(), e.getResponse().getStatus());

			assertEquals(BearerTokenError.INVALID_TOKEN.toWWWAuthenticateHeader(), e.getResponse().getHeaderString("WWW-Authenticate"));

			assertEquals("application/json", e.getResponse().getMetadata().getFirst("content-type").toString());

			JSONObject jsonObject = (JSONObject)JSONValue.parse((String)e.getResponse().getEntity());
			assertEquals("invalid_token", (String)jsonObject.get("error"));
		}
	}
	
	
	public void testBadToken_servletAPI_authzHeader()
		throws Exception {
		
		// Generate two random tokens
		var validator = new SHA256BasedAccessTokenValidator(Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(new BearerAccessToken(), null)));
		
		var otherAccessToken = new BearerAccessToken();
		
		HttpServletRequest request = new MockHttpServletRequest() {
			@Override
			public String getHeader(String s) {
				
				if ("Authorization".equalsIgnoreCase(s)) {
					return otherAccessToken.toAuthorizationHeader();
				} else {
					return null;
				}
			}
		};
		
		final var statusCode = new AtomicInteger();
		
		final var wwwAuthHeader = new AtomicReference<>();
		
		final var contentType = new AtomicReference<>();
		
		final var body = new StringWriter();
		
		HttpServletResponse response = new MockHttpServletResponse() {
			
			@Override
			public void setStatus(int i) {
				statusCode.set(i);
			}
			
			
			@Override
			public void setHeader(String s, String s1) {
				if ("WWW-Authenticate".equalsIgnoreCase(s)) {
					wwwAuthHeader.set(s1);
				}
			}
			
			
			@Override
			public void setContentType(String s) {
				contentType.set(s);
			}
			
			
			@Override
			public PrintWriter getWriter() throws IOException {
				return new PrintWriter(body);
			}
		};
		
		
		assertFalse(validator.validateBearerAccessToken(request, response));
		
		assertEquals(401, statusCode.get());
		assertEquals("Bearer error=\"invalid_token\", error_description=\"Invalid access token\"", wwwAuthHeader.get());
		assertEquals("application/json", contentType.get());
		JSONObject errorObject = (JSONObject)JSONValue.parse(body.toString());
		assertEquals("invalid_token", errorObject.get("error"));
		assertEquals("Unauthorized: Invalid Bearer access token", errorObject.get("error_description"));
		assertEquals(2, errorObject.size());
	}
	
	
	public void testTokenTooShort_servletAPI_authzHeader()
		throws Exception {

                var token = new BearerAccessToken("a".repeat(32 - 1));
		
		var validator = new SHA256BasedAccessTokenValidator(Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(token, null)));
		
		HttpServletRequest request = new MockHttpServletRequest() {
			@Override
			public String getHeader(String s) {
				
				if ("Authorization".equalsIgnoreCase(s)) {
					return token.toAuthorizationHeader();
				} else {
					return null;
				}
			}
		};
		
		final var statusCode = new AtomicInteger();
		
		final var wwwAuthHeader = new AtomicReference<>();
		
		final var contentType = new AtomicReference<>();
		
		final StringWriter body = new StringWriter();
		
		HttpServletResponse response = new MockHttpServletResponse() {
			
			@Override
			public void setStatus(int i) {
				statusCode.set(i);
			}
			
			
			@Override
			public void setHeader(String s, String s1) {
				if ("WWW-Authenticate".equalsIgnoreCase(s)) {
					wwwAuthHeader.set(s1);
				}
			}
			
			
			@Override
			public void setContentType(String s) {
				contentType.set(s);
			}
			
			
			@Override
			public PrintWriter getWriter() {
				return new PrintWriter(body);
			}
		};
		
		
		assertFalse(validator.validateBearerAccessToken(request, response));
		
		assertEquals(401, statusCode.get());
		assertEquals("Bearer error=\"invalid_token\", error_description=\"Invalid access token\"", wwwAuthHeader.get());
		assertEquals("application/json", contentType.get());
		var errorObject = (JSONObject)JSONValue.parse(body.toString());
		assertEquals("invalid_token", errorObject.get("error"));
		assertEquals("Unauthorized: Invalid Bearer access token", errorObject.get("error_description"));
		assertEquals(2, errorObject.size());
	}
	
	
	public void testBadToken_servletAPI_queryParameter()
		throws Exception {
		
		// Generate two random tokens
		var validator = new SHA256BasedAccessTokenValidator(Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(new BearerAccessToken(), null)));
		
		var otherAccessToken = new BearerAccessToken();
		
		HttpServletRequest request = new MockHttpServletRequest() {
			@Override
			public String getParameter(String s) {
				return "access_token".equalsIgnoreCase(s) ? otherAccessToken.getValue() : null;
			}
		};
		
		final var statusCode = new AtomicInteger();
		
		final var wwwAuthHeader = new AtomicReference<>();
		
		final var contentType = new AtomicReference<>();
		
		final var body = new StringWriter();
		
		HttpServletResponse response = new MockHttpServletResponse() {
			
			@Override
			public void setStatus(int i) {
				statusCode.set(i);
			}
			
			
			@Override
			public void setHeader(String s, String s1) {
				if ("WWW-Authenticate".equalsIgnoreCase(s)) {
					wwwAuthHeader.set(s1);
				}
			}
			
			
			@Override
			public void setContentType(String s) {
				contentType.set(s);
			}
			
			
			@Override
			public PrintWriter getWriter() {
				return new PrintWriter(body);
			}
		};
		
		
		assertFalse(validator.validateBearerAccessToken(request, response));
		
		assertEquals(401, statusCode.get());
		assertEquals("Bearer error=\"invalid_token\", error_description=\"Invalid access token\"", wwwAuthHeader.get());
		assertEquals("application/json", contentType.get());
		JSONObject errorObject = (JSONObject)JSONValue.parse(body.toString());
		assertEquals("invalid_token", errorObject.get("error"));
		assertEquals("Unauthorized: Invalid Bearer access token", errorObject.get("error_description"));
		assertEquals(2, errorObject.size());
	}
	
	
	public void testConstructor_invalidHex() {
		
		try {
			new SHA256BasedAccessTokenValidator("---");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid hex for access token SHA-256: ---", e.getMessage());
		}
	}
	
	
	public void testConstructor_varargs_invalidHex() {
		
		try {
			new SHA256BasedAccessTokenValidator("---", "+++");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid hex for access token SHA-256: ---", e.getMessage());
		}
	}
	
	
	public void testConstructor_mainPlusAdditional_invalidHex() {
		
		try {
			new SHA256BasedAccessTokenValidator("---", Collections.emptyList());
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid hex for access token SHA-256: ---", e.getMessage());
		}
		
		try {
			new SHA256BasedAccessTokenValidator(Hex.encodeHexString(MasterAccessTokenValidator.computeSHA256(new BearerAccessToken(), null)), Arrays.asList("---"));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid hex for access token SHA-256: ---", e.getMessage());
		}
	}
}
