package com.nimbusds.common.oauth2;


import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.RefreshToken;
import junit.framework.TestCase;


public class TokenAbbreviatorTest extends TestCase {
	
	
	public void testAbbreviate() {
		
		assertEquals("null", TokenAbbreviator.abbreviate(null));
		assertEquals("123456789...", TokenAbbreviator.abbreviate(new BearerAccessToken("1234567890123")));
		assertEquals("123456789...", TokenAbbreviator.abbreviate(new RefreshToken("1234567890123")));
	}
}
