package com.nimbusds.common.log;


import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Identifier;
import junit.framework.TestCase;


public class LogSanitizerTest extends TestCase {
	
	
	public void testSanitize_null() {
		
		assertNull(LogSanitizer.sanitize((String)null, 10));
		assertNull(LogSanitizer.sanitize((Identifier) null, 10));
	}


	public void testSanitize_trim_to_10() {
		
		Identifier clientID = new ClientID("1234567890ABC");
		
		String expected = "1234567...";
		assertEquals(10, expected.length());
		assertEquals(expected, LogSanitizer.sanitize(clientID, 10));
	}


	public void testSanitize_noTrim() {
		
		Identifier clientID = new ClientID("123");
		
		assertEquals(clientID.getValue(), LogSanitizer.sanitize(clientID, 10));
	}


	public void testSanitize_trim_to_20() {
		
		Identifier clientID = new ClientID("1234567890ABCDEFGIJKLMN");
		assertEquals(23 ,clientID.getValue().length());
		
		String expected = "1234567890ABCDEFG...";
		assertEquals(20, expected.length());
		assertEquals(expected, LogSanitizer.sanitize(clientID, 20));
	}


	public void testSanitize_defaultTrim() {
		
		Identifier clientID = new ClientID("1234567890123456789012345678901234567890");
		assertEquals(40 ,clientID.getValue().length());
		
		String expected = "123456789012345678901234567...";
		assertEquals(30, expected.length());
		assertEquals(expected, LogSanitizer.sanitize(clientID));
	}
	
	
	public void testSanitize_newLine() {
		
		assertEquals("123\\n", LogSanitizer.sanitize(new ClientID("123\n")));
	}
	
	
	public void testSanitize_lineFeed() {
		
		assertEquals("123\\r", LogSanitizer.sanitize(new ClientID("123\r")));
	}
	
	
	public void testSanitize_tab() {
		
		assertEquals("123\\t", LogSanitizer.sanitize(new ClientID("123\t")));
	}
	
	
	public void testSanitize_metaChars() {
		
		assertEquals("123\\\\", LogSanitizer.sanitize(new ClientID("123\\")));
		assertEquals("123\\{", LogSanitizer.sanitize(new ClientID("123{")));
		assertEquals("123\\}", LogSanitizer.sanitize(new ClientID("123}")));
		assertEquals("123\\[", LogSanitizer.sanitize(new ClientID("123[")));
		assertEquals("123\\]", LogSanitizer.sanitize(new ClientID("123]")));
		assertEquals("123\\)", LogSanitizer.sanitize(new ClientID("123)")));
		assertEquals("123\\(", LogSanitizer.sanitize(new ClientID("123(")));
		assertEquals("123\\|", LogSanitizer.sanitize(new ClientID("123|")));
		assertEquals("123\\&", LogSanitizer.sanitize(new ClientID("123&")));
		assertEquals("123\\%", LogSanitizer.sanitize(new ClientID("123%")));
	}
}
