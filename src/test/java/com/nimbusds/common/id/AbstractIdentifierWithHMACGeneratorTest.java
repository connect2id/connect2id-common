package com.nimbusds.common.id;


import com.nimbusds.jose.util.Base64URL;
import junit.framework.TestCase;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;


public class AbstractIdentifierWithHMACGeneratorTest extends TestCase {
	

	static class MySIDCodec extends AbstractIdentifierWithHMACGenerator<SID> {


		MySIDCodec(final SecretKey hmacKey) {
			super(hmacKey);
		}


		@Override
		public SID generate() {
			return new SID(new IdentifierWithHMAC(getHMACKey()).toString());
		}


		@Override
		public SID applyHMAC(final Base64URL value) {

			return new SID(new IdentifierWithHMAC(value.decode(), getHMACKey()).toString());
		}
	}


	public void testLifeCycle()
		throws Exception {

		KeyGenerator keyGen = KeyGenerator.getInstance("HmacSha256");
		SecretKey hmacKey = keyGen.generateKey();

		assertEquals(256 / 8, hmacKey.getEncoded().length);

		MySIDCodec codec = new MySIDCodec(hmacKey);

		SID sid = codec.generate();

		System.out.println("SID: " + sid);

		assertEquals(16, new Base64URL(sid.toString().split("\\.")[0]).decode().length);
		assertEquals(16, new Base64URL(sid.toString().split("\\.")[1]).decode().length);
		assertEquals(2, sid.toString().split("\\.").length);

		Base64URL value = codec.extractValue(sid);
		assertEquals(new Base64URL(sid.toString().split("\\.")[0]), value);

		value = codec.validateAndExtractValue(sid);
		assertEquals(new Base64URL(sid.toString().split("\\.")[0]), value);

		assertEquals(sid, codec.applyHMAC(value));
	}
}
