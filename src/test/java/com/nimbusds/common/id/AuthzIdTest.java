package com.nimbusds.common.id;


import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.LDAPException;
import junit.framework.TestCase;

import java.text.ParseException;


/**
 * Tests the AuthzId class.
 */
public class AuthzIdTest extends TestCase {


	public void testParseDNForm()
		throws LDAPException {
	
		String s = "dn: uid=alice,ou=people,dc=wonderland,dc=net";
		
		AuthzId authzId = null;
		
		try {
			authzId = AuthzId.parse(s);
			
		} catch (ParseException e) {
		
			fail(e.getMessage());
		}
		
		assertEquals(new DN("uid=alice,ou=people,dc=wonderland,dc=net"), authzId.getDN());
		assertNull(authzId.getUsername());
		assertEquals(AuthzId.IdentityType.DN, authzId.getIdentityType());
	}
	
	
	public void testParseUsernameForm() {
	
		String s = "u: alice";
		
		AuthzId authzId = null;
		
		try {
			authzId = AuthzId.parse(s);
			
		} catch (ParseException e) {
		
			fail(e.getMessage());
		}
		
		assertEquals(new Username("alice"), authzId.getUsername());
		assertNull(authzId.getDN());
		assertEquals(AuthzId.IdentityType.USERNAME, authzId.getIdentityType());
	}
	
	
	public void testDNConstructor()
		throws LDAPException {
	
		AuthzId authzId = new AuthzId(new DN("uid=alice,ou=people,dc=wonderland,dc=net"));
		
		// Also tests DN normalisation
		assertEquals(new DN("UID=alice,OU=people,DC=wonderland,DC=net"), authzId.getDN());
		assertNull(authzId.getUsername());
		assertEquals(AuthzId.IdentityType.DN, authzId.getIdentityType());
		
		System.out.println(authzId);
		assertEquals("dn:uid=alice,ou=people,dc=wonderland,dc=net", authzId.toString());
	}
	
	
	public void testAnonymousDNConstructor()
		throws LDAPException {
	
		AuthzId authzId = new AuthzId(DN.NULL_DN);
		
		// Also tests DN normalisation
		assertEquals(DN.NULL_DN, authzId.getDN());
		assertNull(authzId.getUsername());
		assertEquals(AuthzId.IdentityType.DN, authzId.getIdentityType());
		
		System.out.println(authzId);
		assertEquals("dn:", authzId.toString());
	}

	
	public void testUsernameConstructor() {
	
		AuthzId authzId = new AuthzId(new Username("alice"));
		assertEquals(new Username("alice"), authzId.getUsername());
		assertNull(authzId.getDN());
		assertEquals(AuthzId.IdentityType.USERNAME, authzId.getIdentityType());
		
		System.out.println(authzId);
		assertEquals("u:alice", authzId.toString());
	}


	public void testEquals() {

		assertTrue(new AuthzId(new Username("alice")).equals(new AuthzId(new Username("alice"))));
	}


	public void testEqualsNull() {

		assertFalse(new AuthzId(new Username("alice")).equals(null));
	}
}
