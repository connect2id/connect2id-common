package com.nimbusds.common.id;


import com.nimbusds.jose.util.Base64URL;
import junit.framework.TestCase;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;


public class IdentifierWithHMACTest extends TestCase {


	private static SecretKey generateHMACKey() {

		try {
			KeyGenerator keyGen = KeyGenerator.getInstance("HmacSha256");
			return keyGen.generateKey();
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	

	public void testDefaultByteLength() {

		// 128 bits
		assertEquals(16, IdentifierWithHMAC.DEFAULT_BYTE_LENGTH);
	}


	public void testValueConstructor()
		throws InvalidIdentifierException {

		byte[] value = new byte[8];
		new SecureRandom().nextBytes(value);
		SecretKey hmacKey = generateHMACKey();
		IdentifierWithHMAC id = new IdentifierWithHMAC(value, hmacKey);
		System.out.println(id);
		assertEquals(id, IdentifierWithHMAC.parseAndValidate(id.toString(), hmacKey));
	}


	public void testGenerateLifeCycle()
		throws InvalidIdentifierException {

		SecretKey hmacKey = generateHMACKey();

		IdentifierWithHMAC id = new IdentifierWithHMAC(hmacKey);

		System.out.println("ID: " + id);

		String[] parts = id.toString().split("\\.");
		assertEquals(16, Base64.decodeBase64(parts[0]).length);
		assertEquals(16, Base64.decodeBase64(parts[1]).length);
		assertEquals(2, parts.length);
		
		Base64URL[] b64parts = IdentifierWithHMAC.parse(id.toString());
		assertEquals(parts[0], b64parts[0].toString());
		assertEquals(parts[1], b64parts[1].toString());
		assertEquals(2, b64parts.length);
		
		b64parts = IdentifierWithHMAC.parseUnchecked(id.toString());
		assertEquals(parts[0], b64parts[0].toString());
		assertEquals(parts[1], b64parts[1].toString());
		assertEquals(2, b64parts.length);

		IdentifierWithHMAC validatedID = IdentifierWithHMAC.parseAndValidate(id.toString(), hmacKey);

		assertEquals(id.toString(), validatedID.toString());
	}


	public void testParseEmpty() {

		try {
			IdentifierWithHMAC.parse("");
			fail();
		} catch (InvalidIdentifierException e) {
			assertEquals("Illegal identifier with HMAC format", e.getMessage());
		}

		try {
			IdentifierWithHMAC.parseUnchecked("");
			fail();
		} catch (RuntimeException e) {
			assertEquals("Illegal identifier with HMAC format", e.getMessage());
		}

		try {
			IdentifierWithHMAC.parseAndValidate("", generateHMACKey());
			fail();
		} catch (InvalidIdentifierException e) {
			assertEquals("Illegal identifier with HMAC format", e.getMessage());
		}
	}


	public void testParseMissingDelimiters() {

		try {
			IdentifierWithHMAC.parse("abc");
			fail();
		} catch (InvalidIdentifierException e) {
			assertEquals("Illegal identifier with HMAC format", e.getMessage());
		}

		try {
			IdentifierWithHMAC.parseUnchecked("abc");
			fail();
		} catch (RuntimeException e) {
			assertEquals("Illegal identifier with HMAC format", e.getMessage());
		}

		try {
			IdentifierWithHMAC.parseAndValidate("abc", generateHMACKey());
			fail();
		} catch (InvalidIdentifierException e) {
			assertEquals("Illegal identifier with HMAC format", e.getMessage());
		}
	}


	public void testParseTooManyDelimiters() {

		try {
			IdentifierWithHMAC.parse("abc.def.ghi");
			fail();
		} catch (InvalidIdentifierException e) {
			assertEquals("Illegal identifier with HMAC format", e.getMessage());
		}

		try {
			IdentifierWithHMAC.parseUnchecked("abc.def.ghi");
			fail();
		} catch (RuntimeException e) {
			assertEquals("Illegal identifier with HMAC format", e.getMessage());
		}

		try {
			IdentifierWithHMAC.parseAndValidate("abc.def.ghi", generateHMACKey());
			fail();
		} catch (InvalidIdentifierException e) {
			assertEquals("Illegal identifier with HMAC format", e.getMessage());
		}
	}
	
	
	public void testParseIllegalCharsInHMAC() {
		
		try {
			IdentifierWithHMAC.parseAndValidate("bz7rS8lgY1xNI14m-wNIaQ.%%%", generateHMACKey());
			fail();
		} catch (InvalidIdentifierException e) {
			assertEquals("Illegal HMAC value", e.getMessage());
		}
	}


	public void testInvalidHMAC() {

		IdentifierWithHMAC id = new IdentifierWithHMAC(generateHMACKey());

		SecretKey otherKey = generateHMACKey();

		try {
			IdentifierWithHMAC.parseAndValidate(id.toString(), otherKey);
			fail();
		} catch (InvalidIdentifierException e) {
			assertEquals("Invalid HMAC", e.getMessage());
		}
	}


	public void testKeyLength() {

		assertEquals(32, generateHMACKey().getEncoded().length);
	}


	public void testMinKeyLength()
		throws InvalidIdentifierException {

		byte[] keyBytes = new byte[256 / 8];
		new SecureRandom().nextBytes(keyBytes);
		SecretKey hmacKey = new SecretKeySpec(keyBytes, "HmacSha256");
		IdentifierWithHMAC id = new IdentifierWithHMAC(hmacKey);
		assertEquals(id, IdentifierWithHMAC.parseAndValidate(id.toString(), hmacKey));
	}


	public void testKeyTooShort() {

		byte[] keyBytes = new byte[256 / 8 - 1];
		new SecureRandom().nextBytes(keyBytes);
		SecretKey hmacKey = new SecretKeySpec(keyBytes, "HmacSha256");
		try {
			new IdentifierWithHMAC(hmacKey);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The HMAC key must be at least 256 bits long", e.getMessage());
		}
	}
}
