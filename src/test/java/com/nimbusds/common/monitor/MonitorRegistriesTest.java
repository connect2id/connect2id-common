package com.nimbusds.common.monitor;


import com.codahale.metrics.CachedGauge;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Metric;
import com.codahale.metrics.health.HealthCheck;
import com.nimbusds.common.config.MonitorConfiguration;
import junit.framework.TestCase;
import org.junit.After;

import java.util.concurrent.atomic.AtomicInteger;


/**
 * Tests the shared metrics registries.
 */
public class MonitorRegistriesTest extends TestCase {
	
	
	@After
	public void tearDown() throws Exception {
		super.tearDown();
		
		MonitorRegistries.setEntryCountCacheTimeout(MonitorConfiguration.DEFAULT_ENTRY_COUNT_CACHE_TIMEOUT);
	}


	public void testRetrieval() {

		assertNotNull(MonitorRegistries.getMetricRegistry());

		assertNotNull(MonitorRegistries.getHealthCheckRegistry());
	}


	public void testMetricRegistrySameObject() {

		MonitorRegistries.getMetricRegistry().register("test-gauge", (Gauge<Integer>) () -> 100);

		assertEquals(100, MonitorRegistries.getMetricRegistry().getGauges().get("test-gauge").getValue());
	}


	public void testHealthCheckRegistrySameObject() {

		MonitorRegistries.getHealthCheckRegistry().register("test-check", new HealthCheck() {
			@Override
			protected Result check() {
				return Result.healthy("ок");
			}
		});

		assertEquals(HealthCheck.Result.healthy("ок").getMessage(), MonitorRegistries.getHealthCheckRegistry().runHealthCheck("test-check").getMessage());
		assertEquals(HealthCheck.Result.healthy("ок").isHealthy(), MonitorRegistries.getHealthCheckRegistry().runHealthCheck("test-check").isHealthy());
	}


	public void testRegisterMetricSetNull() {

		MonitorRegistries.register(null);
	}


	public void testRegisterMetricNull() {

		MonitorRegistries.register(null, (Metric) null);
		MonitorRegistries.register("name", (Metric) null);
		MonitorRegistries.register(null, (Gauge<Integer>) () -> 100);
	}


	public void testRegisterHealthCheckNull() {

		MonitorRegistries.register(null, (HealthCheck) null);
		MonitorRegistries.register("name", (HealthCheck) null);
		MonitorRegistries.register(null, new HealthCheck() {
			@Override
			protected Result check() {
				return Result.healthy();
			}
		});
	}


	public void testRegisterMetric() {

		MonitorRegistries.register("gauge-1", (Gauge<Integer>) () -> 100);

		assertTrue(MonitorRegistries.getMetricRegistry().getNames().contains("gauge-1"));
	}


	public void testUpdateMetric() {

		MonitorRegistries.register("gauge-2", (Gauge<Integer>) () -> 200);

		MonitorRegistries.register("gauge-2", (Gauge<Integer>) () -> 300);

		assertEquals(300, MonitorRegistries.getMetricRegistry().getGauges().get("gauge-2").getValue());
	}


	public void testRemoveMetric() {

		MonitorRegistries.register("gauge-2", (Gauge<Integer>) () -> 200);

		MonitorRegistries.register("gauge-2", (Metric)null);

		assertNull(MonitorRegistries.getMetricRegistry().getGauges().get("gauge-2"));
	}
	
	
	public void testRegisterEntryCountGauge_cacheTimeout() {
		
		assertEquals(MonitorConfiguration.DEFAULT_ENTRY_COUNT_CACHE_TIMEOUT, MonitorRegistries.getEntryCountCacheTimeout());
		
		MonitorRegistries.registerEntryCountGauge("cached-gauge", () -> 10);
		
		CachedGauge<Integer> cachedGauge = (CachedGauge<Integer>) MonitorRegistries.getMetricRegistry().getGauges().get("cached-gauge");
		
		assertEquals(10, cachedGauge.getValue().intValue());
	}
	
	
	public void testRegisterEntryCountGauge_noCaching() {
		
		MonitorRegistries.setEntryCountCacheTimeout(0L);
		assertEquals(0L, MonitorRegistries.getEntryCountCacheTimeout());
		
		AtomicInteger count = new AtomicInteger(1000);
		
		MonitorRegistries.registerEntryCountGauge("gauge-no-cache", count::get);
		
		Gauge<Integer> gauge = (Gauge<Integer>) MonitorRegistries.getMetricRegistry().getGauges().get("gauge-no-cache");
		
		assertEquals(1000, gauge.getValue().intValue());
		
		count.set(2000);
		assertEquals(2000, gauge.getValue().intValue());
	}
	
	
	public void testRegisterEntryCountGauge_readingDisabled() {
		
		MonitorRegistries.setEntryCountCacheTimeout(-1L);
		assertEquals(-1L, MonitorRegistries.getEntryCountCacheTimeout());
		
		AtomicInteger count = new AtomicInteger(1000);
		
		MonitorRegistries.registerEntryCountGauge("gauge-no-read", count::get);
		
		Gauge<Integer> gauge = (Gauge<Integer>) MonitorRegistries.getMetricRegistry().getGauges().get("gauge-no-read");
		
		assertEquals(-1, gauge.getValue().intValue());
		
		count.set(2000);
		assertEquals(-1, gauge.getValue().intValue());
	}


	public void testRegisterHealthCheck() {

		MonitorRegistries.register("check-1", new HealthCheck() {
			@Override
			protected Result check() {
				return Result.unhealthy("problem");
			}
		});

		assertEquals("problem", MonitorRegistries.getHealthCheckRegistry().runHealthCheck("check-1").getMessage());
	}


	public void testUpdateHealthCheck() {

		MonitorRegistries.register("check-2", new HealthCheck() {
			@Override
			protected Result check() {
				return Result.unhealthy("AAA");
			}
		});

		MonitorRegistries.register("check-2", new HealthCheck() {
			@Override
			protected Result check() {
				return Result.unhealthy("BBB");
			}
		});

		assertEquals("BBB", MonitorRegistries.getHealthCheckRegistry().runHealthCheck("check-2").getMessage());
	}


	public void testRemoveHealthCheck() {

		MonitorRegistries.register("check-3", new HealthCheck() {
			@Override
			protected Result check() {
				return Result.unhealthy("AAA");
			}
		});

		MonitorRegistries.register("check-3", (HealthCheck) null);

		assertFalse(MonitorRegistries.getHealthCheckRegistry().getNames().contains("check-3"));
	}
}
