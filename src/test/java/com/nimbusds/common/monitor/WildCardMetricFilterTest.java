package com.nimbusds.common.monitor;


import com.codahale.metrics.Metric;
import junit.framework.TestCase;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * Tests the wild card metric filter.
 */
public class WildCardMetricFilterTest extends TestCase {


	public void testMatchNone() {

		WildCardMetricFilter filter = new WildCardMetricFilter(null);
		assertTrue(filter.matchesNone());
		assertFalse(filter.matchesAny());
		assertFalse(filter.matches("abc", null));

		filter = new WildCardMetricFilter(Collections.<String>emptyList());
		assertTrue(filter.matchesNone());
		assertFalse(filter.matchesAny());
		assertFalse(filter.matches("abc", null));
	}


	public void testMatchAny() {

		WildCardMetricFilter filter = new WildCardMetricFilter(Collections.singletonList("*"));
		assertTrue(filter.matchesAny());
		assertFalse(filter.matchesNone());
		assertTrue(filter.matches("abc", null));

		filter = new WildCardMetricFilter(Collections.singletonList("  *  ")); // test trim
		assertTrue(filter.matchesAny());
		assertFalse(filter.matchesNone());
		assertTrue(filter.matches("abc", null));

		filter = new WildCardMetricFilter(Arrays.asList("*", "b", "c")); // test precedence
		assertTrue(filter.matchesAny());
		assertFalse(filter.matchesNone());
		assertTrue(filter.matches("abc", null));

		filter = new WildCardMetricFilter(Arrays.asList(null, "*", "b", "c")); // test null safe
		assertTrue(filter.matchesAny());
		assertFalse(filter.matchesNone());
		assertTrue(filter.matches("abc", null));
	}


	public void testNeverMatchNullName() {

		Metric m = null; // ignored

		List<String> whitelist = Arrays.asList(
			"op.success",
			"op.failure",
			"rp.success",
			"rp.failure"
		);

		WildCardMetricFilter filter = new WildCardMetricFilter(whitelist);
		assertFalse(filter.matchesNone());
		assertFalse(filter.matchesAny());

		assertEquals(whitelist, filter.getWhiteList());

		assertFalse(filter.matches(null, m));
	}


	public void testNeverMatchEmptyName() {

		Metric m = null; // ignored

		List<String> whitelist = Arrays.asList(
			"op.success",
			"op.failure",
			"rp.success",
			"rp.failure"
		);

		WildCardMetricFilter filter = new WildCardMetricFilter(whitelist);
		assertFalse(filter.matchesNone());
		assertFalse(filter.matchesAny());

		assertEquals(whitelist, filter.getWhiteList());

		assertFalse(filter.matches("", m));
	}
	

	public void testNoWildCard() {

		Metric m = null; // ignored

		List<String> whitelist = Arrays.asList(
			"op.success",
			"op.failure",
			"rp.success",
			"rp.failure"
		);

		WildCardMetricFilter filter = new WildCardMetricFilter(whitelist);
		assertFalse(filter.matchesNone());
		assertFalse(filter.matchesAny());

		assertEquals(whitelist, filter.getWhiteList());

		assertTrue(filter.matches("op.success", m));
		assertTrue(filter.matches("op.failure", m));
		assertTrue(filter.matches("rp.success", m));
		assertTrue(filter.matches("rp.failure", m));
		assertFalse(filter.matches("x.data", m));
	}


	public void testWithWildCard() {

		Metric m = null; // ignored

		List<String> whitelist = Arrays.asList(
			"op.*",
			"*.apples"
		);

		WildCardMetricFilter filter = new WildCardMetricFilter(whitelist);
		assertFalse(filter.matchesNone());
		assertFalse(filter.matchesAny());

		assertEquals(whitelist, filter.getWhiteList());

		assertTrue(filter.matches("op.success", m));
		assertTrue(filter.matches("op.failure", m));
		assertTrue(filter.matches("green.apples", m));
		assertTrue(filter.matches("red.apples", m));
		assertFalse(filter.matches("green.peaches", m));
		assertFalse(filter.matches("read.peaches", m));
		assertFalse(filter.matches("x.data", m));
	}


	public void testMixed() {

		Metric m = null; // ignored

		List<String> whitelist = Arrays.asList(
			"op.*",
			"*.apples",
			"absolute",
			"a.*.c.d"
		);

		WildCardMetricFilter filter = new WildCardMetricFilter(whitelist);
		assertFalse(filter.matchesNone());
		assertFalse(filter.matchesAny());

		assertEquals(whitelist, filter.getWhiteList());

		assertTrue(filter.matches("op.success", m));
		assertTrue(filter.matches("op.failure", m));
		assertTrue(filter.matches("green.apples", m));
		assertTrue(filter.matches("red.apples", m));
		assertTrue(filter.matches("absolute", m));
		assertTrue(filter.matches("a.b.c.d", m));
		assertFalse(filter.matches("green.peaches", m));
		assertFalse(filter.matches("read.peaches", m));
		assertFalse(filter.matches("x.data", m));
	}
}
