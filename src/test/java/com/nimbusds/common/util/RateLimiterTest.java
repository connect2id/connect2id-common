package com.nimbusds.common.util;


import junit.framework.TestCase;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class RateLimiterTest extends TestCase {
	
	
	public void testRateLimiter() {
		
		List<String> list = new LinkedList<>();
		for (int i=0; i < 10; i++) {
			list.add("item-" + i);
		}
		
		AtomicInteger callCounter = new AtomicInteger();
		
		RateLimiter rl = callCounter::incrementAndGet;
		
		list.forEach((item) -> rl.acquirePermit());
		
		assertEquals(10, callCounter.get());
	}
}
