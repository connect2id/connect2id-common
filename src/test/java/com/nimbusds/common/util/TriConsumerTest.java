package com.nimbusds.common.util;


import junit.framework.TestCase;


public class TriConsumerTest extends TestCase {


	public void testImplementation() {
		
		String s = "abc";
		Number n = Integer.valueOf("1");
		Boolean b = Boolean.TRUE;
		
		TriConsumer<String, Number, Boolean> consumer = (var1, var2, var3) -> {
			assertEquals(s, var1);
			assertEquals(n, var2);
			assertEquals(b, var3);
		};
		
		consumer.accept(s, n, b);
		
		TriConsumer<String, Number, Boolean> after = consumer.andThen(consumer);
		
		assertNotNull(after);
	}
}
