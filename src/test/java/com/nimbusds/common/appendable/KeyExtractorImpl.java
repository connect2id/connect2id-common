package com.nimbusds.common.appendable;


/**
 * Key extractor implementation.
 */
public class KeyExtractorImpl implements KeyExtractor<AppendedElement>{


	@Override
	public String extractKey(AppendedElement object) {

		String[] strings = object.getStrings();

		if (strings == null || strings.length == 0) {
			return null;
		}

		return strings[0];
	}
}
