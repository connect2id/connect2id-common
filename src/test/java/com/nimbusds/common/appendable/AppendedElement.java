package com.nimbusds.common.appendable;


import net.minidev.json.JSONArray;
import net.minidev.json.JSONAware;


/**
 * Appended element.
 */
public class AppendedElement implements JSONAware {


	private final String[] strings;


	public AppendedElement(final String ... strings) {

		this.strings = strings;
	}


	public String[] getStrings() {

		return strings;
	}


	@Override
	public String toJSONString() {

		boolean first = true;

		JSONArray a = new JSONArray();
		for (String s: strings) {

			if (first) {
				first = false;
				continue;
			}

			a.add(s);
		}
		return a.toJSONString();
	}
}
