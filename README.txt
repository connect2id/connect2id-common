Connect2id Common Classes

Copyright (c) Connect2id Ltd., 2010 - 2023


README

Common classes factored out of the Json2Ldap, LdapAuth, Connect2id server and
other projects.


Requirements:

	* Java 17+.
	
	* For package dependencies see the Maven pom.xml.

Questions or comments? Email support@connect2id.com

[EOF]
